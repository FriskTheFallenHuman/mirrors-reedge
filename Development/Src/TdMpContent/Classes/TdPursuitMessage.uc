/*******************************************************************************
 * TdPursuitMessage generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdPursuitMessage extends TdLocalMessage;

var const localized string HelicopterIsApproaching;
var const localized string HelicopterIsLeaving;
var const localized string PoliceSearchingBag;
var const localized string PoliceSearchingIntercepted;
var SoundNodeWave VOHelicopterIsApproaching;
var SoundNodeWave VOHelicopterIsLeaving;
var SoundNodeWave VOPoliceSearchingBag;
var SoundNodeWave VORunnerMissionBriefing;
var SoundNodeWave VOCopMissionBriefing;

static simulated function SoundNodeWave GetAnnouncementSound(PlayerController LocalController, optional int Switch, optional PlayerReplicationInfo RelatedPRI_1, optional PlayerReplicationInfo RelatedPRI_2, optional Object OptionalObject)
{
    switch(Switch)
    {
        // End:0x17
        case 19:
            return default.VOHelicopterIsApproaching;
        // End:0x22
        case 20:
            return default.VOHelicopterIsLeaving;
        // End:0x2D
        case 21:
            return default.VOPoliceSearchingBag;
        // End:0x60
        case 23:
            // End:0x5A
            if(RelatedPRI_1.Team.TeamIndex == 0)
            {
                return default.VORunnerMissionBriefing;                
            }
            else
            {
                return default.VOCopMissionBriefing;
            }
        // End:0xFFFF
        default:
            return none;
            break;
    }
    //return ReturnValue;    
}

static simulated function ClientReceive(PlayerController P, optional int Switch, optional PlayerReplicationInfo PitcherPRI, optional PlayerReplicationInfo RecieverPRI, optional Object OptionalObject)
{
    super(LocalMessage).ClientReceive(P, Switch, PitcherPRI, RecieverPRI, OptionalObject);
    TdPlayerController(P).PlayAnnouncement(default.Class, GetAnnouncementSound(P, Switch, PitcherPRI, RecieverPRI, OptionalObject));
    //return;    
}

static function float GetPos(int Switch, HUD myHUD)
{
    return default.PosY;
    //return ReturnValue;    
}

static function string GetString(optional int InSwitch, optional bool bPRI1HUD, optional PlayerReplicationInfo RelatedPRI_1, optional PlayerReplicationInfo RelatedPRI_2, optional Object OptionalObject)
{
    switch(InSwitch)
    {
        // End:0x18
        case 19:
            return default.HelicopterIsApproaching;
        // End:0x23
        case 20:
            return default.HelicopterIsLeaving;
        // End:0x2E
        case 21:
            return default.PoliceSearchingBag;
        // End:0x39
        case 22:
            return default.PoliceSearchingIntercepted;
        // End:0xFFFF
        default:
            return "";
            break;
    }
    //return ReturnValue;    
}

defaultproperties
{
    MessageArea=EMA_Centered
    PosY=0.25
    FontSize=2
}