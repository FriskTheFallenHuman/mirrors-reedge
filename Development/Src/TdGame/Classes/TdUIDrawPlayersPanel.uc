/*******************************************************************************
 * TdUIDrawPlayersPanel generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUIDrawPlayersPanel extends TdUIDrawPanel
    abstract
    native
    hidecategories(Object,UIRoot,Object);

struct native TdPlayerSlot
{
    var TdPlayerReplicationInfo TdPRI;

    structdefaultproperties
    {
        TdPRI=none
    }
};

struct native TdTeamPlayerSlots
{
    var array<TdPlayerSlot> Slots;

    structdefaultproperties
    {
        Slots=none
    }
};

var(Sound) name ClickedCue;
var transient UIDataStore_TdGameData TdGameData;
var() name ResourceDataStoreName;
var protected int MaxTeams;
var protected int MaxPlayersInTeam;
var int SelectedTeam;
var int SelectedPlayer;
var(TdUIDrawPanel) Font SelectedFont;
var(TdUIDrawPanel) Font UnselectedFont;
var(TdUIDrawPanel) Color PlayerSlotBGColor;
var(TdUIDrawPanel) Color PlayerSlotSelectedBgColor;
var(TdUIDrawPanel) int TeamSpacing;
var array<TdTeamPlayerSlots> TeamSlots;
var delegate<OnPlayerSlotClicked> __OnPlayerSlotClicked__Delegate;

delegate OnPlayerSlotClicked(TdPlayerReplicationInfo TdPRI)
{
    //return;    
}

function PostInitialize()
{
    TdGameData = UIDataStore_TdGameData(GetCurrentUIController().DataStoreManager.FindDataStore(ResourceDataStoreName));
    //return;    
}

function UpdatePlayers(array<PlayerReplicationInfo> pris)
{
    local int PRIIndex, TeamIndex;
    local TdPlayerReplicationInfo TdPRI;

    TeamSlots.Remove(0, TeamSlots.Length);
    TeamSlots.Insert(0, MaxTeams);
    TeamIndex = 0;
    J0x20:

    // End:0x56 [Loop If]
    if(TeamIndex < MaxTeams)
    {
        TeamSlots[TeamIndex].Slots.Insert(0, MaxPlayersInTeam);
        ++TeamIndex;
        // [Loop Continue]
        goto J0x20;
    }
    PRIIndex = 0;
    J0x5D:

    // End:0xD8 [Loop If]
    if(PRIIndex < pris.Length)
    {
        TdPRI = TdPlayerReplicationInfo(pris[PRIIndex]);
        TeamSlots[TdPRI.Team.TeamIndex].Slots[TdPRI.TeamId].TdPRI = TdPRI;
        ++PRIIndex;
        // [Loop Continue]
        goto J0x5D;
    }
    //return;    
}

protected function bool Draw(Canvas C)
{
    local int TeamIndex, TeamX, TeamXL, TeamY, TeamYL;

    TeamX = 0;
    TeamXL = int((pWidth / float(MaxTeams)) - float(TeamSpacing));
    TeamY = 0;
    TeamYL = int(pHeight);
    TeamIndex = 0;
    J0x41:

    // End:0x99 [Loop If]
    if(TeamIndex < MaxTeams)
    {
        DrawTeam(C, TeamIndex, TeamX, TeamY, TeamXL, TeamYL);
        TeamX += (TeamXL + (TeamSpacing * 2));
        ++TeamIndex;
        // [Loop Continue]
        goto J0x41;
    }
    return true;
    //return ReturnValue;    
}

protected function DrawTeam(Canvas C, int TeamIndex, int X, int Y, int XL, int YL)
{
    local int SlotX, SlotXL, SlotY, SlotYL, PlayerIndex, RowHeight;

    local TdPlayerReplicationInfo TdPRI;
    local bool bIsSelectedPlayer;

    RowHeight = YL / MaxPlayersInTeam;
    SlotX = X;
    SlotY = Y;
    SlotXL = XL;
    SlotYL = RowHeight;
    PlayerIndex = 0;
    J0x45:

    // End:0x165 [Loop If]
    if(PlayerIndex < MaxPlayersInTeam)
    {
        bIsSelectedPlayer = IsFocused() && (TeamIndex == SelectedTeam) && PlayerIndex == SelectedPlayer;
        DrawPlayerSlotBG(C, TeamIndex, PlayerIndex, SlotX, SlotY, SlotXL, SlotYL);
        // End:0x14F
        if((TeamIndex < TeamSlots.Length) && PlayerIndex < TeamSlots[TeamIndex].Slots.Length)
        {
            TdPRI = TeamSlots[TeamIndex].Slots[PlayerIndex].TdPRI;
            // End:0x14F
            if(TdPRI != none)
            {
                DrawPlayer(C, TdPRI, TeamIndex, bIsSelectedPlayer, SlotX, SlotY, SlotXL, SlotYL);
            }
        }
        SlotY += RowHeight;
        ++PlayerIndex;
        // [Loop Continue]
        goto J0x45;
    }
    //return;    
}

protected function DrawPlayerSlotBG(Canvas C, int TeamIndex, int PlayerSlotIndex, int X, int Y, int XL, int YL)
{
    //return;    
}

protected function DrawPlayer(Canvas C, TdPlayerReplicationInfo TdPRI, int TeamIndex, bool bIsSelected, int Left, int Top, int Width, int Height)
{
    //return;    
}

function bool TdUIDrawPlayerPanel_OnClicked(UIScreenObject Sender, int PlayerIndex)
{
    local TdPlayerReplicationInfo TdPRI;

    TdPRI = GetSelectedTdPRI();
    // End:0x2F
    if(TdPRI != none)
    {
        OnPlayerSlotClicked(TdPRI);
    }
    return true;
    //return ReturnValue;    
}

function TdPlayerReplicationInfo GetSelectedTdPRI()
{
    return TeamSlots[SelectedTeam].Slots[SelectedPlayer].TdPRI;
    //return ReturnValue;    
}

function SetSelectedSlot(int TeamIndex, int PlayerIndex)
{
    SelectedTeam = TeamIndex;
    SelectedPlayer = PlayerIndex;
    //return;    
}

function int GetMaxPlayersInTeam()
{
    return MaxPlayersInTeam;
    //return ReturnValue;    
}

function int GetMaxTeams()
{
    return MaxTeams;
    //return ReturnValue;    
}

defaultproperties
{
    ClickedCue=Clicked
    ResourceDataStoreName=TdGameData
    MaxTeams=2
    MaxPlayersInTeam=6
    SelectedFont=MultiFont'UI_Fonts_Final.Helvetica_Small_Normal'
    UnselectedFont=MultiFont'UI_Fonts_Final.Helvetica_Small_Normal'
    PlayerSlotBGColor=(B=0,G=0,R=0,A=255)
    PlayerSlotSelectedBgColor=(B=0,G=0,R=200,A=255)
    TeamSpacing=20
    DrawDelegate=Draw
    OnClicked=TdUIDrawPlayerPanel_OnClicked
    DefaultStates=/* Array type was not detected. */
    EventProvider=UIComp_Event'Default__TdUIDrawPlayersPanel.WidgetEventComponent'
}