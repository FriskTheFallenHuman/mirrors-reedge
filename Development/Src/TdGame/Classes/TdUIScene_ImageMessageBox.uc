/*******************************************************************************
 * TdUIScene_ImageMessageBox generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUIScene_ImageMessageBox extends TdUIScene_MessageBox
    config(UI)
    hidecategories(Object,UIRoot,Object);

var transient UIImage Image;

function SetImage(Surface InImage)
{
    Image.SetValue(InImage);
    //return;    
}

function SetImageMarkup(string Markup)
{
    Image.SetDataStoreBinding(Markup);
    //return;    
}

defaultproperties
{
    EventProvider=UIComp_Event'Default__TdUIScene_ImageMessageBox.SceneEventComponent'
}