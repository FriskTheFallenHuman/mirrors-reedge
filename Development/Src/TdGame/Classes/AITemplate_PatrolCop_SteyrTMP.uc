/*******************************************************************************
 * AITemplate_PatrolCop_SteyrTMP generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class AITemplate_PatrolCop_SteyrTMP extends AITemplate_PatrolCop
    config(AITemplates)
    editinlinenew;

defaultproperties
{
    PawnClass="TdSpContent.TdBotPawn_PatrolCop_Steyr"
    ProfileName="PatrolCopOneHanded-SteyrTMP"
    AnimationSets[1]="AS_AI_PatrolCop_OneHanded.AS_AI_PatrolCop_OneHanded_SteyrTMP"
    SkeletalMesh="CH_TKY_Cop_Patrol.SK_TKY_Cop_Patrol_PK"
    MainWeaponClass="TdSharedContent.TdWeapon_SMG_SteyrTMP"
    MainWeaponAmmoDrops_Dropped=(Easy=30,Medium=30,Hard=30)
    MainWeaponAmmoDrops_Disarmed=(Easy=30,Medium=30,Hard=30)
    DisarmWindow=0.15
}