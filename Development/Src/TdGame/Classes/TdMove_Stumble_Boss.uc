/*******************************************************************************
 * TdMove_Stumble_Boss generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_Stumble_Boss extends TdMove_StumbleBot
    config(PawnMovement);

enum EStumbleStageBoss
{
    SSBoss_Normal,
    SSBoss_One,
    SSBoss_Two,
    SSBoss_Recover,
    SSBoss_MAX
};

var TdMove_Stumble_Boss.EStumbleStageBoss StumbleStage;
