/*******************************************************************************
 * SeqEvt_TdPlayerSwitchedWeapon generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class SeqEvt_TdPlayerSwitchedWeapon extends SequenceEvent
    hidecategories(Object);

var() Volume ConditionalVolume;
var() class<TdWeapon> ConditionalWeaponClass;

defaultproperties
{
    ObjName="Player Switched Weapon"
    ObjCategory="Takedown"
}