/*******************************************************************************
 * TdUIScene_MPPause generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUIScene_MPPause extends TdUIScene_Pause
    config(UI)
    hidecategories(Object,UIRoot,Object);

defaultproperties
{
    EventProvider=UIComp_Event'Default__TdUIScene_MPPause.SceneEventComponent'
}