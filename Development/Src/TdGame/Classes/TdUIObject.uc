/*******************************************************************************
 * TdUIObject generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUIObject extends UIObject
    abstract
    native
    hidecategories(Object,UIRoot,Object);

var bool requiresTick;
var(Widget) transient bool bShowBounds;
var array<TdUIObject> TickStack;

// Export UTdUIObject::execGetTdPlayerController(FFrame&, void* const)
native function TdPlayerController GetTdPlayerController(optional int Index);

// Export UTdUIObject::execGetTdPawn(FFrame&, void* const)
native function TdPawn GetTdPawn();

// Export UTdUIObject::execGetPRI(FFrame&, void* const)
native function TdPlayerReplicationInfo GetPRI();

// Export UTdUIObject::execGetWorldInfo(FFrame&, void* const)
native function WorldInfo GetWorldInfo();

event Initialized()
{
    super(UIScreenObject).Initialized();
    // End:0x3D
    if(requiresTick && TdUIObject(GetOwner()) != none)
    {
        TdUIObject(GetOwner()).AddChildToTickStack(self);
    }
    //return;    
}

event AddChildToTickStack(TdUIObject ChildToAdd)
{
    // End:0x29
    if(TickStack.Find(ChildToAdd) == -1)
    {
        TickStack[TickStack.Length] = ChildToAdd;
    }
    // End:0x6B
    if((TdUIObject(GetOwner()) != none) && TickStack.Length > 0)
    {
        requiresTick = true;
        TdUIObject(GetOwner()).AddChildToTickStack(self);
    }
    //return;    
}

event RemoveChildFromTickStack(TdUIObject ChildToRemove)
{
    local int ChildIndex;

    ChildIndex = TickStack.Find(ChildToRemove);
    // End:0x2E
    if(ChildIndex != -1)
    {
        TickStack.Remove(ChildIndex, 1);
    }
    // End:0x70
    if((TdUIObject(GetOwner()) != none) && TickStack.Length <= 0)
    {
        requiresTick = false;
        TdUIObject(GetOwner()).RemoveChildFromTickStack(self);
    }
    //return;    
}

event AddedChild(UIScreenObject WidgetOwner, UIObject NewChild)
{
    local TdUIObject ChildObject;

    super(UIScreenObject).AddedChild(WidgetOwner, NewChild);
    ChildObject = TdUIObject(NewChild);
    // End:0x4D
    if(ChildObject != none)
    {
        // End:0x4D
        if(ChildObject.requiresTick)
        {
            AddChildToTickStack(ChildObject);
        }
    }
    //return;    
}

event RemovedChild(UIScreenObject WidgetOwner, UIObject OldChild, optional array<UIObject> ExclusionSet)
{
    local TdUIObject ChildObject;

    super(UIScreenObject).RemovedChild(WidgetOwner, OldChild, ExclusionSet);
    ChildObject = TdUIObject(OldChild);
    // End:0x53
    if(ChildObject != none)
    {
        // End:0x53
        if(ChildObject.requiresTick)
        {
            RemoveChildFromTickStack(ChildObject);
        }
    }
    //return;    
}

defaultproperties
{
    EventProvider=UIComp_Event'Default__TdUIObject.WidgetEventComponent'
}