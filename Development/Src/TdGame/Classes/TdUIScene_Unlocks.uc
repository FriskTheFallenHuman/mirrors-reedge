/*******************************************************************************
 * TdUIScene_Unlocks generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUIScene_Unlocks extends TdUIScene
    config(UI)
    hidecategories(Object,UIRoot,Object);

var transient UIImage ArtworkBGImage;
var transient UIImage ArtworkBGTopImage;
var transient UIImage VideosBGImage;
var transient UIImage VideosBGTopImage;
var transient UIImage MusicBGImage;
var transient UIImage MusicBGTopImage;
var transient TdUITabControl TabControl;

event PostInitialize()
{
    super.PostInitialize();
    TabControl.__OnPageActivated__Delegate = OnTabPageActivated;
    OnTabPageActivated(TabControl, TabControl.ActivePage, GetPlayerIndex());
    //return;    
}

function OnTabPageActivated(UITabControl Sender, UITabPage NewlyActivePage, int PlayerIndex)
{
    ArtworkBGImage.SetVisibility(false);
    ArtworkBGTopImage.SetVisibility(false);
    VideosBGImage.SetVisibility(false);
    VideosBGTopImage.SetVisibility(false);
    MusicBGImage.SetVisibility(false);
    MusicBGTopImage.SetVisibility(false);
    // End:0xE1
    if(NewlyActivePage.Name == 'ArtworkTabPage')
    {
        ArtworkBGImage.SetVisibility(true);
        ArtworkBGTopImage.SetVisibility(true);
        TdUITabPage_UnlockedArtwork(NewlyActivePage).RefreshButtonBar();        
    }
    else
    {
        // End:0x144
        if(NewlyActivePage.Name == 'VideosTabPage')
        {
            VideosBGImage.SetVisibility(true);
            VideosBGTopImage.SetVisibility(true);
            TdUITabPage_UnlockedVideos(NewlyActivePage).RefreshButtonBar();            
        }
        else
        {
            // End:0x1A4
            if(NewlyActivePage.Name == 'MusicTabPage')
            {
                MusicBGImage.SetVisibility(true);
                MusicBGTopImage.SetVisibility(true);
                TdUITabPage_UnlockedMusic(NewlyActivePage).RefreshButtonBar();
            }
        }
    }
    //return;    
}

event OnPostTick(float DeltaTime, bool bTopmostScene)
{
    super.OnPostTick(DeltaTime, bTopmostScene);
    // End:0x5E
    if(TabControl.ActivePage.IsA('TdUITabPage_UnlockedArtwork'))
    {
        TdUITabPage_UnlockedArtwork(TabControl.ActivePage).TickArtworkUnlock(DeltaTime);        
    }
    else
    {
        // End:0xA8
        if(TabControl.ActivePage.IsA('TdUITabPage_UnlockedVideos'))
        {
            TdUITabPage_UnlockedVideos(TabControl.ActivePage).TickPreviewImageUnlock(DeltaTime);
        }
    }
    //return;    
}

function SetupButtonBar()
{
    local TdUITabPage_UnlockedArtwork ArtworkTabPage;

    ArtworkTabPage = TdUITabPage_UnlockedArtwork(TabControl.ActivePage);
    // End:0x39
    if(ArtworkTabPage != none)
    {
        ArtworkTabPage.RefreshButtonBar();
    }
    //return;    
}

function OnCloseScene()
{
    CloseScene(self);
    //return;    
}

function bool HandleInputKey(const out InputEventParameters EventParms)
{
    local bool bResult;
    local TdUITabPage CurrentTabPage;

    CurrentTabPage = TdUITabPage(TabControl.ActivePage);
    bResult = CurrentTabPage.HandleInputKey(EventParms);
    // End:0xB1
    if(bResult == false)
    {
        // End:0xB1
        if(EventParms.EventType == 1)
        {
            // End:0xB1
            if((EventParms.InputKeyName == 'XboxTypeS_B') || EventParms.InputKeyName == 'Escape')
            {
                OnCloseScene();
                bResult = true;
            }
        }
    }
    return bResult;
    //return ReturnValue;    
}

defaultproperties
{
    EventProvider=UIComp_Event'Default__TdUIScene_Unlocks.SceneEventComponent'
}