/*******************************************************************************
 * TdHudEffect_FallDamage generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdHudEffect_FallDamage extends TdHudEffect
    native
    config(HudEffects);

var config PPSettings PPDistortion;
var config float DurationWhenDying;
var config float FadeInWhenDying;
var transient float PPDFadeInTimer;
var transient float PPDFadeInStart;
var transient float PPDPeakDurationTimer;
var transient float PPDFadeOutTimer;
var transient float PPDTargetStrength;
var transient float PPDCurrentStrength;
var private name DirectionName;

function ResetPP()
{
    PPDFadeInTimer = default.PPDFadeInTimer;
    PPDFadeOutTimer = default.PPDFadeOutTimer;
    PPDPeakDurationTimer = default.PPDPeakDurationTimer;
    PPDFadeInStart = default.PPDFadeInStart;
    PPDTargetStrength = default.PPDTargetStrength;
    PPDCurrentStrength = default.PPDCurrentStrength;
    super.ResetPP();
    //return;    
}

defaultproperties
{
    PPDistortion=(ParameterName=FXFScreen_FallDistortionSpeed,FadeInDuration=0.1,Duration=0.1,FadeOutDuration=0.3)
    DurationWhenDying=2.5
    DirectionName=FXFScreen_FallDirection
    Blur=(Duration=0.12,FadeOutDuration=0.75)
    PPStrength=(ParameterName=FXFScreen_FallDamage,FadeInDuration=0.03,Duration=0.12,FadeOutDuration=0.75)
}