/*******************************************************************************
 * TdReachSpec_Wallrun generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdReachSpec_Wallrun extends TdMoveReachSpec
    native
    config(PathfindingCosts);
