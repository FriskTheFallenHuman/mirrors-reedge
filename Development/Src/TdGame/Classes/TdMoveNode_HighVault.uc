/*******************************************************************************
 * TdMoveNode_HighVault generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMoveNode_HighVault extends TdMoveNode_Vault
    config(PathfindingCosts)
    hidecategories(Navigation,Lighting,LightColor,Force);

defaultproperties
{
    VaultOntoIcon=SpriteComponent'Default__TdMoveNode_HighVault.Sprite_VaultOnto'
    VaultOverIcon=SpriteComponent'Default__TdMoveNode_HighVault.Sprite_VaultOver'
    CylinderComponent=CylinderComponent'Default__TdMoveNode_HighVault.CollisionCylinder'
    GoodSprite=SpriteComponent'Default__TdMoveNode_HighVault.Sprite'
    BadSprite=SpriteComponent'Default__TdMoveNode_HighVault.Sprite2'
    Components(0)=SpriteComponent'Default__TdMoveNode_HighVault.Sprite'
    Components(1)=SpriteComponent'Default__TdMoveNode_HighVault.Sprite2'
    Components(2)=ArrowComponent'Default__TdMoveNode_HighVault.Arrow'
    Components(3)=CylinderComponent'Default__TdMoveNode_HighVault.CollisionCylinder'
    Components(4)=PathRenderingComponent'Default__TdMoveNode_HighVault.PathRenderer'
    Components(5)=SpriteComponent'Default__TdMoveNode_HighVault.Sprite_VaultOver'
    Components(6)=SpriteComponent'Default__TdMoveNode_HighVault.Sprite_VaultOnto'
    CollisionComponent=CylinderComponent'Default__TdMoveNode_HighVault.CollisionCylinder'
}