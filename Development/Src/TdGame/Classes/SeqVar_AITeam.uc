/*******************************************************************************
 * SeqVar_AITeam generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class SeqVar_AITeam extends SeqVar_Object
    native
    hidecategories(Object);

var() editinline string TeamName;
var transient AITeam Team;
var() AITeam.ESide Side;

defaultproperties
{
    TeamName="Unnamed Team"
    ObjName="Team"
    ObjCategory="AI"
    ObjColor=(B=0,G=128,R=255,A=255)
}