/*******************************************************************************
 * UIDataProvider_TdPersonaProvider generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class UIDataProvider_TdPersonaProvider extends UIDataProvider_TdResource
    transient
    native
    config(Game)
    perobjectconfig
    hidecategories(Object,UIRoot);

var array<string> Personas;

event OnRegister(LocalPlayer InPlayer)
{
    //return;    
}

event OnUnregister()
{
    //return;    
}
