/*******************************************************************************
 * TdUIDrawPanel generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUIDrawPanel extends TdUIObject
    native
    hidecategories(Object,UIRoot,Object);

var() bool bUseFullViewport;
var Canvas Canvas;
var float pLeft;
var float pTop;
var float pWidth;
var float pHeight;
var float ResolutionScale;
var delegate<DrawDelegate> __DrawDelegate__Delegate;

// Export UTdUIDrawPanel::execDraw2DLine(FFrame&, void* const)
native final function Draw2DLine(int X1, int Y1, int X2, int Y2, Color LineColor);

delegate bool DrawDelegate(Canvas C)
{
    //return ReturnValue;    
}

event DrawPanel()
{
    //return;    
}

defaultproperties
{
    DefaultStates=/* Array type was not detected. */
    EventProvider=UIComp_Event'Default__TdUIDrawPanel.WidgetEventComponent'
}