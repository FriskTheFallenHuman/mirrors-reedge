/*******************************************************************************
 * TdActorFactoryPickup generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdActorFactoryPickup extends ActorFactory
    native
    config(Editor)
    editinlinenew
    collapsecategories
    hidecategories(Object);

var() class<Inventory> InventoryClass;
var() float LifeSpanInSeconds;
var() bool bLiveForever;

defaultproperties
{
    LifeSpanInSeconds=16
    NewActorClass=Class'TdPickup'
    bPlaceable=false
}