/*******************************************************************************
 * TdDmgType_Taser generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdDmgType_Taser extends TdDamageType;

var float ContiniousTaserDamage;
var float InitialTaserDamage;

defaultproperties
{
    ContiniousTaserDamage=7.5
    InitialTaserDamage=30
    bCausesBlood=false
    KDamageImpulse=1000
    KImpulseRadius=20
    begin object name=ForceFeedbackWaveform0 class=ForceFeedbackWaveform
        Samples=/* Array type was not detected. */
    object end
    // Reference: ForceFeedbackWaveform'Default__TdDmgType_Taser.ForceFeedbackWaveform0'
    DamagedFFWaveform=ForceFeedbackWaveform0
    begin object name=ForceFeedbackWaveform1 class=ForceFeedbackWaveform
        Samples=/* Array type was not detected. */
    object end
    // Reference: ForceFeedbackWaveform'Default__TdDmgType_Taser.ForceFeedbackWaveform1'
    KilledFFWaveform=ForceFeedbackWaveform1
}