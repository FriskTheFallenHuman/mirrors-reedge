/*******************************************************************************
 * TdDrawArcComponent generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdDrawArcComponent extends PrimitiveComponent
    native
    editinlinenew
    collapsecategories
    hidecategories(Object);

var transient Color ArcColor;
var transient float ArcRadius;
var transient int ArcRes;
var transient float ArcAngle;

defaultproperties
{
    ArcColor=(B=0,G=128,R=255,A=255)
    ArcRadius=128
    ArcRes=16
    ArcAngle=175
    HiddenGame=true
}