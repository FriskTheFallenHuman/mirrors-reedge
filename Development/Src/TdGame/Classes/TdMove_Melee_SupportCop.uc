/*******************************************************************************
 * TdMove_Melee_SupportCop generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_Melee_SupportCop extends TdMove_BotMelee
    config(AIMeleeAttacks);

defaultproperties
{
    GenericAttackProperties=(Damage=60)
}