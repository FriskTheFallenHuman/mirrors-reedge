/*******************************************************************************
 * UIDataProvider_UnlocksProvider generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class UIDataProvider_UnlocksProvider extends UIDataProvider_TdResource
    transient
    native
    config(Game)
    perobjectconfig
    hidecategories(Object,UIRoot);

var const config localized string FriendlyName;
var const config localized string Description;
var config string ResourcePath;
var config int LevelId;
var config int UnlockId;
