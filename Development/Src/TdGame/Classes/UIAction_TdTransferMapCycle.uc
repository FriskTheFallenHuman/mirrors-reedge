/*******************************************************************************
 * UIAction_TdTransferMapCycle generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class UIAction_TdTransferMapCycle extends UIAction
    native
    hidecategories(Object);

var() bool bTransferToProfile;

defaultproperties
{
    bAutoTargetOwner=true
    ObjName="Transfer Map Cycle"
    ObjCategory="Takedown"
}