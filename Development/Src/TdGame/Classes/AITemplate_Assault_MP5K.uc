/*******************************************************************************
 * AITemplate_Assault_MP5K generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class AITemplate_Assault_MP5K extends AITemplate_Assault
    config(AITemplates)
    editinlinenew;

defaultproperties
{
    ProfileName="AssaultTwoHanded-MP5K"
    AnimationSets[1]="AS_AI_Assault_TwoHanded.AS_AI_Assault_TwoHanded_MP5K"
    MainWeaponClass="TdSharedContent.TdWeapon_AssaultRifle_MP5K"
    MainWeaponAmmoDrops_Dropped=(Easy=30,Medium=30,Hard=30)
    MainWeaponAmmoDrops_Disarmed=(Easy=30,Medium=30,Hard=30)
    DisarmWindow=0.25
}