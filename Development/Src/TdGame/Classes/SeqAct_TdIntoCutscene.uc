/*******************************************************************************
 * SeqAct_TdIntoCutscene generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class SeqAct_TdIntoCutscene extends SeqAct_Latent
    native
    hidecategories(Object);

var transient TdPlayerPawn PlayerPawn;
var transient Actor DestinationActor;

// Export USeqAct_TdIntoCutscene::execAbortScriptedMove(FFrame&, void* const)
native function AbortScriptedMove();

// Export USeqAct_TdIntoCutscene::execFinishScriptedMove(FFrame&, void* const)
native function FinishScriptedMove();

event Activated()
{
    TdMove_Cutscene(PlayerPawn.Moves[93]).TargetLocation = DestinationActor.Location;
    TdMove_Cutscene(PlayerPawn.Moves[93]).TargetRotation = DestinationActor.Rotation;
    PlayerPawn.SetMove(93);
    //return;    
}

event bool Update(float DeltaTime)
{
    // End:0x1C
    if(PlayerPawn.MovementState != 93)
    {
        return false;
    }
    return true;
    //return ReturnValue;    
}

defaultproperties
{
    VariableLinks(0)=(ExpectedType=Class'Engine.SeqVar_Object',LinkedVariables=none,LinkDesc="Target",LinkVar=None,PropertyName=Targets,bWriteable=false,bHidden=false,MinVars=1,MaxVars=255,DrawX=0,CachedProperty=none)
    VariableLinks(1)=(ExpectedType=Class'Engine.SeqVar_Object',LinkedVariables=none,LinkDesc="Destination",LinkVar=None,PropertyName=None,bWriteable=false,bHidden=false,MinVars=1,MaxVars=255,DrawX=0,CachedProperty=none)
    ObjName="Into Cutscene"
    ObjCategory="Takedown"
}