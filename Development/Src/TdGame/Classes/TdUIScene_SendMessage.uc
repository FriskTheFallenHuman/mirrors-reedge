/*******************************************************************************
 * TdUIScene_SendMessage generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUIScene_SendMessage extends TdUIScene_SubMenu
    config(UI)
    hidecategories(Object,UIRoot,Object);

var transient UIEditBox PlayerNameEditBox;
var transient string TargetPlayerName;
var transient UniqueNetId TargetPlayerNetId;

event PostInitialize()
{
    super.PostInitialize();
    PlayerNameEditBox.__OnSubmitText__Delegate = None;
    //return;    
}

function SetupButtonBar()
{
    ButtonBar.AppendButton("Cancel", OnButtonBar_Cancel);
    ButtonBar.AppendButton("AddFriendAccept", OnButtonBar_AddFriend);
    //return;    
}

function bool OnButtonBar_Cancel(UIScreenObject Sender, int PlayerIndex)
{
    OnCloseScene();
    return true;
    //return ReturnValue;    
}

function bool OnButtonBar_AddFriend(UIScreenObject Sender, int PlayerIndex)
{
    AddFriend();
    return true;
    //return ReturnValue;    
}

function SetTargetPlayer(UniqueNetId InPlayerNetId, string InPlayerName)
{
    TargetPlayerNetId = InPlayerNetId;
    TargetPlayerName = InPlayerName;
    PlayerNameEditBox.SetValue(InPlayerName);
    //return;    
}

function AddFriend()
{
    local OnlinePlayerInterface PlayerInt;
    local string LocalPlayerName;
    local bool bTargetIsSelf;

    TargetPlayerName = PlayerNameEditBox.GetValue();
    // End:0x195
    if(Len(TargetPlayerName) > 0)
    {
        GetDataStoreStringValue("<OnlinePlayerData:PlayerNickName>", LocalPlayerName, self, GetPlayerOwner());
        bTargetIsSelf = TargetPlayerName == LocalPlayerName;
        // End:0x113
        if(!bTargetIsSelf)
        {
            PlayerInt = GetPlayerInterface();
            // End:0x110
            if(NotEqual_InterfaceInterface(PlayerInt, OnlinePlayerInterface(none)))
            {
                PlayerInt.AddAddFriendByNameCompleteDelegate(byte(GetPlayerOwner().ControllerId), OnAddFriendByNameComplete);
                // End:0x110
                if(PlayerInt.AddFriendByName(byte(GetPlayerOwner().ControllerId), TargetPlayerName) == false)
                {
                    OnAddFriendByNameComplete(false);
                }
            }            
        }
        else
        {
            DisplaySimpleMessageBox("<Strings:TdGameUI.TdMessageBox.MessageToYourself_Message>", "<Strings:TdGameUI.TdMessageBox.MessageToYourself_Title>");
        }        
    }
    else
    {
        DisplaySimpleMessageBox("<Strings:TdGameUI.TdMessageBox.InvalidPlayerName_Message>", "<Strings:TdGameUI.TdMessageBox.InvalidPlayerName_Title>");
    }
    //return;    
}

function OnAddFriendByNameComplete(bool bWasSuccessful)
{
    local string FormattedMsg, TitleMsg;

    // End:0xA4
    if(bWasSuccessful)
    {
        TitleMsg = "<Strings:TdGameUI.TdMessageBox.FriendRequestSent_Title>";
        FormattedMsg = Repl(Localize("TdMessageBox", "FriendRequestSent_Message", "TdGameUI"), "`PlayerName`", TargetPlayerName);        
    }
    else
    {
        TitleMsg = "<Strings:TdGameUI.TdMessageBox.FriendRequestFailed_Title>";
        FormattedMsg = Repl(Localize("TdMessageBox", "FriendRequestFailed_Message", "TdGameUI"), "`PlayerName`", TargetPlayerName);
    }
    DisplaySimpleMessageBox(FormattedMsg, TitleMsg, OnFinishedAddFriend);
    GetPlayerInterface().ClearAddFriendByNameCompleteDelegate(byte(GetPlayerOwner().ControllerId), OnAddFriendByNameComplete);
    //return;    
}

function OnFinishedAddFriend()
{
    OnCloseScene();
    //return;    
}

function OnCloseScene()
{
    CloseScene(self);
    //return;    
}

function bool HandleInputKey(const out InputEventParameters EventParms)
{
    local bool bResult;

    // End:0xBE
    if(EventParms.EventType == 1)
    {
        // End:0x6E
        if((EventParms.InputKeyName == 'XboxTypeS_A') || EventParms.InputKeyName == 'Enter')
        {
            AddFriend();
            bResult = true;            
        }
        else
        {
            // End:0xBE
            if((EventParms.InputKeyName == 'XboxTypeS_B') || EventParms.InputKeyName == 'Escape')
            {
                OnCloseScene();
                bResult = true;
            }
        }
    }
    return bResult;
    //return ReturnValue;    
}

defaultproperties
{
    EventProvider=UIComp_Event'Default__TdUIScene_SendMessage.SceneEventComponent'
}