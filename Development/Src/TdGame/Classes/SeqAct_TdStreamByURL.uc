/*******************************************************************************
 * SeqAct_TdStreamByURL generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class SeqAct_TdStreamByURL extends SeqAct_Latent
    native
    hidecategories(Object);

var int stage;

defaultproperties
{
    InputLinks(0)=(LinkDesc="Stream",bHasImpulse=false,bDisabled=false,bDisabledPIE=false,LinkedOp=none,DrawY=0,bHidden=false,ActivateDelay=0)
    VariableLinks=none
    ObjName="Td Stream Level By URL"
    ObjCategory="Takedown"
}