/*******************************************************************************
 * SeqAct_TdDisableReactionTime generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class SeqAct_TdDisableReactionTime extends SequenceAction
    hidecategories(Object);

event bool IsValidUISequenceObject(optional UIScreenObject TargetObject)
{
    return false;
    //return ReturnValue;    
}

defaultproperties
{
    ObjName="Disable Reaction Time"
    ObjCategory="Takedown"
}