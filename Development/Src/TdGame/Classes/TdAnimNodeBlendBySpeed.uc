/*******************************************************************************
 * TdAnimNodeBlendBySpeed generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdAnimNodeBlendBySpeed extends AnimNodeBlendBySpeed
    native
    hidecategories(Object,Object,Object);

var() bool bDebugThis;
var() bool bUseAverageSpeed;

defaultproperties
{
    bUseAverageSpeed=true
    Constraints=/* Array type was not detected. */
}