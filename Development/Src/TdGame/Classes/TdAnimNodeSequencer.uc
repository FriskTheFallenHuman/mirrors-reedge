/*******************************************************************************
 * TdAnimNodeSequencer generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdAnimNodeSequencer extends TdAnimNodeBlendList
    native
    hidecategories(Object,Object,Object);

event OnBecomeRelevant()
{
    SetActiveChild(0, 0);
    //return;    
}

defaultproperties
{
    Children=/* Array type was not detected. */
    bFixNumChildren=true
}