/*******************************************************************************
 * TdMove_Cutscene generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_Cutscene extends TdPhysicsMove
    config(PawnMovement);

var Vector TargetLocation;
var Rotator TargetRotation;

function bool CanDoMove()
{
    return true;
    //return ReturnValue;    
}

simulated function StartMove()
{
    local Vector ToTarget;
    local float TimeToTarget;

    super.StartMove();
    ToTarget = TargetLocation - PawnOwner.Location;
    TimeToTarget = VSize2D(ToTarget) / 250;
    // End:0x7E
    if(TimeToTarget > 0.4)
    {
        SetPreciseRotation(rotator(ToTarget), 0.2);
        SetMoveTimer(TimeToTarget - 0.3, false, 'AlignToTargetRotation');        
    }
    else
    {
        SetPreciseRotation(TargetRotation, 0.2);
    }
    SetPreciseLocation(TargetLocation, 0, 250);
    //return;    
}

simulated function StopMove()
{
    super.StopMove();
    //return;    
}

simulated event ReachedPreciseLocation()
{
    PawnOwner.Velocity = vect(0, 0, 0);
    PawnOwner.Acceleration = vect(0, 0, 0);
    PawnOwner.SetMove(1);
    //return;    
}

simulated function AlignToTargetRotation()
{
    SetPreciseRotation(TargetRotation, 0.2);
    //return;    
}

simulated function Landed(Vector aNormal, Actor anActor)
{
    //return;    
}

defaultproperties
{
    PawnPhysics=PHYS_Flying
    bDisableCollision=true
    DisableMovementTime=-1
    DisableLookTime=-1
}