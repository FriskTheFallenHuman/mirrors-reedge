/*******************************************************************************
 * TdSkelControlAim1p generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdSkelControlAim1p extends SkelControlSingleBone
    native
    config(Animation)
    hidecategories(Object);

enum EAimingType
{
    TDEAT_NoAim,
    TDEAT_UpperBody,
    TDEAT_Right,
    TDEAT_Left,
    TDEAT_MAX
};

var() bool bApplySwanNeckTranslation;
var bool bBlendInUnarmedAim;
var config float UnarmedAimingMomentumThreshold;
var config float UnarmedAimingBlendInTime;
var config float UnarmedAimingBlendOutTime;
var float CurrentUnarmedAimingMomentumThreshold;
var(Aiming) TdSkelControlAim1p.EAimingType AimingType;

// Export UTdSkelControlAim1p::execUpdateTransformation(FFrame&, void* const)
native function UpdateTransformation(TdPawn PawnOwner);

event OnInit()
{
    CurrentUnarmedAimingMomentumThreshold = UnarmedAimingMomentumThreshold;
    //return;    
}

defaultproperties
{
    bApplySwanNeckTranslation=true
    UnarmedAimingMomentumThreshold=100
    UnarmedAimingBlendInTime=0.5
    UnarmedAimingBlendOutTime=0.2
    CurrentUnarmedAimingMomentumThreshold=330
}