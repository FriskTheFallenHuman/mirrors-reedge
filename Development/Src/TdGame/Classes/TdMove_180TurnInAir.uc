/*******************************************************************************
 * TdMove_180TurnInAir generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_180TurnInAir extends TdPhysicsMove
    native
    config(PawnMovement);

var Rotator WantedRotation;

function bool CanDoMove()
{
    // End:0x1D
    if(PawnOwner.GetWeaponType() == 1)
    {
        return false;
    }
    // End:0x39
    if(PawnOwner.MovementState == 78)
    {
        return false;
    }
    // End:0x6B
    if((Normal(vector(PawnOwner.Rotation)) Dot Normal(PawnOwner.Velocity)) < 0.2)
    {
        return false;
    }
    return super(TdMove).CanDoMove();
    //return ReturnValue;    
}

simulated function StartMove()
{
    local Vector LookAtPosition;
    local TdPlayerController TdPC;

    super.StartMove();
    PlayMoveAnim(2, name("JumpTurnFly"), 1, 0.1, 0.1, false, true);
    TdPC = TdPlayerController(PawnOwner.Controller);
    // End:0x64
    if(TdPC != none)
    {
        TdPC.TargetingPawn = none;
    }
    PawnOwner.UseRootRotation(true);
    LookAtPosition = PawnOwner.LastJumpLocation;
    LookAtPosition.Z += 90;
    SetLookAtTargetLocation(LookAtPosition, 0.3, 2);
    //return;    
}

simulated function StopMove()
{
    super.StopMove();
    PawnOwner.UseRootRotation(false);
    PawnOwner.StopCustomAnim(7, 0.2);
    //return;    
}

simulated function UpdateViewRotation(out Rotator out_Rotation, float DeltaTime, out Rotator DeltaRot)
{
    super(TdMove).UpdateViewRotation(out_Rotation, DeltaTime, DeltaRot);
    // End:0x49
    if((DeltaRot.Yaw > 0) || DeltaRot.Yaw < 0)
    {
        AbortLookAtTarget();
    }
    //return;    
}

simulated function OnCeaseRelevantRootMotion(AnimNodeSequence SeqNode)
{
    // End:0x32
    if(SeqNode.AnimSeqName == 'JumpTurnFly')
    {
        PawnOwner.UseRootRotation(false);
    }
    //return;    
}

simulated function HandleMoveAction(TdPawn.EMovementAction Action)
{
    local TdPlayerController TdPC;
    local TdPawn P;
    local TdBotPawn BotPawn;

    // End:0x137
    if((Action == 3) && PawnOwner.WeaponAnimState == 0)
    {
        PlayMoveAnim(7, name("Taunt"), 1, 0.1, 0.2);
        TdPC = TdPlayerController(PawnOwner.Controller);
        // End:0x137
        if(TdPC != none)
        {
            // End:0x136
            foreach TdPC.LocalEnemyActors(P)
            {
                BotPawn = TdBotPawn(P);
                // End:0x135
                if(((((BotPawn != none) && BotPawn.Controller != none) && BotPawn.Controller.Enemy == PawnOwner) && BotPawn.Controller.CanSee(PawnOwner)) && !BotPawn.IsA('TdBotPawn_Tutorial'))
                {
                    TdPC.AddStatsEvent(10);
                }                
            }            
        }
    }
    //return;    
}

defaultproperties
{
    PawnPhysics=PHYS_Falling
    bCheckExitToUncontrolledFalling=true
    bCheckForSoftLanding=true
    bConstrainLook=true
    bLookAtTargetLocation=true
    bDisableFaceRotation=true
    bUseCustomCollision=true
    FirstPersonLowerBodyDPG=SDPG_Foreground
    DisableMovementTime=-1
    MinLookConstraint=(Pitch=0,Yaw=-5000,Roll=-32768)
    MaxLookConstraint=(Pitch=32768,Yaw=5000,Roll=32768)
    SwanNeckEnableAtPitch=0
    SwanNeckForward=0
    SwanNeckDown=0
}