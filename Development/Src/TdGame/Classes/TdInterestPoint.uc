/*******************************************************************************
 * TdInterestPoint generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdInterestPoint extends NavigationPoint
    hidecategories(Navigation,Lighting,LightColor,Force);

var() float InvestigationDistance;
var() float InvestigationInterval;
var protected float InvestigationTime;

function bool ShouldBeInvestigated(float GameTime)
{
    return (GameTime - InvestigationTime) > InvestigationInterval;
    //return ReturnValue;    
}

function MarkInvestigated(float GameTime)
{
    InvestigationTime = GameTime;
    //return;    
}

defaultproperties
{
    InvestigationDistance=-1
    InvestigationInterval=10
    CylinderComponent=CylinderComponent'Default__TdInterestPoint.CollisionCylinder'
    begin object name=Sprite class=SpriteComponent
        Sprite=Texture2D'TdEditorResources.InvestigateIcon'
    object end
    // Reference: SpriteComponent'Default__TdInterestPoint.Sprite'
    GoodSprite=Sprite
    BadSprite=SpriteComponent'Default__TdInterestPoint.Sprite2'
    begin object name=Sprite class=SpriteComponent
        Sprite=Texture2D'TdEditorResources.InvestigateIcon'
    object end
    // Reference: SpriteComponent'Default__TdInterestPoint.Sprite'
    Components(0)=Sprite
    Components(1)=SpriteComponent'Default__TdInterestPoint.Sprite2'
    Components(2)=ArrowComponent'Default__TdInterestPoint.Arrow'
    Components(3)=CylinderComponent'Default__TdInterestPoint.CollisionCylinder'
    Components(4)=PathRenderingComponent'Default__TdInterestPoint.PathRenderer'
    CollisionComponent=CylinderComponent'Default__TdInterestPoint.CollisionCylinder'
}