/*******************************************************************************
 * TdMenuGameInfo generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMenuGameInfo extends TdSPGame
    config(Game)
    hidecategories(Navigation,Movement,Collision);
