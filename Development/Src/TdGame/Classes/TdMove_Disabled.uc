/*******************************************************************************
 * TdMove_Disabled generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_Disabled extends TdMove
    config(PawnMovement);

function bool CanDoMove()
{
    return false;
    //return ReturnValue;    
}

simulated function StartMove()
{
    ScriptTrace();
    PawnOwner.SetMove(1);
    //return;    
}
