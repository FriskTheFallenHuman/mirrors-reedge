/*******************************************************************************
 * TdUITextSlider generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUITextSlider extends UISlider
    native
    hidecategories(Object,UIRoot,Object);

enum ESliderButtonArrow
{
    SLIDERARROW_Left,
    SLIDERARROW_Right,
    SLIDERARROW_MAX
};

var UIStyleReference LeftArrowStyle;
var UIStyleReference RightArrowStyle;
var class<UIState> LeftArrowState;
var class<UIState> RightArrowState;
var(Image) export editinlineuse UITexture LeftArrowTexture;
var(Image) export editinlineuse UITexture RightArrowTexture;

defaultproperties
{
    LeftArrowStyle=(DefaultStyleTag=DefaultButtonLeftArrowStyle,RequiredStyleClass=Class'Engine.UIStyle_Image',AssignedStyleID=(A=0,B=0,C=0,D=0),ResolvedStyle=none)
    RightArrowStyle=(DefaultStyleTag=DefaultButtonRightArrowStyle,RequiredStyleClass=Class'Engine.UIStyle_Image',AssignedStyleID=(A=0,B=0,C=0,D=0),ResolvedStyle=none)
    LeftArrowState=Class'Engine.UIState_Enabled'
    RightArrowState=Class'Engine.UIState_Enabled'
    BackgroundImageComponent=UIComp_DrawImage'Default__TdUITextSlider.SliderBackgroundImageTemplate'
    SliderBarImageComponent=UIComp_DrawImage'Default__TdUITextSlider.SliderBarImageTemplate'
    MarkerImageComponent=UIComp_DrawImage'Default__TdUITextSlider.SliderMarkerImageTemplate'
    CaptionRenderComponent=UIComp_DrawStringSlider'Default__TdUITextSlider.SliderCaptionRenderer'
    PrimaryStyle=(DefaultStyleTag=DefaultComboStyle,RequiredStyleClass=Class'Engine.UIStyle_Combo')
    EventProvider=UIComp_Event'Default__TdUITextSlider.WidgetEventComponent'
}