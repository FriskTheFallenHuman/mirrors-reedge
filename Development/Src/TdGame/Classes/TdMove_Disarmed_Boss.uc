/*******************************************************************************
 * TdMove_Disarmed_Boss generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_Disarmed_Boss extends TdMove_DisarmedBot
    config(PawnMovement);

var TdAIController.EDisarmState DisarmState;
