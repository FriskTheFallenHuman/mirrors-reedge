/*******************************************************************************
 * TdCheckpointManager generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdCheckpointManager extends Object
    native;

struct native CheckpointInformation
{
    var string MapName;
    var array<string> Checkpoints;

    structdefaultproperties
    {
        MapName=""
        Checkpoints=none
    }
};

var private transient string ActiveCheckpoint;
var private transient string ActiveMap;
var private transient int ActiveCheckpointWeight;
var private transient string LastSavedMap;
var private transient string LastSavedCheckpoint;
var private transient int NumProfileSaveTries;
var private transient TdPlayerController ActivePlayerController;
var private transient array<CheckpointInformation> CachedCheckpointInformation;
var private transient UIDataStore_TdGameData GameData;
var bool bDebugCheckpoints;

// Export UTdCheckpointManager::execFindCurrentCheckpoint(FFrame&, void* const)
native final function TdCheckpoint FindCurrentCheckpoint();

function Initialize(UIDataStore_TdGameData TdGameData)
{
    local OnlineSubsystem OnlineSub;

    CacheCheckpointInformation(TdGameData);
    OnlineSub = Class'GameEngine'.static.GetOnlineSubsystem();
    // End:0x83
    if(OnlineSub != none)
    {
        OnlineSub.PlayerInterface.AddLoginChangeDelegate(OnLoginChange);
        OnlineSub.PlayerInterface.AddReadProfileSettingsCompleteDelegate(0, OnProfileReadComplete);
    }
    //return;    
}

private final function OnProfileReadComplete(bool bSuccess)
{
    local OnlineSubsystem OnlineSub;
    local TdProfileSettings Profile;

    // End:0xC2
    if(bSuccess)
    {
        OnlineSub = Class'GameEngine'.static.GetOnlineSubsystem();
        // End:0xC2
        if(OnlineSub != none)
        {
            Profile = TdProfileSettings(OnlineSub.PlayerInterface.GetProfileSettings(byte(Class'UIInteraction'.static.GetPlayerControllerId(0))));
            // End:0xC2
            if(Profile != none)
            {
                Profile.GetProfileSettingValue(Profile.900, LastSavedMap);
                Profile.GetProfileSettingValue(Profile.901, LastSavedCheckpoint);
            }
        }
    }
    //return;    
}

function OnLoginChange()
{
    LastSavedMap = "";
    LastSavedCheckpoint = "";
    ActiveCheckpoint = "";
    ActiveMap = "";
    //return;    
}

function bool CanContinueGame()
{
    // End:0x1C
    if((LastSavedMap != "") && LastSavedCheckpoint != "")
    {
        return true;
    }
    return false;
    //return ReturnValue;    
}

function bool GetContinueGame(out string Map, out string Checkpoint)
{
    // End:0x48
    if((LastSavedMap != "") && LastSavedCheckpoint != "")
    {
        Map = LastSavedMap;
        ActiveMap = LastSavedMap;
        Checkpoint = LastSavedCheckpoint;
        ActiveCheckpoint = LastSavedCheckpoint;
        return true;
    }
    return false;
    //return ReturnValue;    
}

event string GetActiveCheckpoint()
{
    return ActiveCheckpoint;
    //return ReturnValue;    
}

function SetActiveCheckpoint(string NewCheckpoint)
{
    ActiveCheckpoint = NewCheckpoint;
    ActiveCheckpointWeight = 0;
    ActiveMap = Class'Engine'.static.GetCurrentWorldInfo().GetMapName();
    //return;    
}

event string GetActiveMap()
{
    return ActiveMap;
    //return ReturnValue;    
}

event SetNewCheckpoint(TdCheckpoint NewCheckpoint, TdPlayerController PC, optional bool skipSaveToDisk)
{
    local int NewCheckpointIndex, NewMapIndex, LastSavedCheckpointIndex, LastSavedMapIndex;
    local OnlineSubsystem OnlineSub;
    local TdProfileSettings Profile;
    local int ControllerId;
    local TdSPGame GameInfo;

    // End:0x1C
    if(NewCheckpoint.CheckpointWeight < ActiveCheckpointWeight)
    {
        return;
    }
    ActiveCheckpoint = NewCheckpoint.CheckpointName;
    ActiveCheckpointWeight = NewCheckpoint.CheckpointWeight;
    ActiveMap = Class'Engine'.static.GetCurrentWorldInfo().GetMapName();
    ActivePlayerController = PC;
    GetMapAndCheckpointIndex(ActiveMap, ActiveCheckpoint, NewMapIndex, NewCheckpointIndex);
    GetMapAndCheckpointIndex(LastSavedMap, LastSavedCheckpoint, LastSavedMapIndex, LastSavedCheckpointIndex);
    GameInfo = TdSPGame(Class'Engine'.static.GetCurrentWorldInfo().Game);
    GameInfo.SetObjective(name(NewCheckpoint.CheckpointName));
    // End:0x289
    if((!skipSaveToDisk && !(NewMapIndex < 0) || NewCheckpointIndex < 0) && (NewMapIndex > LastSavedMapIndex) || (NewMapIndex == LastSavedMapIndex) && NewCheckpointIndex > LastSavedCheckpointIndex)
    {
        OnlineSub = Class'GameEngine'.static.GetOnlineSubsystem();
        // End:0x286
        if(OnlineSub != none)
        {
            ControllerId = Class'UIInteraction'.static.GetPlayerControllerId(0);
            Profile = TdProfileSettings(OnlineSub.PlayerInterface.GetProfileSettings(byte(ControllerId)));
            // End:0x286
            if(Profile != none)
            {
                // End:0x1F8
                if(NewCheckpointIndex == 0)
                {
                    PC.ResetStats(true, false, false);
                    PC.LoadStatsFromProfile(true, false, false);
                }
                LastSavedMap = ActiveMap;
                LastSavedCheckpoint = ActiveCheckpoint;
                Profile.SetProfileSettingValue(Profile.900, ActiveMap);
                Profile.SetProfileSettingValue(Profile.901, ActiveCheckpoint);
                PC.SaveStatsToProfile(true, true, true);
                NumProfileSaveTries = 0;
                TryWriteProfile();
            }
        }        
    }
    else
    {
        PC.SaveStatsToProfile(GameInfo.bShouldSaveCheckpointProgress, GameInfo.bShouldSaveCheckpointProgress, true);
    }
    //return;    
}

function TryWriteProfile()
{
    local OnlineSubsystem OnlineSub;
    local TdProfileSettings Profile;
    local int ControllerId;

    OnlineSub = Class'GameEngine'.static.GetOnlineSubsystem();
    ControllerId = Class'UIInteraction'.static.GetPlayerControllerId(0);
    Profile = TdProfileSettings(OnlineSub.PlayerInterface.GetProfileSettings(byte(ControllerId)));
    // End:0xAC
    if((Profile.AsyncState == 2) && NumProfileSaveTries < 5)
    {
        NumProfileSaveTries++;
        ActivePlayerController.SetTimer(1, false, 'TryWriteProfile', self);
        return;
    }
    OnlineSub.PlayerInterface.AddWriteProfileSettingsCompleteDelegate(byte(ControllerId), OnProfileWriteComplete);
    // End:0x13A
    if(!OnlineSub.PlayerInterface.WriteProfileSettings(byte(ControllerId), Profile))
    {
        OnlineSub.PlayerInterface.ClearWriteProfileSettingsCompleteDelegate(byte(ControllerId), OnProfileWriteComplete);
    }
    //return;    
}

private final function OnProfileWriteComplete(bool bSuccess)
{
    local OnlineSubsystem OnlineSub;

    OnlineSub = Class'GameEngine'.static.GetOnlineSubsystem();
    OnlineSub.PlayerInterface.ClearWriteProfileSettingsCompleteDelegate(0, OnProfileWriteComplete);
    // End:0x4C
    if(bSuccess)
    {        
    }
    else
    {
        ShowErrorMessageBox();
    }
    //return;    
}

private final function ShowErrorMessageBox()
{
    local TdGameUISceneClient SceneClient;

    SceneClient = TdGameUISceneClient(Class'UIRoot'.static.GetSceneClient());
    SceneClient.OpenMessageBox(OnShowErrorMessageBox_Init, 2);
    //return;    
}

private final function OnShowErrorMessageBox_Init(UIScene OpenedScene, bool bInitialActivation)
{
    local TdUIScene_MessageBox ErrorMessageBox;
    local TdGameUISceneClient SceneClient;

    ErrorMessageBox = TdUIScene_MessageBox(OpenedScene);
    SceneClient = TdGameUISceneClient(Class'UIRoot'.static.GetSceneClient());
    ErrorMessageBox.bPauseGameWhileActive = true;
    SceneClient.RequestPausedUpdate();
    ErrorMessageBox.DisplayAcceptCancelRetryBox("<Strings:TdGameUI.TdMessageBox.FailedToWriteProfileProgress_Message>", "<Strings:TdGameUI.TdMessageBox.FailedToWriteProfile_Title>", OnErrorMessageBoxClosed);
    //return;    
}

private final function OnErrorMessageBoxClosed(TdUIScene_MessageBox InMessageBox, int SelectedOption, int PlayerIndex)
{
    switch(SelectedOption)
    {
        // End:0x16
        case 1:
            TryWriteProfile();
        // End:0xFFFF
        default:
            //return;
            break;
    }    
}

function ClearGameProgress()
{
    local OnlineSubsystem OnlineSub;
    local TdProfileSettings Profile;

    LastSavedMap = "";
    LastSavedCheckpoint = "";
    ActiveCheckpoint = "";
    ActiveMap = "";
    OnlineSub = Class'GameEngine'.static.GetOnlineSubsystem();
    // End:0xD9
    if(OnlineSub != none)
    {
        Profile = TdProfileSettings(OnlineSub.PlayerInterface.GetProfileSettings(byte(Class'UIInteraction'.static.GetPlayerControllerId(0))));
        // End:0xD9
        if(Profile != none)
        {
            Profile.SetProfileSettingValue(Profile.900, ActiveMap);
            Profile.SetProfileSettingValue(Profile.901, ActiveCheckpoint);
        }
    }
    //return;    
}

private final event GetMapAndCheckpointIndex(string MapName, string CheckpointName, out int MapIndex, out int CheckpointIndex)
{
    local int Idx, Idy;

    MapIndex = -1;
    CheckpointIndex = -1;
    // End:0x24
    if(CachedCheckpointInformation.Length == 0)
    {
        return;
    }
    Idx = 0;
    J0x2B:

    // End:0xE3 [Loop If]
    if(Idx < CachedCheckpointInformation.Length)
    {
        // End:0xD9
        if(CachedCheckpointInformation[Idx].MapName ~= MapName)
        {
            MapIndex = Idx;
            // End:0x74
            if(CheckpointName == "")
            {
                return;
            }
            Idy = 0;
            J0x7B:

            // End:0xD9 [Loop If]
            if(Idy < CachedCheckpointInformation[Idx].Checkpoints.Length)
            {
                // End:0xCF
                if(CachedCheckpointInformation[Idx].Checkpoints[Idy] ~= CheckpointName)
                {
                    CheckpointIndex = Idy;
                    return;
                }
                ++Idy;
                // [Loop Continue]
                goto J0x7B;
            }
        }
        ++Idx;
        // [Loop Continue]
        goto J0x2B;
    }
    //return;    
}

function CacheCheckpointInformation(UIDataStore_TdGameData TdGameData)
{
    local int NumMaps, NumCheckPoints, MapId, CheckpointId;
    local string CheckpointName;

    NumMaps = TdGameData.GetProviderCount('TdMaps', true);
    // End:0x36
    if(CachedCheckpointInformation.Length == NumMaps)
    {
        return;
    }
    CachedCheckpointInformation.Length = NumMaps;
    MapId = 0;
    J0x49:

    // End:0x117 [Loop If]
    if(MapId < NumMaps)
    {
        NumCheckPoints = TdGameData.GetMapNumCheckpoints(MapId);
        CachedCheckpointInformation[MapId].MapName = TdGameData.GetFileNameFromMapIndex(MapId);
        CheckpointId = 0;
        J0xAE:

        // End:0x10D [Loop If]
        if(CheckpointId < NumCheckPoints)
        {
            CheckpointName = TdGameData.GetCheckpointNameFromIndexAndMap(MapId, CheckpointId);
            CachedCheckpointInformation[MapId].Checkpoints[CheckpointId] = CheckpointName;
            ++CheckpointId;
            // [Loop Continue]
            goto J0xAE;
        }
        ++MapId;
        // [Loop Continue]
        goto J0x49;
    }
    //return;    
}
