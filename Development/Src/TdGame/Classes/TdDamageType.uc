/*******************************************************************************
 * TdDamageType generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdDamageType extends DamageType
    abstract;

var editinline array<editinline name> PhysicsImpactSpringList;
var editinline array<editinline name> PhysicsBodyImpactBoneList;
var float PhysicsHitReactionBlendOutTime;
var float PhysicsHitReactionDuration;
var Vector2D PhysHitReactionMotorStrength;
var Vector2D PhysHitReactionSpringStrength;
var bool bCausePhysicalHitReaction;

defaultproperties
{
    PhysicsBodyImpactBoneList(0)=Spine
    PhysicsBodyImpactBoneList(1)=Spine1
    PhysicsBodyImpactBoneList(2)=Spine2
    PhysicsBodyImpactBoneList(3)=Neck
    PhysicsBodyImpactBoneList(4)=LeftShoulder
    PhysicsBodyImpactBoneList(5)=LeftArm
    PhysicsBodyImpactBoneList(6)=LeftForeArm
    PhysicsBodyImpactBoneList(7)=LeftHand
    PhysicsBodyImpactBoneList(8)=RightShoulder
    PhysicsBodyImpactBoneList(9)=RightArm
    PhysicsBodyImpactBoneList(10)=RightForeArm
    PhysicsBodyImpactBoneList(11)=RightHand
    PhysicsHitReactionBlendOutTime=0.3
    PhysicsHitReactionDuration=0.4
    PhysHitReactionMotorStrength=(X=20000,Y=20000)
    bCausePhysicalHitReaction=true
    bNeverGibs=true
    KDamageImpulse=50000
    KDeathUpKick=50
    KImpulseRadius=0
    begin object name=ForceFeedbackWaveform0 class=ForceFeedbackWaveform
        Samples=/* Array type was not detected. */
    object end
    // Reference: ForceFeedbackWaveform'Default__TdDamageType.ForceFeedbackWaveform0'
    DamagedFFWaveform=ForceFeedbackWaveform0
    begin object name=ForceFeedbackWaveform1 class=ForceFeedbackWaveform
        Samples=/* Array type was not detected. */
    object end
    // Reference: ForceFeedbackWaveform'Default__TdDamageType.ForceFeedbackWaveform1'
    KilledFFWaveform=ForceFeedbackWaveform1
}