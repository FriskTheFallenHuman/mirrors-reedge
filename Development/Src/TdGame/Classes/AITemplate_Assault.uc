/*******************************************************************************
 * AITemplate_Assault generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class AITemplate_Assault extends AITemplate_Default
    native
    config(AITemplates)
    editinlinenew;

defaultproperties
{
    ControllerClass="TdGame.TdAI_Assault"
    AnimationSets="AS_AI_Assault_TwoHanded.AS_AI_Assault_TwoHanded"
    SkeletalMesh="CH_TKY_Cop_SWAT.CH_TKY_Cop_SWAT"
    LegOffsetRunRight90=0
    LegOffsetRunRight180=0
    bEnableInverseKinematics=true
    bEnableMeleePose=true
    MainWeaponAmmoDrops_Dropped=(Easy=24,Medium=24,Hard=24)
    MainWeaponAmmoDrops_Disarmed=(Easy=24,Medium=24,Hard=24)
    DisarmWindow=0.28
    MeleePredictionTimeMin=0.08
    MeleePredictionTimeMax=0.38
}