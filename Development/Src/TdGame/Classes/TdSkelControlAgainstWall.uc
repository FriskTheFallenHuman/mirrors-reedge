/*******************************************************************************
 * TdSkelControlAgainstWall generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdSkelControlAgainstWall extends SkelControlLimb
    native
    hidecategories(Object);

var() bool bLeftHand;
var() Vector MinLocation;
var() Vector MaxLocation;
var() Vector TargetLocation;
var() Vector HandOffset;

defaultproperties
{
    MinLocation=(X=5,Y=-170,Z=10)
    MaxLocation=(X=35,Y=-100,Z=30)
    HandOffset=(X=0,Y=0,Z=-20)
    EffectorLocationSpace=BCS_ComponentSpace
    JointTargetLocationSpace=BCS_ParentBoneSpace
}