/*******************************************************************************
 * TdUIKeyBindingList generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUIKeyBindingList extends TdUIWidgetList
    native
    hidecategories(Object,UIRoot,Object);

struct native BindKeyData
{
    var name KeyName;
    var string Command;
    var PlayerInput PInput;
    var name PreviousBinding;
    var bool bBindIsPrimary;

    structdefaultproperties
    {
        KeyName=None
        Command=""
        PInput=none
        PreviousBinding=None
        bBindIsPrimary=false
    }
};

var transient BindKeyData CurrKeyBindData;
var transient int NumButtons;
var transient bool bCurrentlyBindingKey;
var transient UIObject CurrentlyBindingObject;
var transient TdUIScene_MessageBox BindKeyMessageBox;
var transient array<string> CurrentBindings;
var transient array<string> StoredBindings;
var transient array<string> LocalizedFriendlyNames;

// Export UTdUIKeyBindingList::execRefreshBindingLabels(FFrame&, void* const)
native function RefreshBindingLabels();

// Export UTdUIKeyBindingList::execGetBindKeyFromCommand(FFrame&, void* const)
native function string GetBindKeyFromCommand(PlayerInput PInput, string Command, out int StartIdx);

event PostInitialize()
{
    local int ObjectIdx, Idx;

    super.PostInitialize();
    StoredBindings.Length = 0;
    Idx = 0;
    J0x15:

    // End:0x55 [Loop If]
    if(Idx < CurrentBindings.Length)
    {
        StoredBindings.Length = Idx + 1;
        StoredBindings[Idx] = CurrentBindings[Idx];
        Idx++;
        // [Loop Continue]
        goto J0x15;
    }
    ObjectIdx = 0;
    J0x5C:

    // End:0xCA [Loop If]
    if(ObjectIdx < GeneratedObjects.Length)
    {
        GeneratedObjects[ObjectIdx].OptionObj.__OnClicked__Delegate = OnClicked;
        GeneratedObjects[ObjectIdx].OptionObj.__OnRawInputKey__Delegate = OnButton_InputKey;
        ObjectIdx++;
        // [Loop Continue]
        goto J0x5C;
    }
    //return;    
}

event PlayerInput GetPlayerInput()
{
    local LocalPlayer LP;
    local PlayerInput Result;

    Result = none;
    LP = GetPlayerOwner();
    // End:0x55
    if((LP != none) && LP.Actor != none)
    {
        Result = LP.Actor.PlayerInput;
    }
    return Result;
    //return ReturnValue;    
}

function ReloadDefaults()
{
    Class'TdProfileSettings'.static.ResetKeysToDefault(GetPlayerOwner());
    RefreshBindingLabels();
    //return;    
}

function bool OnButton_InputKey(const out InputEventParameters EventParms)
{
    local bool bResult;

    bResult = false;
    // End:0x6B
    if((EventParms.EventType == 1) && EventParms.InputKeyName == 'XboxTypeS_A')
    {
        OnAcceptOptions(self, EventParms.PlayerIndex);
        bResult = true;
    }
    return bResult;
    //return ReturnValue;    
}

function ClickedScrollZone(UIScrollbar Sender, float PositionPerc, int PlayerIndex)
{
    local int MouseX, MouseY;
    local float MarkerPosition;
    local bool bDecrement;
    local int NewTopItem;

    // End:0xA7
    if(GetCursorPosition(MouseX, MouseY))
    {
        MarkerPosition = Sender.GetMarkerButtonPosition();
        bDecrement = ((Sender.ScrollbarOrientation == 1) ? float(MouseY) < MarkerPosition : float(MouseX) < MarkerPosition);
        NewTopItem = ((bDecrement) ? CurrentIndex - (MaxVisibleItems - 1) : CurrentIndex + (MaxVisibleItems - 1));
        SelectItem(NewTopItem);
    }
    //return;    
}

function bool ScrollVertical(UIScrollbar Sender, float PositionChange, optional bool bPositionMaxed)
{
    bPositionMaxed = false;
    SelectItem(int(float(CurrentIndex) + (PositionChange * float(2))));
    return true;
    //return ReturnValue;    
}

function SelectNextItem(optional bool bWrap)
{
    local int TargetIndex;

    bWrap = false;
    TargetIndex = CurrentIndex + 2;
    // End:0x34
    if(bWrap)
    {
        TargetIndex = Percent_IntInt(TargetIndex, GeneratedObjects.Length);
    }
    SelectItem(TargetIndex);
    //return;    
}

function SelectPreviousItem(optional bool bWrap)
{
    local int TargetIndex;

    bWrap = false;
    TargetIndex = CurrentIndex - 2;
    // End:0x39
    if(bWrap && TargetIndex < 0)
    {
        TargetIndex = GeneratedObjects.Length - 1;
    }
    SelectItem(TargetIndex);
    //return;    
}

function bool BindingsHaveChanged()
{
    local int Idx;

    Idx = 0;
    J0x07:

    // End:0x42 [Loop If]
    if(Idx < CurrentBindings.Length)
    {
        // End:0x38
        if(name(StoredBindings[Idx]) != name(CurrentBindings[Idx]))
        {
            return true;
        }
        Idx++;
        // [Loop Continue]
        goto J0x07;
    }
    return false;
    //return ReturnValue;    
}

function bool IsAlreadyBound(name KeyName)
{
    local int Idx;

    // End:0x4D
    if(KeyName != 'None')
    {
        Idx = 0;
        J0x1A:

        // End:0x4D [Loop If]
        if(Idx < CurrentBindings.Length)
        {
            // End:0x43
            if(KeyName == name(CurrentBindings[Idx]))
            {
                return true;
            }
            Idx++;
            // [Loop Continue]
            goto J0x1A;
        }
    }
    return false;
    //return ReturnValue;    
}

function SpawnBindStompWarningMessage()
{
    //return;    
}

function OnMessageBox_BindOverwriteConfirm(TdUIScene_MessageBox MessageBox, int SelectedItem, int PlayerIndex)
{
    // End:0x18
    if(SelectedItem == 1)
    {
        BindKey();        
    }
    else
    {
        CancelKeyBind();
    }
    //return;    
}

function AttemptKeyBind()
{
    // End:0x33
    if(CurrKeyBindData.KeyName == CurrKeyBindData.PreviousBinding)
    {
        FinishKeyBinding(false);        
    }
    else
    {
        // End:0x5E
        if(IsAlreadyBound(CurrKeyBindData.KeyName))
        {
            FinishKeyBinding(true);            
        }
        else
        {
            BindKey();
            FinishKeyBinding(false);
        }
    }
    //return;    
}

function BindKey()
{
    local int StartIdx;
    local KeyBind NewKeyBind;

    UnbindKey(CurrKeyBindData.PreviousBinding);
    UnbindKey(CurrKeyBindData.KeyName);
    NewKeyBind.Name = CurrKeyBindData.KeyName;
    NewKeyBind.Command = CurrKeyBindData.Command;
    // End:0xC9
    if(CurrKeyBindData.bBindIsPrimary)
    {
        CurrKeyBindData.PInput.Bindings[CurrKeyBindData.PInput.Bindings.Length] = NewKeyBind;        
    }
    else
    {
        StartIdx = -1;        
        GetBindKeyFromCommand(CurrKeyBindData.PInput, CurrKeyBindData.Command, StartIdx);
        // End:0x141
        if(StartIdx > -1)
        {
            CurrKeyBindData.PInput.Bindings.InsertItem(StartIdx, NewKeyBind);            
        }
        else
        {
            CurrKeyBindData.PInput.Bindings[CurrKeyBindData.PInput.Bindings.Length] = NewKeyBind;
        }
    }
    CurrKeyBindData.PInput.SaveConfig();
    FinishBinding();
    //return;    
}

function CancelKeyBind()
{
    FinishBinding();
    //return;    
}

function UnbindKey(name BindName)
{
    local PlayerInput PInput;
    local int BindingIdx;

    PInput = GetPlayerInput();
    BindingIdx = 0;
    J0x17:

    // End:0x7E [Loop If]
    if(BindingIdx < PInput.Bindings.Length)
    {
        // End:0x74
        if(PInput.Bindings[BindingIdx].Name == BindName)
        {
            PInput.Bindings.Remove(BindingIdx, 1);
            // [Explicit Break]
            goto J0x7E;
        }
        BindingIdx++;
        // [Loop Continue]
        goto J0x17;
    }
    J0x7E:

    //return;    
}

function bool OnClicked(UIScreenObject Sender, int PlayerIndex)
{
    local bool bResult;
    local UILabelButton BindingButton;
    local int ObjectIdx;

    // End:0x95
    if((CurrentlyBindingObject == none) && BindKeyMessageBox == none)
    {
        bCurrentlyBindingKey = true;
        CurrentlyBindingObject = UIObject(Sender);
        BindingButton = UILabelButton(CurrentlyBindingObject);
        // End:0x8D
        if(BindingButton != none)
        {
            ObjectIdx = GetObjectInfoIndexFromObject(CurrentlyBindingObject);
            // End:0x8D
            if(ObjectIdx != -1)
            {
                CurrKeyBindData.PreviousBinding = name(CurrentBindings[ObjectIdx]);
            }
        }
        bResult = true;
    }
    return bResult;
    //return ReturnValue;    
}

function bool OnBindKeyMessageBox_HandleInputKey(const out InputEventParameters EventParms)
{
    return HandleInputKey(EventParms);
    //return ReturnValue;    
}

function FinishKeyBinding(bool bPromptForBindStomp)
{
    BindKeyMessageBox.__OnMBInputKey__Delegate = None;
    BindKeyMessageBox.Close();
    BindKeyMessageBox = none;
    // End:0x4D
    if(bPromptForBindStomp)
    {
        SpawnBindStompWarningMessage();        
    }
    else
    {
        FinishBinding();
    }
    //return;    
}

function FinishBinding()
{
    bCurrentlyBindingKey = false;
    CurrentlyBindingObject = none;
    CurrKeyBindData.Command = "";
    CurrKeyBindData.KeyName = 'None';
    CurrKeyBindData.PreviousBinding = 'None';
    CurrKeyBindData.PInput = none;
    RefreshBindingLabels();
    //return;    
}

function bool HandleInputKey(const out InputEventParameters EventParms)
{
    local bool bResult;
    local PlayerInput PInput;
    local int ObjectIdx;
    local UIDataProvider_TdKeyBinding KeyBindingProvider;

    // End:0x19F
    if(bCurrentlyBindingKey)
    {
        // End:0x50
        if(EventParms.InputKeyName == 'Escape')
        {
            // End:0x4D
            if(EventParms.EventType == 1)
            {
                FinishKeyBinding(false);
            }            
        }
        else
        {
            // End:0x197
            if(EventParms.EventType == 1)
            {
                // End:0x197
                if(!IsConsole(2) && !TdUIScene(GetScene()).IsControllerInput(EventParms.InputKeyName))
                {
                    ObjectIdx = GetObjectInfoIndexFromObject(CurrentlyBindingObject);
                    // End:0x197
                    if(ObjectIdx != -1)
                    {
                        KeyBindingProvider = UIDataProvider_TdKeyBinding(GeneratedObjects[ObjectIdx].OptionProvider);
                        // End:0x197
                        if(KeyBindingProvider != none)
                        {
                            PInput = GetPlayerInput();
                            // End:0x197
                            if(PInput != none)
                            {
                                CurrKeyBindData.KeyName = EventParms.InputKeyName;
                                CurrKeyBindData.Command = KeyBindingProvider.Command;
                                CurrKeyBindData.PInput = PInput;
                                CurrKeyBindData.bBindIsPrimary = Percent_IntInt(ObjectIdx, 2) == 0;
                                AttemptKeyBind();
                            }
                        }
                    }
                }
            }
        }
        bResult = true;
    }
    return bResult;
    //return ReturnValue;    
}

defaultproperties
{
    NumButtons=2
    EventProvider=UIComp_Event'Default__TdUIKeyBindingList.WidgetEventComponent'
}