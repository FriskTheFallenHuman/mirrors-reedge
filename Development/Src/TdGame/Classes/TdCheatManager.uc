/*******************************************************************************
 * TdCheatManager generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdCheatManager extends CheatManager within TdPlayerController;

var Vector P1;
var Vector P2;
var Vector oldPos;
var TdAIController DebugController;
var Pawn aPawn;
var Vector OldHitLocation;
var Vector StoredPosition;
var bool bStefan;
var bool bQA;
var bool bShowTestAnimHud;
var TdAITestScripts AITests;
var float SlomoSpeed;
var Vector enemyPos;
var TdPawn ActiveActor;

function SetDebugControllers()
{
    //return;    
}

function InitAITestScripts()
{
    //return;    
}

exec function SuppressAI()
{
    //return;    
}

exec function Difficulty(int Level)
{
    //return;    
}

exec function Ammo()
{
    //return;    
}

exec function ToggleDifficultyLevel()
{
    //return;    
}

exec function SeeMe()
{
    //return;    
}

exec function Test()
{
    //return;    
}

exec function Run()
{
    //return;    
}

exec function DebugCover(bool bSelectClosestCover)
{
    //return;    
}

exec function ChangeCover(bool bSelectClosestCover)
{
    //return;    
}

exec function CoverGoToState(string iState)
{
    //return;    
}

exec function ToggleSlomo()
{
    //return;    
}

exec function PlayAnim(name AnimationName)
{
    //return;    
}

exec function hack1()
{
    //return;    
}

exec function hack2()
{
    //return;    
}

exec function Invisible()
{
    //return;    
}

exec function ToggleAIWalking()
{
    //return;    
}

exec function ToggleAIFocus()
{
    //return;    
}

exec function ToggleAICrouching()
{
    //return;    
}

exec function TestCovers()
{
    //return;    
}

exec function Idle()
{
    //return;    
}

exec function AIGotoState(name NewState, optional bool onlyFirst)
{
    onlyFirst = false;
    //return;    
}

exec function AIGod()
{
    //return;    
}

exec function SetGod()
{
    //return;    
}

exec function RemoveGod()
{
    //return;    
}

exec function God()
{
    //return;    
}

exec function Jesus()
{
    //return;    
}

exec function Stefan()
{
    //return;    
}

exec function QA()
{
    //return;    
}

exec function DebugTrace(optional int Type)
{
    Type = 0;
    //return;    
}

exec function DropMe()
{
    //return;    
}

exec function ShowClosest()
{
    //return;    
}

exec function ShowThisOne()
{
    //return;    
}

exec function ShowAll()
{
    //return;    
}

exec function pp()
{
    //return;    
}

exec function dc()
{
    //return;    
}

exec function PS()
{
    //return;    
}

exec function pe()
{
    //return;    
}

exec function AiRootRotation()
{
    //return;    
}

exec function GoHere()
{
    //return;    
}

exec function GoAngle(int Angle, float Distance)
{
    //return;    
}

exec function WalkHere()
{
    //return;    
}

exec function GoHereAll()
{
    //return;    
}

exec function MoveStraightHere()
{
    //return;    
}

exec function RunStraightHere()
{
    //return;    
}

exec function RunHere()
{
    //return;    
}

exec function HeadFocus(bool flag)
{
    //return;    
}

exec function SetHeadFocusOnPlayer()
{
    //return;    
}

exec function MoveAIHere()
{
    //return;    
}

exec function sp()
{
    //return;    
}

exec function mp()
{
    //return;    
}

exec function DebugStop()
{
    //return;    
}

exec function UpdatePath()
{
    //return;    
}

exec function ClearScreenLog()
{
    //return;    
}

exec function ScreenLogFilter(name F)
{
    //return;    
}

exec function AILogFilter(name F)
{
    //return;    
}

exec function TestReachable()
{
    //return;    
}

exec function Roll()
{
    //return;    
}

exec function SpawnAt(Vector pos)
{
    //return;    
}

exec function Training()
{
    //return;    
}

exec function Atrium()
{
    //return;    
}

exec function subway()
{
    //return;    
}

exec function Platform()
{
    //return;    
}

exec function Helicopter()
{
    //return;    
}

exec function rb()
{
    //return;    
}

exec function Boss(int stage)
{
    //return;    
}

exec function ShowAIDebug()
{
    //return;    
}

exec function TdToggleSlomo()
{
    //return;    
}

exec function FootPlacement(bool bEnable)
{
    //return;    
}

exec function AlignFootPosition(bool bEnable)
{
    //return;    
}

exec function AlignFootRotation(bool bEnable)
{
    //return;    
}

exec function InterpolateFootPosition(bool bEnable)
{
    //return;    
}

exec function FootPositionInterpolationSpeed(float Speed)
{
    //return;    
}

exec function InterpolateFootRotation(bool bEnable)
{
    //return;    
}

exec function GiveStarRating(int NumStars)
{
    //return;    
}

exec function IsLevelUnlocked(int Index)
{
    //return;    
}

exec function IsBagFound(int Index)
{
    //return;    
}

exec function SetGameProgress(string Map, string Checkpoint)
{
    //return;    
}

exec function UnlockTT(int Index)
{
    //return;    
}

exec function LockTT(int Index)
{
    //return;    
}

exec function IsAllTTStretchesUnlocked()
{
    //return;    
}

exec function LockAllTT()
{
    //return;    
}

exec function IsTTUnlocked(int Index)
{
    //return;    
}

exec function UnLockAllTT()
{
    //return;    
}

exec function ActivateSaturation()
{
    //return;    
}

exec function DeActivateSaturation()
{
    //return;    
}

exec function FindAllBags()
{
    //return;    
}

exec function FindBag(int Bag)
{
    //return;    
}

exec function UncontrolledFalling(int On)
{
    //return;    
}

exec function UncontrolledFallingClamp(float Clamp)
{
    //return;    
}

exec function Flashbang(optional int dmg)
{
    //return;    
}

exec function Taser(optional int dmg)
{
    //return;    
}

exec function Bullet(optional int dmg)
{
    //return;    
}

exec function DebugMixGroups()
{
    //return;    
}

exec function DebugVelocitySounds()
{
    //return;    
}

exec function DebugReverbVolumes()
{
    //return;    
}

exec function DumpAudioAllocations()
{
    //return;    
}

exec function UnlockAllLevels()
{
    //return;    
}

exec function LockAllLevels()
{
    //return;    
}

exec function LockLevel(int Index)
{
    //return;    
}

exec function UnlockLevel(int Index)
{
    //return;    
}

exec function CompleteLR(float Time)
{
    //return;    
}

exec function CompleteLevel(int Level)
{
    //return;    
}

exec function ShowScenes()
{
    //return;    
}

exec function CP()
{
    //return;    
}

exec function WriteTTTime(int Stretch, float Time, int NumIntermediateTimes)
{
    //return;    
}

exec function ReadTTTime(int Stretch)
{
    //return;    
}

exec function DefaultProfile()
{
    //return;    
}

exec function SaveProfile()
{
    //return;    
}

exec function KillAllButOne()
{
    //return;    
}

exec function KillAllAI()
{
    //return;    
}

exec function KillClosest()
{
    //return;    
}

private final function TdAIController GetClosestAI()
{
    //return ReturnValue;    
}

exec function DebugHalt()
{
    //return;    
}

exec function DebugLineOfSight()
{
    //return;    
}

exec function PrintLog(string iString)
{
    //return;    
}

exec function AICrouch()
{
    //return;    
}

exec function SetMeleeType(string mt)
{
    //return;    
}

exec function CoverCPOL()
{
    //return;    
}

exec function CoverDetour()
{
    //return;    
}

exec function CoverPath()
{
    //return;    
}

exec function ResetAI()
{
    //return;    
}

exec function RenameNodes()
{
    //return;    
}

exec function HeliMove(string navpointName)
{
    //return;    
}

exec function SetHeliSpeed(int I)
{
    //return;    
}

exec function Eyelids()
{
    //return;    
}

exec function DebugFalling()
{
    //return;    
}

exec function SetAimState(TdAIAnimationController.EAimState iAimState)
{
    //return;    
}

exec function AiHoldFire(bool bHoldFire)
{
    //return;    
}

exec function InvertMouseCheat()
{
    //return;    
}

exec function ChaseAI()
{
    //return;    
}

defaultproperties
{
    SlomoSpeed=1
}