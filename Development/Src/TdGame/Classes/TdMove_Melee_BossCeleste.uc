/*******************************************************************************
 * TdMove_Melee_BossCeleste generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_Melee_BossCeleste extends TdMove_PursuitMelee
    config(AIMeleeAttacks);

function bool CanDoMove()
{
    // End:0x51
    if((PawnOwner.IsInMove(69) || PawnOwner.IsInMove(35)) || PawnOwner.IsInMove(18))
    {
        return false;
    }
    return super.CanDoMove();
    //return ReturnValue;    
}

defaultproperties
{
    JumpKickAttackPropertiesE=(Damage=25,AttackSpeed=0.6,RotationLimitAngle=0,PredictionWeight=1.2,InvulnerableDamageTypes=(Dummy))
    RunAttackPropertiesE=(MissedAttackPenelty=2,Damage=20,AttackSpeed=1,RotationLimitAngle=0,PredictionTime=0.1,PredictionWeight=1.2,InvulnerableDamageTypes=(Dummy))
    StandAttackPropertiesE=(HitAngle=65,HitRange=100,MissedAttackPenelty=1,Damage=15,AttackSpeed=0.7,PredictionTime=0.15,PredictionWeight=0.8,InvulnerableDamageTypes=(Dummy))
    JumpKickAttackPropertiesN=(MissedAttackPenelty=2,Damage=25,AttackSpeed=0.6,PredictionWeight=1.2,InvulnerableDamageTypes=(Dummy))
    RunAttackPropertiesN=(MissedAttackPenelty=2,Damage=20,AttackSpeed=1,PredictionTime=0.1,PredictionWeight=1.2,InvulnerableDamageTypes=(Dummy))
    StandAttackPropertiesN=(HitAngle=65,HitRange=100,MissedAttackPenelty=1,Damage=15,AttackSpeed=0.7,PredictionTime=0.15,InvulnerableDamageTypes=(Dummy))
    JumpKickAttackPropertiesH=(MissedAttackPenelty=1,Damage=35,AttackSpeed=0.7,RotationLimitAngle=0,PredictionWeight=1)
    RunAttackPropertiesH=(MissedAttackPenelty=1,Damage=30,AttackSpeed=1.1,RotationLimitAngle=0,PredictionTime=0.1,PredictionWeight=1)
    StandAttackPropertiesH=(HitAngle=65,HitRange=100,MissedAttackPenelty=0.8,Damage=25,AttackSpeed=0.8,PredictionTime=0.15)
}