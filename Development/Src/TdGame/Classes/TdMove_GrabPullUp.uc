/*******************************************************************************
 * TdMove_GrabPullUp generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_GrabPullUp extends TdPhysicsMove
    config(PawnMovement);

enum EGrabPullUpType
{
    GPUT_IntoWalking,
    GPUT_IntoCrouch,
    GPUT_MAX
};

var TdMove_GrabPullUp.EGrabPullUpType GrabPullUpType;
var Vector FloorOverLedgeLocation;
var(Gameplay) config int GrabAllowedPullUpAngle;

function bool CanDoMove()
{
    local Rotator ControllerRotation;
    local float FindFloorDepthCheck, LedgeAndFloorDeltaHeight, MaxLedgeThickness;

    // End:0x0D
    if(!super(TdMove).CanDoMove())
    {
        return false;
    }
    // End:0x29
    if(PawnOwner.MovementState != 3)
    {
        return false;
    }
    // End:0x56
    if(!TdMove_Grab(PawnOwner.Moves[3]).CanPullUp())
    {
        return false;
    }
    ControllerRotation = Normalize(PawnOwner.Rotation - PawnOwner.Controller.Rotation);
    // End:0xBD
    if(Abs(float(ControllerRotation.Yaw)) > (float(32768 * GrabAllowedPullUpAngle) / 180))
    {
        return false;
    }
    FloorOverLedgeLocation = PawnOwner.Location;
    FloorOverLedgeLocation.Z -= PawnOwner.CylinderComponent.CollisionHeight;
    MaxLedgeThickness = 5;
    FindFloorDepthCheck = 64 + MaxLedgeThickness;
    FindFloorOverLedge(FindFloorDepthCheck, FloorOverLedgeLocation, PawnOwner.MaxStepHeight + 4);
    LedgeAndFloorDeltaHeight = FloorOverLedgeLocation.Z - PawnOwner.MoveLedgeLocation.Z;
    // End:0x1A4
    if(LedgeAndFloorDeltaHeight > -PawnOwner.MaxStepHeight)
    {
        return CanPullUp(63);        
    }
    else
    {
        // End:0x1FA
        if(!CanPullUp(90 + MaxLedgeThickness))
        {
            FloorOverLedgeLocation.Z = PawnOwner.MoveLedgeLocation.Z;
            return CanPullUp(63);
        }
        return true;
    }
    return false;
    //return ReturnValue;    
}

function bool CanPullUp(float CheckDepth)
{
    local TdMove_Grab GrabMove;
    local float HighestLocation;

    GrabMove = TdMove_Grab(PawnOwner.Moves[3]);
    HighestLocation = PawnOwner.MoveLedgeLocation.Z + (Tan(Acos(PawnOwner.MoveLedgeNormal.Z)) * PawnOwner.default.CylinderComponent.CollisionRadius);
    // End:0xA5
    if(CanHeaveOverLedgeFullyExtented(CheckDepth, FloorOverLedgeLocation, HighestLocation))
    {
        GrabPullUpType = 0;
        return true;        
    }
    else
    {
        // End:0x12E
        if((GrabMove.GetCurrentFoldedType() != 1) && CanHeaveOverLedgeCrouched(CheckDepth, FloorOverLedgeLocation, HighestLocation))
        {
            GrabPullUpType = 1;
            // End:0x12C
            if(FloorOverLedgeLocation.Z < (PawnOwner.MoveLedgeLocation.Z - PawnOwner.MaxStepHeight))
            {
                return false;
            }
            return true;
        }
    }
    return false;
    //return ReturnValue;    
}

function DrawAnimDebugInfo(HUD HUD, out float out_YL, out float out_YPos)
{
    //return;    
}

simulated function StartMove()
{
    local bool bHangingFree, bHeaveOver;
    local float TimeToReleaseCamera, TimeToEnableCollision;

    super.StartMove();
    bHeaveOver = FloorOverLedgeLocation.Z < (PawnOwner.MoveLedgeLocation.Z - PawnOwner.MaxStepHeight);
    bHangingFree = TdMove_Grab(PawnOwner.Moves[3]).IsHangingFree();
    // End:0x10B
    if(TdMove_Grab(PawnOwner.Moves[3]).GetCurrentFoldedType() == 1)
    {
        PlayMoveAnim(2, 'HangFoldedHeaveUp', 1, 0.1, 0.2, true);
        TdMove_Grab(PawnOwner.Moves[3]).SetCurrentFoldedType(0);
        TimeToReleaseCamera = 0.8;
        TimeToEnableCollision = 0.8;        
    }
    else
    {
        // End:0x1D3
        if(!bHeaveOver)
        {
            // End:0x16E
            if(bHangingFree)
            {
                PlayMoveAnim(2, ((GrabPullUpType == 1) ? 'hangfreeheaveuptocrouch' : 'HangFreeHeaveUp'), 1, 0.1, 0.2, PawnOwner.IsLocallyControlled());                
            }
            else
            {
                PlayMoveAnim(2, ((GrabPullUpType == 1) ? 'HangHeaveUpToCrouch' : 'HangHeaveUp'), 1, 0.1, 0.2, PawnOwner.IsLocallyControlled());
            }
            TimeToReleaseCamera = 0.8;
            TimeToEnableCollision = 1.4;            
        }
        else
        {
            PlayMoveAnim(2, ((bHangingFree) ? 'HangFreeHeaveOver' : 'HangHeaveOver'), 1, 0.2, 0.2, PawnOwner.IsLocallyControlled());
            TimeToReleaseCamera = ((bHangingFree) ? 0.7 : 0.6);
            TimeToEnableCollision = 0.6;
        }
    }
    PawnOwner.SetAnimationMovementState(3);
    // End:0x283
    if(GrabPullUpType == 1)
    {
        PawnOwner.SetAnimationMovementState(15, 0.2);        
    }
    else
    {
        PawnOwner.SetAnimationMovementState(0, 0.2);
    }
    bDisableFaceRotation = true;
    PawnOwner.UseRootMotion(true);
    ResetCameraLook(DisableLookTime);
    PawnOwner.Mesh.bUseLegRotationHack1 = true;
    PawnOwner.UpdateLegToWorldMatrix(rotator(-PawnOwner.MoveNormal).Yaw);
    SetMoveTimer(TimeToReleaseCamera, false, 'ReleaseCamera');
    SetMoveTimer(TimeToEnableCollision, false, 'EnableCollision');
    //return;    
}

simulated function ReleaseCamera()
{
    bDisableFaceRotation = false;
    PawnOwner.FaceRotationTimeLeft = 0.25;
    AbortLookAtTarget();
    //return;    
}

simulated function EnableCollision()
{
    PawnOwner.SetCollision(PawnOwner.bCollideActors, true);
    PawnOwner.bCollideWorld = true;
    PawnOwner.Mesh.bUseLegRotationHack1 = false;
    PawnOwner.StopIgnoreMoveInput();
    //return;    
}

simulated function StopMove()
{
    super.StopMove();
    EnableCollision();
    PawnOwner.DisableHandsWorldIK();
    //return;    
}

simulated function OnCeaseRelevantRootMotion(AnimNodeSequence SeqNode)
{
    PawnOwner.UseRootMotion(false);
    //return;    
}

simulated function OnCustomAnimEnd(AnimNodeSequence SeqNode, float PlayedTime, float ExcessTime)
{
    PawnOwner.UseRootMotion(false);
    PawnOwner.Acceleration = Normal(PawnOwner.Velocity);
    PawnOwner.SetPhysics(1);
    PawnOwner.SetMove(((GrabPullUpType == 1) ? 15 : 1));
    //return;    
}

simulated function int HandleDeath(int Damage)
{
    return PawnOwner.Health - 1;
    //return ReturnValue;    
}

function CheckForCameraCollision(Vector CameraLocation, Rotator CameraRotation)
{
    local Vector HitLocation, HitNormal, TraceEnd, TraceStart, TraceExtent;

    TraceStart = CameraLocation - (vector(PawnOwner.Controller.Rotation) * 5);
    TraceEnd = CameraLocation + (vector(PawnOwner.Controller.Rotation) * 20);
    TraceExtent = vect(2, 2, 2);
    // End:0xB4
    if(MovementTraceForBlockingEx(TraceEnd, TraceStart, TraceExtent, HitLocation, HitNormal))
    {
        PawnOwner.OffsetMeshXY(HitLocation - TraceEnd, true);
    }
    //return;    
}

defaultproperties
{
    GrabAllowedPullUpAngle=45
    PawnPhysics=PHYS_Flying
    ControllerState=PlayerWalking
    bDisableCollision=true
    bShouldHolsterWeapon=true
    bConstrainLook=true
    bDisableFaceRotation=true
    bUseCameraCollision=true
    MovementGroup=MG_NonInteractive
    AimMode=MAM_NoHands
    DisableMovementTime=-1
    DisableLookTime=0.2
    MinLookConstraint=(Pitch=0,Yaw=-10000,Roll=0)
    MaxLookConstraint=(Pitch=16384,Yaw=10000,Roll=0)
}