/*******************************************************************************
 * TdMove_BotStartRunning generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_BotStartRunning extends TdMove_BotStart
    native
    config(PawnMovement);

const cMinDistForStandToRun = 150.f;
const cMinClearDist = 150.f;

var float MoveStartedTimeStamp;
var float MinTimeBetweenTwoStartMoves;

event float TimeSinceMoveStarted()
{
    return PawnOwner.TimeSince(MoveStartedTimeStamp);
    //return ReturnValue;    
}

function bool CanDoMove()
{
    local Vector MoveDir;

    // End:0x25
    if(!BotOwner.myController.OkToDoStartMove())
    {
        return false;
    }
    // End:0x3E
    if(BotOwner.IsCrouching())
    {
        return false;
    }
    // End:0x5F
    if(PawnOwner.TimeSince(fMoveStartedTimeStamp) < MinTimeBetweenTwoStartMoves)
    {
        return false;
    }
    // End:0xCB
    if(((BotOwner.myController.Enemy == none) || !BotOwner.myController.MoveGoalIsEnemy) && BotOwner.myController.GetCurrentRouteDist() < 150)
    {
        return false;
    }
    // End:0x160
    if(((BotOwner.myController.RouteCache.Length > 0) && BotOwner.myController.RouteCache[0].bIsSpecialMove) && VSize2D(BotOwner.Location - BotOwner.myController.RouteCache[0].Location) < 150)
    {
        return false;
    }
    // End:0x18A
    if(!BotOwner.myController.EnoughSpaceForStartMove(150))
    {
        return false;
    }
    MoveDir = Normal(BotOwner.myController.Destination - BotOwner.Location);
    // End:0x201
    if(!BotOwner.myController.TdPointReachable(BotOwner.Location + (MoveDir * 150)))
    {
        return false;
    }
    return super(TdMove).CanDoMove();
    //return ReturnValue;    
}

simulated function StartMove()
{
    super.StartMove();
    MoveStartedTimeStamp = PawnOwner.WorldInfo.TimeSeconds;
    BotOwner.myAnimationController.SetUseLazySpring(false, 0);
    BotOwner.myController.HeadFocus.PushEnabled(true, Class.Name);
    //return;    
}

simulated function StopMove()
{
    super.StopMove();
    BotOwner.myAnimationController.SetUseLazySpring(true, 0);
    BotOwner.myController.HeadFocus.PopEnabled(Class.Name);
    //return;    
}

defaultproperties
{
    MoveStartedTimeStamp=-999
    MinTimeBetweenTwoStartMoves=0.5
}