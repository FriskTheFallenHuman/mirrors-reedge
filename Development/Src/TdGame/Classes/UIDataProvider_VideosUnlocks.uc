/*******************************************************************************
 * UIDataProvider_VideosUnlocks generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class UIDataProvider_VideosUnlocks extends UIDataProvider_UnlocksProvider
    transient
    native
    config(Game)
    perobjectconfig
    hidecategories(Object,UIRoot);
