/*******************************************************************************
 * UIDataProvider_TdChallengeObjectiveProvider generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class UIDataProvider_TdChallengeObjectiveProvider extends UIDataProvider_TdGameObjectiveProvider
    transient
    native
    config(Game)
    perobjectconfig
    hidecategories(Object,UIRoot);

var config int ChallengeId;
