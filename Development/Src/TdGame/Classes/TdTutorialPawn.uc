/*******************************************************************************
 * TdTutorialPawn generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdTutorialPawn extends TdPlayerPawn
    config(Game)
    hidecategories(Navigation);

var TdTutorialListener TutorialListener;

simulated event bool SetMove(TdPawn.EMovement NewMove, optional bool bViaReplication, optional bool bCheckCanDo)
{
    bViaReplication = false;
    bCheckCanDo = false;
    // End:0x55
    if(super(TdPawn).SetMove(NewMove, bViaReplication, bCheckCanDo))
    {
        // End:0x53
        if(NotEqual_InterfaceInterface(TutorialListener, TdTutorialListener(none)))
        {
            TutorialListener.OnPlayerSetMove(NewMove, self);
        }
        return true;
    }
    return false;
    //return ReturnValue;    
}

function OnTutorialEvent(int TutorialEvent)
{
    TutorialListener.OnTutorialEvent(TutorialEvent, self);
    //return;    
}

function bool Died(Controller Killer, class<DamageType> DamageType, Vector HitLocation)
{
    // End:0x21
    if(super.Died(Killer, DamageType, HitLocation))
    {
        TutorialListener = none;
        return true;
    }
    return false;
    //return ReturnValue;    
}

function TossWeapon(Weapon Weap, optional Vector ForceVelocity)
{
    TdWeapon(Weap).AmmoCount = 0;
    super(TdPawn).TossWeapon(Weap, ForceVelocity);
    //return;    
}

simulated function UpdateAnimSets(optional TdWeapon iWeapon)
{
    // End:0x215
    if(iWeapon != none)
    {
        UpdateDPG(iWeapon.Mesh);
        UpdateWeaponPoseProfile(iWeapon);
        // End:0x119
        if(Mesh1p != none)
        {
            // End:0x94
            if(iWeapon.WeaponType == 2)
            {
                Mesh1p.AnimSets[1] = CommonArmedLight1p;
                iWeapon.Mesh1p.AnimSets[0] = CommonArmedLight1p;                
            }
            else
            {
                Mesh1p.AnimSets[1] = CommonArmedHeavy1p;
                iWeapon.Mesh1p.AnimSets[0] = CommonArmedHeavy1p;
            }
            Mesh1p.AnimSets[2] = iWeapon.AnimationSetCharacter1p;
            iWeapon.Mesh1p.AnimSets[1] = iWeapon.AnimationSetCharacter1p;
        }
        // End:0x1FE
        if(Mesh3p != none)
        {
            // End:0x179
            if(iWeapon.WeaponType == 2)
            {
                Mesh3p.AnimSets[1] = CommonArmedLight3p;
                iWeapon.Mesh3p.AnimSets[0] = CommonArmedLight3p;                
            }
            else
            {
                Mesh3p.AnimSets[1] = CommonArmedHeavy3p;
                iWeapon.Mesh3p.AnimSets[0] = CommonArmedHeavy3p;
            }
            Mesh3p.AnimSets[2] = iWeapon.AnimationSetFemale3p;
            iWeapon.Mesh3p.AnimSets[1] = iWeapon.AnimationSetFemale3p;
        }
        iWeapon.UpdateAnimations();        
    }
    else
    {
        // End:0x247
        if(Mesh1p != none)
        {
            Mesh1p.AnimSets[1] = none;
            Mesh1p.AnimSets[2] = none;
        }
        // End:0x279
        if(Mesh3p != none)
        {
            Mesh3p.AnimSets[1] = none;
            Mesh3p.AnimSets[2] = none;
        }
    }
    // End:0x294
    if(Mesh1p != none)
    {
        Mesh1p.UpdateAnimations();
    }
    Mesh3p.UpdateAnimations();
    //return;    
}

defaultproperties
{
    ActorCylinderComponent=CylinderComponent'Default__TdTutorialPawn.ActorCollisionCylinder'
    begin object name=TdPawnMesh1p class=TdSkeletalMeshComponent
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdTutorialPawn.MyLightEnvironment1P'
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdTutorialPawn.TdPawnMesh1p'
    Mesh1p=TdPawnMesh1p
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdTutorialPawn.MyLightEnvironment'
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdTutorialPawn.TdPawnMesh3p'
    Mesh3p=TdPawnMesh3p
    begin object name=TdPawnMesh1pLowerBody class=TdSkeletalMeshComponent
        ParentAnimComponent=TdSkeletalMeshComponent'Default__TdTutorialPawn.TdPawnMesh1p'
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdTutorialPawn.MyLightEnvironment'
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdTutorialPawn.TdPawnMesh1pLowerBody'
    Mesh1pLowerBody=TdPawnMesh1pLowerBody
    MoveClasses(0)=none
    MoveClasses(1)=class'TdMove_Walking'
    MoveClasses(2)=class'TdMove_Falling'
    MoveClasses(3)=class'TdMove_Grab'
    MoveClasses(4)=class'TdMove_WallRun'
    MoveClasses(5)=class'TdMove_WallRun'
    MoveClasses(6)=class'TdMove_WallClimb'
    MoveClasses(7)=class'TdMove_SpringBoard'
    MoveClasses(8)=class'TdMove_SpeedVault'
    MoveClasses(9)=class'TdMove_VaultOver'
    MoveClasses(10)=class'TdMove_GrabPullUp'
    MoveClasses(11)=class'TdMove_Jump'
    MoveClasses(12)=class'TdMove_WallrunJump'
    MoveClasses(13)=class'TdMove_GrabJump'
    MoveClasses(14)=class'TdMove_IntoGrab'
    MoveClasses(15)=class'TdMove_Crouch'
    MoveClasses(16)=class'TdMove_Slide'
    MoveClasses(17)=class'TdMove_Melee'
    MoveClasses(18)=class'TdMove_Disarm_Tutorial'
    MoveClasses(19)=class'TdMove_Barge'
    MoveClasses(20)=class'TdMove_Landing'
    MoveClasses(21)=class'TdMove_Climb'
    MoveClasses(22)=class'TdMove_IntoClimb'
    MoveClasses(23)=class'TdMove_Disabled'
    MoveClasses(24)=class'TdMove_180Turn'
    MoveClasses(25)=class'TdMove_180TurnInAir'
    MoveClasses(26)=class'TdMove_LayOnGround'
    MoveClasses(27)=class'TdMove_IntoZipLine'
    MoveClasses(28)=class'TdMove_ZipLine'
    MoveClasses(29)=class'TdMove_Balance'
    MoveClasses(30)=class'TdMove_LedgeWalk'
    MoveClasses(31)=class'TdMove_GrabTransfer'
    MoveClasses(32)=class'TdMove_MeleeAir'
    MoveClasses(33)=class'TdMove_DodgeJump'
    MoveClasses(34)=class'TdMove_WallrunDodgeJump'
    MoveClasses(35)=class'TdMove_Stumble'
    MoveClasses(36)=class'TdMove_Disarmed'
    MoveClasses(37)=class'TdMove_StepUp'
    MoveClasses(38)=class'TdMove_RumpSlide'
    MoveClasses(39)=class'TdMove_Interact'
    MoveClasses(40)=class'TdMove_WallRun'
    MoveClasses(41)=none
    MoveClasses(42)=none
    MoveClasses(43)=none
    MoveClasses(44)=none
    MoveClasses(45)=none
    MoveClasses(46)=none
    MoveClasses(47)=class'TdMove_Vertigo'
    MoveClasses(48)=class'TdMove_MeleeSlide'
    MoveClasses(49)=class'TdMove_WallClimbDodgeJump'
    MoveClasses(50)=class'TdMove_WallClimb180TurnJump'
    MoveClasses(51)=class'TdMove_WallClimbDodgeJump'
    MoveClasses(52)=class'TdMove_WallClimbDodgeJump'
    MoveClasses(53)=class'TdMove_MeleeVault'
    MoveClasses(54)=none
    MoveClasses(55)=class'TdMove_StumbleHard'
    MoveClasses(56)=class'TdMove_BotRoll'
    MoveClasses(57)=none
    MoveClasses(58)=none
    MoveClasses(59)=none
    MoveClasses(60)=class'TdMove_Swing'
    MoveClasses(61)=class'TdMove_Coil'
    MoveClasses(62)=class'TdMove_MeleeWallrun'
    MoveClasses(63)=class'TdMove_MeleeCrouch'
    MoveClasses(64)=none
    MoveClasses(65)=none
    MoveClasses(66)=none
    MoveClasses(67)=none
    MoveClasses(68)=none
    MoveClasses(69)=none
    MoveClasses(70)=none
    MoveClasses(71)=class'TdMove_Disabled'
    MoveClasses(72)=class'TdMove_FallingUncontrolled'
    MoveClasses(73)=class'TdMove_SwingJump'
    MoveClasses(74)=class'TdMove_AnimationPlayback'
    MoveClasses(75)=none
    MoveClasses(76)=none
    MoveClasses(77)=none
    MoveClasses(78)=class'TdMove_SoftLanding'
    MoveClasses(79)=none
    MoveClasses(80)=none
    MoveClasses(81)=class'TdMove_AutoStepUp'
    MoveClasses(82)=class'TdMove_MeleeAirAbove'
    MoveClasses(83)=none
    MoveClasses(84)=none
    MoveClasses(85)=class'TdMove_AirBarge'
    MoveClasses(86)=none
    MoveClasses(87)=none
    MoveClasses(88)=none
    MoveClasses(89)=none
    MoveClasses(90)=none
    MoveClasses(91)=class'TdMove_SkillRoll'
    MoveClasses(92)=none
    MoveClasses(93)=class'TdMove_Cutscene'
    SceneCapture=SceneCaptureCharacterComponent'Default__TdTutorialPawn.SceneCaptureCharacterComponent0'
    DrawFrustum=DrawFrustumComponent'Default__TdTutorialPawn.DrawFrust0'
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdTutorialPawn.MyLightEnvironment'
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdTutorialPawn.TdPawnMesh3p'
    Mesh=TdPawnMesh3p
    CylinderComponent=CylinderComponent'Default__TdTutorialPawn.CollisionCylinder'
    Components(0)=SceneCaptureCharacterComponent'Default__TdTutorialPawn.SceneCaptureCharacterComponent0'
    Components(1)=DrawFrustumComponent'Default__TdTutorialPawn.DrawFrust0'
    Components(2)=CylinderComponent'Default__TdTutorialPawn.CollisionCylinder'
    Components(3)=ArrowComponent'Default__TdTutorialPawn.Arrow'
    Components(4)=DynamicLightEnvironmentComponent'Default__TdTutorialPawn.MyLightEnvironment'
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdTutorialPawn.MyLightEnvironment'
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdTutorialPawn.TdPawnMesh3p'
    Components(5)=TdPawnMesh3p
    Components(6)=CylinderComponent'Default__TdTutorialPawn.CollisionCylinder'
    Components(7)=CylinderComponent'Default__TdTutorialPawn.ActorCollisionCylinder'
    Components(8)=DynamicLightEnvironmentComponent'Default__TdTutorialPawn.MyLightEnvironment1P'
    CollisionComponent=CylinderComponent'Default__TdTutorialPawn.CollisionCylinder'
}