/*******************************************************************************
 * TdLobbyBackend generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdLobbyBackend extends Object
    abstract
    native;

var protected transient TdLobbyEventListener Listener;

function SetListener(TdLobbyEventListener InListener)
{
    Listener = InListener;
    //return;    
}

function Update()
{
    //return;    
}

function Cleanup()
{
    //return;    
}

function OnPlayerLogin(Controller C)
{
    //return;    
}

function OnPlayerLogout(Controller C)
{
    //return;    
}

function OnChangeTeam(Controller C)
{
    //return;    
}

function OnChangeRole(Controller C)
{
    //return;    
}

function OnSetReady(Controller C)
{
    //return;    
}

function OnGameStarted()
{
    //return;    
}

function RequestTeamChange(Controller C, int Dir)
{
    //return;    
}

function RequestRoleChange(Controller C, int Dir)
{
    //return;    
}

function RequestSetReady(Controller C)
{
    //return;    
}

function bool AllPlayersReady()
{
    //return ReturnValue;    
}

function StartGame(string MapName, string GameMode)
{
    //return;    
}
