/*******************************************************************************
 * TdSkelControlSpring generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdSkelControlSpring extends SkelControlSingleBone
    native
    config(Animation)
    hidecategories(Object);

var() config bool EnableVelocityDependedTranslationSpring;
var() config bool EnableOnSpecificWeaponType;
var() bool bInvertYaw;
var() bool bInvertPitch;
var() bool bInvertRoll;
var bool bInitialized;
var bool bLag;
var bool bOverlap;
var() config bool EnableAccelerationBased;
var() config bool bIsAppliedToWrist;
var bool HasReachedTargetVelocity;
var bool bDeltaIsIncreasing;
var() TdPawn.EWeaponType SpringWeaponType;
var() config float SpringYawInterpVel;
var() config float SpringPitchInterpVel;
var() config float SpringRollInterpVel;
var() config float TimeBetweenSpringUpdates;
var float PreviousYaw;
var float PreviousPitch;
var float YawTargetDiff;
var float PitchTargetDiff;
var float RollTargetDiff;
var float CurrentYawInterp;
var float CurrentPitchInterp;
var float CurrentRollInterp;
var float CurrentYawInterpVel;
var float YawDeltaTarget;
var float YawTargetSpeed;
var() config float MaxPitchDeltaOffset;
var() config float MaxYawDeltaOffset;
var() config float MaxRollDeltaOffset;
var float CurrTime;
var Rotator PreviousPlayerControllerRotation;
var Vector PreviousLinearVelocity;
var Vector PreviousAngularVelocity;
var Vector PreviousAngularAcceleration;
var Rotator PreviousDeltaRotation;
var Rotator PreviousDeltaInterpolation;
var Rotator TargetRotator;
var Rotator InterpolatedRotator;
var Rotator AngularRotationLimiter;
var Rotator PreviousInterpolatedRotator;
var Vector VelocityIncrement;
var Vector InterpolationVelocity;
var Vector TargetInterpolationVelocity;
var Vector VelocityInterpolationSpeed;
var(Lag) config Vector DefaultInterpolationVelocity;
var(Lag) config Vector DefaultVelocityIncrement;
var(Lag) config Vector DefaultVelocityDecrement;
var(Lag) config Rotator DefaultAngularRotationLimiter;
var(Lag) config Vector AngularVelocityTargetMuliplier;
var(Lag) config Vector AngularVelocityLowerThresholdMuliplier;
var(Lag) config Vector AngularVelocityDecreaseInterpSpeed;
var Rotator OverlapRotator;
var Rotator InterpolatedOverlapRotator;
var Rotator TargetOverlapRotator;
var Vector OverlapVelocity;
var(Overlap) config Vector OverlapVelocityIncrement;
var(Overlap) config Vector ThresholdSmall;
var(Overlap) config Vector ThresholdMedium;
var(Overlap) config Vector ThresholdLarge;
var(Overlap) config Vector VelocitySmall;
var(Overlap) config Vector VelocityMedium;
var(Overlap) config Vector VelocityLarge;
var(Overlap) config Rotator RotatorSmall;
var(Overlap) config Rotator RotatorMedium;
var(Overlap) config Rotator RotatorLarge;
var float ControllerTime;
var() config float ControllerTimeLimit;
var Rotator ZeroRotator;
var Vector ZeroVector;

defaultproperties
{
    EnableOnSpecificWeaponType=true
    bInvertPitch=true
    bLag=true
    bOverlap=true
    SpringWeaponType=EWT_Light
    SpringYawInterpVel=5
    SpringPitchInterpVel=10
    SpringRollInterpVel=5
    TimeBetweenSpringUpdates=0.04
    YawTargetSpeed=0.01
    MaxPitchDeltaOffset=45
    MaxYawDeltaOffset=45
    MaxRollDeltaOffset=45
    DefaultInterpolationVelocity=(X=10,Y=75,Z=0)
    DefaultVelocityIncrement=(X=5,Y=20,Z=0)
    DefaultVelocityDecrement=(X=0,Y=-4.25,Z=0)
    DefaultAngularRotationLimiter=(Pitch=7282,Yaw=4551,Roll=4551)
    AngularVelocityTargetMuliplier=(X=0,Y=1.15,Z=0)
    AngularVelocityLowerThresholdMuliplier=(X=0,Y=1.05,Z=0)
    AngularVelocityDecreaseInterpSpeed=(X=0,Y=25,Z=0)
    OverlapVelocityIncrement=(X=5,Y=15,Z=0)
    ThresholdSmall=(X=0,Y=20,Z=0)
    ThresholdMedium=(X=0,Y=40,Z=0)
    ThresholdLarge=(X=0,Y=90,Z=0)
    VelocitySmall=(X=0,Y=5,Z=0)
    VelocityMedium=(X=0,Y=25,Z=0)
    VelocityLarge=(X=0,Y=80,Z=0)
    RotatorSmall=(Pitch=0,Yaw=455,Roll=0)
    RotatorMedium=(Pitch=0,Yaw=910,Roll=0)
    RotatorLarge=(Pitch=0,Yaw=1365,Roll=0)
    ControllerTimeLimit=0.15
}