/*******************************************************************************
 * TdAttackPathNode generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdAttackPathNode extends TdConfinedVolumePathNode
    native
    hidecategories(Navigation,Lighting,LightColor,Force);

var(NavigationPoint) float AttackVolumeHeight;
var(NavigationPoint) float AttackVolumeAngle;
var(NavigationPoint) float AttackVolumeRadius;

// Export UTdAttackPathNode::execPointInside(FFrame&, void* const)
native function bool PointInside(Vector Point);

defaultproperties
{
    AttackVolumeHeight=2000
    AttackVolumeAngle=45
    AttackVolumeRadius=3000
    CylinderComponent=CylinderComponent'Default__TdAttackPathNode.CollisionCylinder'
    GoodSprite=SpriteComponent'Default__TdAttackPathNode.Sprite'
    BadSprite=SpriteComponent'Default__TdAttackPathNode.Sprite2'
    Components(0)=SpriteComponent'Default__TdAttackPathNode.Sprite'
    Components(1)=SpriteComponent'Default__TdAttackPathNode.Sprite2'
    Components(2)=ArrowComponent'Default__TdAttackPathNode.Arrow'
    Components(3)=CylinderComponent'Default__TdAttackPathNode.CollisionCylinder'
    Components(4)=PathRenderingComponent'Default__TdAttackPathNode.PathRenderer'
    Components(5)=TdAttackPathNodeRenderingComponent'Default__TdAttackPathNode.VolumeRenderer'
    CollisionComponent=CylinderComponent'Default__TdAttackPathNode.CollisionCylinder'
}