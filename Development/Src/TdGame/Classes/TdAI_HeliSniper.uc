/*******************************************************************************
 * TdAI_HeliSniper generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdAI_HeliSniper extends TdAIController
    native
    config(AI)
    hidecategories(Navigation);

// Export UTdAI_HeliSniper::execUpdateAnchor(FFrame&, void* const)
native function UpdateAnchor();

function UpdatePose()
{
    myPawn.SetPose(0);
    //return;    
}

function bool ShouldEnterMelee(float Range)
{
    return false;
    //return ReturnValue;    
}

defaultproperties
{
    Components(0)=SpriteComponent'Default__TdAI_HeliSniper.Sprite'
}