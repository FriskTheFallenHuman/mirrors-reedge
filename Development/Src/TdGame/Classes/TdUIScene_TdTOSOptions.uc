/*******************************************************************************
 * TdUIScene_TdTOSOptions generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUIScene_TdTOSOptions extends TdUIScene_SubMenu
    config(UI)
    hidecategories(Object,UIRoot,Object);

function SetupButtonBar()
{
    ButtonBar.AppendButton("Back", OnButtonBar_Back);
    ButtonBar.AppendButton("Submit", OnButtonBar_Submit);
    //return;    
}

function bool OnButtonBar_Back(UIScreenObject Sender, int PlayerIndex)
{
    OnCloseScene();
    return true;
    //return ReturnValue;    
}

function bool OnButtonBar_Submit(UIScreenObject Sender, int PlayerIndex)
{
    OnSubmit();
    return true;
    //return ReturnValue;    
}

function OnCloseScene()
{
    CloseScene(self);
    //return;    
}

function OnSubmit()
{
    local UIDataStore_TdLoginData LoginData;
    local TdLoginSettings Settings;
    local int Agreement1Value, Agreement2Value;

    LoginData = UIDataStore_TdLoginData(FindDataStore('TdLoginData'));
    // End:0x7E
    if(LoginData != none)
    {
        Settings = LoginData.LoginSettings;
        // End:0x7E
        if(Settings != none)
        {
            Settings.GetStringSettingValue(0, Agreement1Value);
            Settings.GetStringSettingValue(1, Agreement2Value);
        }
    }
    //return;    
}

function bool HandleInputKey(const out InputEventParameters EventParms)
{
    local bool bResult;

    // End:0x7E
    if(EventParms.EventType == 1)
    {
        // End:0x4E
        if(EventParms.InputKeyName == 'XboxTypeS_A')
        {
            OnSubmit();
            bResult = true;            
        }
        else
        {
            // End:0x7E
            if(EventParms.InputKeyName == 'XboxTypeS_B')
            {
                OnCloseScene();
                bResult = true;
            }
        }
    }
    return bResult;
    //return ReturnValue;    
}

defaultproperties
{
    EventProvider=UIComp_Event'Default__TdUIScene_TdTOSOptions.SceneEventComponent'
}