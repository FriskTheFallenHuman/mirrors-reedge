/*******************************************************************************
 * TdUIScene_ControlsSettingsPC generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUIScene_ControlsSettingsPC extends TdUIScene_OptionMenu
    config(UI)
    hidecategories(Object,UIRoot,Object);

function SetupButtonBar()
{
    super.SetupButtonBar();
    ButtonBar.AppendButton("<Strings:TdGameUI.TdbuttonCallouts.Controller>", OnGamepadSettingsButton_Clicked);
    ButtonBar.AppendButton("<Strings:TdGameUI.TdbuttonCallouts.KeyMappings>", OnKeyMappingsButton_Clicked);
    //return;    
}

function bool OnKeyMappingsButton_Clicked(UIScreenObject Sender, int PlayerIndex)
{
    OpenScene(Class'TdHUDContent'.static.GetUISceneByName('TdKeyMappings'));
    return true;
    //return ReturnValue;    
}

function bool OnGamepadSettingsButton_Clicked(UIScreenObject Sender, int PlayerIndex)
{
    OpenScene(Class'TdHUDContent'.static.GetUISceneByName('TdControlsSettings'));
    return true;
    //return ReturnValue;    
}

function ResetSettingsToDefault()
{
    local TdProfileSettings Profile;
    local array<int> ProfileIds;

    Profile = TdProfileSettings(GetPlayerInterface().GetProfileSettings(byte(GetPlayerOwner().ControllerId)));
    // End:0x7D
    if(Profile != none)
    {
        ProfileIds.AddItem(Profile.400);
        ProfileIds.AddItem(2);
        Profile.ResetIdsToDefault(ProfileIds);
    }
    //return;    
}

function InitializeSettings()
{
    GetTdPlayerController().SetControlBindingsProfileSettings();
    //return;    
}

function bool HandleInputKey(const out InputEventParameters EventParms)
{
    local bool bResult;

    bResult = super.HandleInputKey(EventParms);
    // End:0xDB
    if(!bResult)
    {
        // End:0xDB
        if(EventParms.EventType == 1)
        {
            // End:0x8B
            if(EventParms.InputKeyName == 'XboxTypeS_Y')
            {
                OpenScene(Class'TdHUDContent'.static.GetUISceneByName('TdControlsSettings'));
                bResult = true;                
            }
            else
            {
                // End:0xDB
                if(EventParms.InputKeyName == 'XboxTypeS_Start')
                {
                    OpenScene(Class'TdHUDContent'.static.GetUISceneByName('TdKeyMappings'));
                    bResult = true;
                }
            }
        }
    }
    return bResult;
    //return ReturnValue;    
}

event bool PlayInputKeyNotification(const out InputEventParameters EventParms)
{
    local bool bResult;

    // End:0x80
    if(EventParms.EventType == 1)
    {
        // End:0x80
        if((EventParms.InputKeyName == 'XboxTypeS_Y') || EventParms.InputKeyName == 'XboxTypeS_Start')
        {
            PlayUISound('Accept', EventParms.PlayerIndex);
            bResult = true;
        }
    }
    return bResult || super.PlayInputKeyNotification(EventParms);
    //return ReturnValue;    
}

defaultproperties
{
    EventProvider=UIComp_Event'Default__TdUIScene_ControlsSettingsPC.SceneEventComponent'
}