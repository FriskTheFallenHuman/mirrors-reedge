/*******************************************************************************
 * TdHUDContent generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdHUDContent extends Object;

var const array<Texture2D> Textures;
var const array<UIScene> Scenes;
var const array<SoundCue> SoundCues;
var const array<MultiFont> MultiFonts;

static function Texture2D GetTextureByName(name TextureName, optional string InLoaderClass)
{
    local int Idx;
    local class<TdHUDContent> LoaderClass;

    // End:0x54
    if(InLoaderClass != "")
    {
        LoaderClass = class<TdHUDContent>(DynamicLoadObject(InLoaderClass, Class'Class'));
        // End:0x52
        if(LoaderClass != none)
        {
            return LoaderClass.static.GetTextureByName(TextureName);            
        }
        else
        {
            return none;
        }
    }
    Idx = 0;
    J0x5B:

    // End:0xA0 [Loop If]
    if(Idx < default.Textures.Length)
    {
        // End:0x96
        if(default.Textures[Idx].Name == TextureName)
        {
            return default.Textures[Idx];
        }
        ++Idx;
        // [Loop Continue]
        goto J0x5B;
    }
    return none;
    //return ReturnValue;    
}

static function UIScene GetUISceneByName(name UISceneName, optional string InLoaderClass)
{
    local int Idx;
    local class<TdHUDContent> LoaderClass;

    // End:0x54
    if(InLoaderClass != "")
    {
        LoaderClass = class<TdHUDContent>(DynamicLoadObject(InLoaderClass, Class'Class'));
        // End:0x52
        if(LoaderClass != none)
        {
            return LoaderClass.static.GetUISceneByName(UISceneName);            
        }
        else
        {
            return none;
        }
    }
    Idx = 0;
    J0x5B:

    // End:0xA0 [Loop If]
    if(Idx < default.Scenes.Length)
    {
        // End:0x96
        if(default.Scenes[Idx].Name == UISceneName)
        {
            return default.Scenes[Idx];
        }
        ++Idx;
        // [Loop Continue]
        goto J0x5B;
    }
    return none;
    //return ReturnValue;    
}

static function SoundCue GetSoundCueByName(name SoundCueName, optional string InLoaderClass)
{
    local int Idx;
    local class<TdHUDContent> LoaderClass;

    // End:0x54
    if(InLoaderClass != "")
    {
        LoaderClass = class<TdHUDContent>(DynamicLoadObject(InLoaderClass, Class'Class'));
        // End:0x52
        if(LoaderClass != none)
        {
            return LoaderClass.static.GetSoundCueByName(SoundCueName);            
        }
        else
        {
            return none;
        }
    }
    Idx = 0;
    J0x5B:

    // End:0xA0 [Loop If]
    if(Idx < default.SoundCues.Length)
    {
        // End:0x96
        if(default.SoundCues[Idx].Name == SoundCueName)
        {
            return default.SoundCues[Idx];
        }
        ++Idx;
        // [Loop Continue]
        goto J0x5B;
    }
    return none;
    //return ReturnValue;    
}

static function MultiFont GetMultiFontByName(name FontName, optional string InLoaderClass)
{
    local int Idx;
    local class<TdHUDContent> LoaderClass;

    // End:0x54
    if(InLoaderClass != "")
    {
        LoaderClass = class<TdHUDContent>(DynamicLoadObject(InLoaderClass, Class'Class'));
        // End:0x52
        if(LoaderClass != none)
        {
            return LoaderClass.static.GetMultiFontByName(FontName);            
        }
        else
        {
            return none;
        }
    }
    Idx = 0;
    J0x5B:

    // End:0xA0 [Loop If]
    if(Idx < default.MultiFonts.Length)
    {
        // End:0x96
        if(default.MultiFonts[Idx].Name == FontName)
        {
            return default.MultiFonts[Idx];
        }
        ++Idx;
        // [Loop Continue]
        goto J0x5B;
    }
    return none;
    //return ReturnValue;    
}

defaultproperties
{
    Scenes(0)=TdUIScene_MiniMenu'TdUI.TdMiniMenu'
    Scenes(1)=TdUIScene_LoadIndicator'TdUI.TdLoadIndicator'
    Scenes(2)=TdUIScene_LoadIndicator'TdUI.TdDiskAccessIndicator'
    Scenes(3)=TdUIScene_PauseOptions'TdUI.TdPauseOptions'
    Scenes(4)=TdUIScene_MessageBox'TdUI.TdMessageBox'
    Scenes(5)=TdUIScene_MessageBox'TdUI.TdTinyMessageBox'
    Scenes(6)=TdUIScene_ImageMessageBox'TdUI.TdImageMessageBox'
    Scenes(7)=TdUIScene_SupersMessage'TdUI.TdSupersMessage'
    Scenes(8)=TdUIScene_TdCredits'TdUI.TdCredits'
    Scenes(9)=TdUIScene_SplashHint'TdUI_InGame.TdSplashHint'
    Scenes(10)=TdUIScene_ControlsSettings'TdUI_Options.TdControlsSettings'
    Scenes(11)=TdUIScene_ControlsSettingsPC'TdUI_Options.TdControlsSettingsPC'
    Scenes(12)=TdUIScene_ControlsSettings'TdUI_Options.TdControlsSettingsPS3'
    Scenes(13)=TdUIScene_AudioSettings'TdUI_Options.TdAudioSettings'
    Scenes(14)=TdUIScene_VideoSettings'TdUI_Options.TdVideoSettings'
    Scenes(15)=TdUIScene_VideoSettingsPC'TdUI_Options.TdVideoSettingsPC'
    Scenes(16)=TdUIScene_GameSettings'TdUI_Options.TdGameSettings'
    Scenes(17)=TdUIScene_KeyMappings'TdUI_Options.TdKeyMappings'
    MultiFonts(0)=MultiFont'UI_Fonts_Final.Helvetica_Headline_Thick_Italic'
    MultiFonts(1)=MultiFont'UI_Fonts_Final.Helvetica_Small_Normal'
    MultiFonts(2)=MultiFont'UI_Fonts_Final.Helvetica_Small_Bold'
    MultiFonts(3)=MultiFont'UI_Fonts_Final.Helvetica_Small_Bold_Italic'
    MultiFonts(4)=MultiFont'UI_Fonts_Final.Helvetica_Medium_Italic'
}