/*******************************************************************************
 * UIComp_TdDropShadowString generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class UIComp_TdDropShadowString extends UIComp_DrawString within UIObject
    native
    editinlinenew
    hidecategories(Object);

var(DropShadow) UIStyleReference DropShadowStyle;
var(DropShadow) float VerticalPctOffset;
var(DropShadow) float HorizontalPctOffset;

defaultproperties
{
    DropShadowStyle=(DefaultStyleTag=DefaultDropShadowStyle,RequiredStyleClass=Class'Engine.UIStyle_Combo',AssignedStyleID=(A=0,B=0,C=0,D=0),ResolvedStyle=none)
    VerticalPctOffset=0.06
    HorizontalPctOffset=0.06
}