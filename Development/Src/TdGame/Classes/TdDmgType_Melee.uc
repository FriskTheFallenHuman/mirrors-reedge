/*******************************************************************************
 * TdDmgType_Melee generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdDmgType_Melee extends TdDamageType;

defaultproperties
{
    bCausePhysicalHitReaction=false
    bCausesFracture=true
}