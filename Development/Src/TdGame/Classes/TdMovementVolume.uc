/*******************************************************************************
 * TdMovementVolume generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMovementVolume extends PhysicsVolume
    abstract
    native
    hidecategories(Navigation,Object,Movement,Display);

var Vector FloorNormal;
var Vector WallNormal;
var Vector MoveDirection;
var Vector Start;
var Vector End;
var private Vector Middle;
var Vector Center;
var() bool bAutoPath;
var() noimport transient bool bHideSplineMarkers;
var() bool bAllowSplineControl;
var bool bLatent;
var private noimport const transient array<TdMovementSplineMarker> SplineMarkers;
var() int NumSplineSegments;
var private editoronly Vector OldScale;
var private editoronly Vector OldLocation;
var private editoronly Rotator OldRotation;
var float SplineLength;
var array<Vector> SplineLocations;

// Export UTdMovementVolume::execGetLocationOnSpline(FFrame&, void* const)
native function Vector GetLocationOnSpline(float ParamT);

// Export UTdMovementVolume::execGetSlopeOnSpline(FFrame&, void* const)
native function Vector GetSlopeOnSpline(float ParamT);

// Export UTdMovementVolume::execFindClosestPointOnDSpline(FFrame&, void* const)
native function FindClosestPointOnDSpline(Vector InLocation, out Vector ClosestLocation, out float NParamT, optional int LowestIndexHint);

// Export UTdMovementVolume::execIsSplineMarkerSelected(FFrame&, void* const)
native function bool IsSplineMarkerSelected();

simulated event PostBeginPlay()
{
    super.PostBeginPlay();
    //return;    
}

function bool InUse(Pawn Ignored)
{
    return false;
    //return ReturnValue;    
}

simulated event PawnEnteredVolume(Pawn P)
{
    local TdPawn TdP;

    // End:0x35
    if(bLatent)
    {
        TdP = TdPawn(P);
        // End:0x35
        if(TdP != none)
        {
            TdP.ActiveMovementVolume = self;
        }
    }
    //return;    
}

simulated event PawnLeavingVolume(Pawn P)
{
    local TdPawn TdP;

    // End:0x4C
    if(bLatent)
    {
        TdP = TdPawn(P);
        // End:0x4C
        if((TdP != none) && TdP.ActiveMovementVolume == self)
        {
            TdP.ActiveMovementVolume = none;
        }
    }
    //return;    
}

simulated function PawnUpdate(TdPawn TdP)
{
    //return;    
}

defaultproperties
{
    bAutoPath=true
    bHideSplineMarkers=true
    NumSplineSegments=10
    BrushComponent=BrushComponent'Default__TdMovementVolume.BrushComponent0'
    Components(0)=BrushComponent'Default__TdMovementVolume.BrushComponent0'
    begin object name=WallDir class=ArrowComponent
        ArrowColor=(B=150,G=100,R=150,A=255)
        ArrowSize=5
    object end
    // Reference: ArrowComponent'Default__TdMovementVolume.WallDir'
    Components(1)=WallDir
    begin object name=MovementMesh class=TdMoveVolumeRenderComponent
        bUseAsOccluder=false
        bUsePrecomputedShadows=false
    object end
    // Reference: TdMoveVolumeRenderComponent'Default__TdMovementVolume.MovementMesh'
    Components(2)=MovementMesh
    RemoteRole=ROLE_SimulatedProxy
    CollisionComponent=BrushComponent'Default__TdMovementVolume.BrushComponent0'
}