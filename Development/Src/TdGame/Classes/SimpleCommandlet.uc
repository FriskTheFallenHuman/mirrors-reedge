/*******************************************************************************
 * SimpleCommandlet generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class SimpleCommandlet extends Commandlet
    transient;

var int intparm;

function int TestFunction()
{
    return 666;
    //return ReturnValue;    
}

function int Main(string Parms)
{
    //return ReturnValue;    
}
