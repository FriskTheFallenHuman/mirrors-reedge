/*******************************************************************************
 * TdCopStashpoint generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdCopStashpoint extends TdStashpoint
    config(Game)
    placeable
    hidecategories(Navigation);

var transient TdPawn StashingPawn;

auto state Idle
{
    singular event Touch(Actor Other, PrimitiveComponent OtherComp, Vector HitLocation, Vector HitNormal)
    {
        local TdPlayerPawn MyTdPawn;

        MyTdPawn = TdPlayerPawn(Other);
        // End:0x5E
        if(((MyTdPawn == none) || !MyTdPawn.IsCarryingBag()) || !IsTeamTerritory(MyTdPawn.PlayerReplicationInfo.Team))
        {
            return;
        }
        StashingPawn = MyTdPawn;
        InitiateStashing();
        //return;        
    }
Begin:

    StashingPawn = none;
    stop;                    
}

state Stashing
{
    singular event UnTouch(Actor Other)
    {
        local TdPlayerPawn MyTdPawn;

        MyTdPawn = TdPlayerPawn(Other);
        // End:0x3E
        if((MyTdPawn != none) && MyTdPawn.IsCarryingBag())
        {
            InterceptStashing();
        }
        //return;        
    }

    singular event Touch(Actor Other, PrimitiveComponent OtherComp, Vector HitLocation, Vector HitNormal)
    {
        local TdPawn InterceptingPawn;

        InterceptingPawn = TdPawn(Other);
        // End:0x6D
        if((InterceptingPawn != none) && InterceptingPawn != StashingPawn)
        {
            // End:0x6D
            if(InterceptingPawn.PlayerReplicationInfo.Team != StashingPawn.PlayerReplicationInfo.Team)
            {
                InterceptStashing();
            }
        }
        //return;        
    }
Begin:

    TimeOfStashingInitiated = WorldInfo.TimeSeconds;
    SetTimer(1, true, 'NotifyProgressed');
    SetTimer(GetDuration(), false, 'CompleteStashing');
    stop;                
}

defaultproperties
{
    TerritoryOfTeam=1
    bNotifyKismet=false
    bNoDelete=false
    begin object name=CollisionCylinder class=CylinderComponent
        CollisionHeight=100
        CollisionRadius=30
        CollideActors=true
    object end
    // Reference: CylinderComponent'Default__TdCopStashpoint.CollisionCylinder'
    Components(0)=CollisionCylinder
    begin object name=CollisionCylinder class=CylinderComponent
        CollisionHeight=100
        CollisionRadius=30
        CollideActors=true
    object end
    // Reference: CylinderComponent'Default__TdCopStashpoint.CollisionCylinder'
    CollisionComponent=CollisionCylinder
}