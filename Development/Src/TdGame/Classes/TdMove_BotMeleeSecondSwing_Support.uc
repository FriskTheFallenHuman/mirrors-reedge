/*******************************************************************************
 * TdMove_BotMeleeSecondSwing_Support generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_BotMeleeSecondSwing_Support extends TdMove_BotMeleeSecondSwing
    config(AIMeleeAttacks);

defaultproperties
{
    GenericAttackProperties=(Damage=60)
}