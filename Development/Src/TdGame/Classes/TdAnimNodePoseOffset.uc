/*******************************************************************************
 * TdAnimNodePoseOffset generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdAnimNodePoseOffset extends AnimNodeBlendBase
    native
    hidecategories(Object,Object);

struct native PoseProfile
{
    var name Name;
    var bool bIsValid;
    var array<int> BoneIndices;
    var array<Matrix> MatrixTransforms;

    structdefaultproperties
    {
        Name=None
        bIsValid=false
        BoneIndices=none
        MatrixTransforms=none
    }
};

var array<PoseProfile> Profiles;
var PoseProfile ActivePoseProfile;
var(GeneralSettings) transient bool bDisable;
var(EditorSettings) transient bool BuildPoseOffsets;
var(EditorSettings) editoronly int ActivePoseProfileIndex;

event EditorProfileUpdated(name ProfileName)
{
    SetActiveProfileByName(ProfileName);
    //return;    
}

simulated function SetActiveProfileByName(name ProfileName)
{
    local int Index;
    local bool bFound;

    bFound = false;
    Index = 0;
    J0x0F:

    // End:0x70 [Loop If]
    if(Index < Profiles.Length)
    {
        // End:0x66
        if(Profiles[Index].Name == ProfileName)
        {
            bFound = true;
            ActivePoseProfileIndex = Index;
            ActivePoseProfile = Profiles[Index];
            // [Explicit Break]
            goto J0x70;
        }
        Index++;
        // [Loop Continue]
        goto J0x0F;
    }
    J0x70:

    // End:0x8F
    if(!bFound)
    {
        ActivePoseProfileIndex = 0;
        ActivePoseProfile = Profiles[0];
    }
    //return;    
}

defaultproperties
{
    Children=/* Array type was not detected. */
    bFixNumChildren=true
}