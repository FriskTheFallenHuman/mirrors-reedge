/*******************************************************************************
 * TdMove_Interact generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_Interact extends TdPhysicsMove
    config(PawnMovement);

var float AnimationInteractionDelay;
var private float EventDelay;
var private TdTrigger CurrentTrigger;
var private SeqEvent_TdUsed CurrentEvent;
var Vector MoveLocation;
var Vector NormalFromTrigger2D;
var config float DistanceToAButton;
var config float DistanceToAValve;
var private float DistanceToInteractableObject;
var bool bReverseRev;
var bool bReachedTrigger;

function bool CanDoMove()
{
    local Vector X, Y, Z, DeltaPosition;
    local float DegDiff;

    // End:0x0D
    if(!super(TdMove).CanDoMove())
    {
        return false;
    }
    // End:0x27
    if((CurrentTrigger == none) || CurrentEvent == none)
    {
        return false;
    }
    // End:0x43
    if(PawnOwner.MovementState != 1)
    {
        return false;
    }
    PawnOwner.GetAxes(CurrentTrigger.Rotation, X, Y, Z);
    DeltaPosition = CurrentTrigger.Location - PawnOwner.Location;
    DeltaPosition.Z = 0;
    DeltaPosition = Normal(DeltaPosition);
    DegDiff = vector(PawnOwner.Rotation) Dot -Y;
    DegDiff = (Acos(DegDiff) * 180) / 3.141593;
    // End:0x118
    if(Abs(DegDiff) > (CurrentTrigger.AngleLimit / float(2)))
    {
        return false;
    }
    // End:0x169
    if((CurrentTrigger.TriggerType == 0) || CurrentTrigger.TriggerType == 2)
    {
        NormalFromTrigger2D = Normal(Y);
        DistanceToInteractableObject = DistanceToAButton;        
    }
    else
    {
        // End:0x19B
        if(CurrentTrigger.TriggerType == 1)
        {
            NormalFromTrigger2D = Normal(Y);
            DistanceToInteractableObject = DistanceToAValve;
        }
    }
    NormalFromTrigger2D.Z = 0;
    MoveLocation = CurrentTrigger.Location + (NormalFromTrigger2D * DistanceToInteractableObject);
    MoveLocation.Z = PawnOwner.Location.Z;
    // End:0x22E
    if(MovementTraceForBlocking(MoveLocation, PawnOwner.Location, PawnOwner.GetCollisionExtent()))
    {
        return false;
    }
    return true;
    //return ReturnValue;    
}

simulated function ReleaseCurrentTriggerAndEvent()
{
    CurrentTrigger = none;
    CurrentEvent = none;
    //return;    
}

simulated function SetCurrentTrigger(TdTrigger ActiveTrigger)
{
    CurrentTrigger = ActiveTrigger;
    // End:0x3E
    if(CurrentTrigger.GetRevCount() == CurrentTrigger.NumberOfRevs)
    {
        bReverseRev = true;        
    }
    else
    {
        bReverseRev = false;
    }
    //return;    
}

simulated function SetCurrentEvent(SeqEvent_TdUsed ActiveUsedEvent)
{
    CurrentEvent = ActiveUsedEvent;
    CurrentEvent.bInteract = true;
    //return;    
}

private final simulated function UpdateEventDelay()
{
    // End:0x44
    if((CurrentTrigger.TriggerType == 0) || CurrentTrigger.TriggerType == 2)
    {
        AnimationInteractionDelay = 0.6;        
    }
    else
    {
        AnimationInteractionDelay = 0;
    }
    EventDelay = AnimationInteractionDelay;
    //return;    
}

simulated event StartInteract()
{
    local float WalkSpeed, WalkTime;
    local TdPawn.EWeaponType WeaponType;

    PawnOwner.Velocity = vect(0, 0, 0);
    PawnOwner.Acceleration = vect(0, 0, 0);
    bReachedTrigger = false;
    // End:0x69
    if(PawnOwner.OkToInteract())
    {
        PawnOwner.Interacted();
    }
    // End:0xE2
    if(CurrentTrigger.TriggerType == 1)
    {
        WalkTime = 0.3;
        WalkSpeed = float(Clamp(int(VSize2D(MoveLocation - PawnOwner.Location) / WalkTime), 100, 1000));
        TdPlayerPawn(PawnOwner).SetFirstPersonDPG(2);        
    }
    else
    {
        // End:0x159
        if((CurrentTrigger.TriggerType == 0) || CurrentTrigger.TriggerType == 2)
        {
            WalkTime = 0.2;
            WalkSpeed = float(Clamp(int(VSize2D(MoveLocation - PawnOwner.Location) / WalkTime), 100, 1000));
        }
    }
    ResetCameraLook(WalkTime);
    SetPreciseRotation(Normalize(rotator(-NormalFromTrigger2D)), WalkTime);
    SetPreciseLocation(MoveLocation, 1, WalkSpeed);
    WeaponType = PawnOwner.GetWeaponType();
    // End:0x221
    if((WeaponType == 1) || (WeaponType == 2) && PawnOwner.WeaponAnimState == 2)
    {
        PlayMoveAnim(7, 'unholster', -1.8, 0.2, -1);
        PawnOwner.SetCustomAnimsBlendOutTime(7, -1);
    }
    //return;    
}

simulated event ReachedPreciseLocationAndRotation()
{
    local Rotator ControllerAlignRotation;

    // End:0x3A
    if(bReachedTrigger)
    {
        PawnOwner.Velocity = vect(0, 0, 0);
        PawnOwner.SetLocation(MoveLocation);
        return;
    }
    bReachedTrigger = true;
    CurrentEvent.OutputLinks[0].ActivateDelay = EventDelay;
    // End:0xA8
    if(PawnOwner.Weapon != none)
    {
        PawnOwner.StopCustomAnim(7, 0.2);
        PawnOwner.SetUnarmed();
    }
    AbortLookAtTarget();
    ResetCameraLook(0);
    PawnOwner.Velocity = vect(0, 0, 0);
    PawnOwner.Acceleration = vect(0, 0, 0);
    PawnOwner.SetPhysics(4);
    PawnOwner.SetCollision(PawnOwner.bCollideActors, false);
    PawnOwner.bCollideWorld = false;
    PawnOwner.SetLocation(MoveLocation);
    PawnOwner.SetRotation(Normalize(rotator(-NormalFromTrigger2D)));
    ControllerAlignRotation = PawnOwner.Controller.Rotation;
    ControllerAlignRotation.Yaw = PawnOwner.Rotation.Yaw;
    PawnOwner.Controller.SetRotation(ControllerAlignRotation);
    PawnOwner.SetCollision(PawnOwner.bCollideActors, true);
    PawnOwner.bCollideWorld = true;
    PawnOwner.SetPhysics(1);
    // End:0x2C8
    if((CurrentTrigger.TriggerType == 0) || CurrentTrigger.TriggerType == 2)
    {
        PlayInteractAnimation(((CurrentTrigger.TriggerType == 0) ? 'interactbutton' : 'interactbuttonhigh'), 1, 0.2, 0.45);
        PawnOwner.SetIgnoreLookInput(-1);
        SetTimer(0.85);
        SetMoveTimer(EventDelay, false, 'ActivateFirstOuputLink');        
    }
    else
    {
        // End:0x30A
        if(CurrentTrigger.TriggerType == 1)
        {
            PawnOwner.SetIgnoreLookInput(-1);
            SetTimer(0.1);
        }
    }
    //return;    
}

simulated function ActivateFirstOuputLink()
{
    ActivateOutputLink(0);
    //return;    
}

simulated function OnTimer()
{
    // End:0x5B
    if(!bReachedTrigger)
    {
        // End:0x40
        if(CurrentTrigger.TriggerType == 1)
        {
            PawnOwner.StopCustomAnim(0, 0.3);
        }
        PawnOwner.SetMove(1);        
    }
    else
    {
        // End:0x142
        if(CurrentTrigger.TriggerType == 1)
        {
            // End:0xF9
            if(bReverseRev)
            {
                // End:0xC9
                if(CurrentTrigger.GetRevCount() == 1)
                {
                    PlayInteractAnimation('interactvalveend', 1, 0.2, 0);
                    ActivateOutputLink(2);                    
                }
                else
                {
                    PlayInteractAnimation('interactvalveloop', -1, 0.2, 0);
                    ActivateOutputLink(0);
                }                
            }
            else
            {
                PlayInteractAnimation('interactvalvestart', 1, 0.2, 0);
                ActivateOutputLink(0);
            }
            PawnOwner.SetIgnoreLookInput(-1);            
        }
        else
        {
            ActivateOutputLink(3);
            PawnOwner.SetMove(1);
        }
    }
    //return;    
}

simulated function StartMove()
{
    super.StartMove();
    UpdateEventDelay();
    StartInteract();
    //return;    
}

simulated function StopMove()
{
    super.StopMove();
    CurrentEvent.bInteract = false;
    CurrentTrigger = none;
    CurrentEvent = none;
    bReachedTrigger = false;
    // End:0x86
    if(PawnOwner.GetWeaponType() == 1)
    {
        PawnOwner.StopCustomAnim(7, 0.1);
        PlayMoveAnim(7, 'unholster', 1, 0, 0.2);
    }
    //return;    
}

simulated function PlayInteractAnimation(name AnimSeqName, float Rate, float BlendInTime, float BlendOutTime)
{
    PlayMoveAnim(0, AnimSeqName, Rate, BlendInTime, BlendOutTime);
    // End:0x84
    if(((CurrentEvent != none) && CurrentTrigger.TriggerType == 1) && CurrentEvent.InteractSkelMeshRef != none)
    {
        CurrentEvent.InteractSkelMeshRef.PlayAnimation(AnimSeqName, Rate);
    }
    //return;    
}

simulated function OnCustomAnimEnd(AnimNodeSequence SeqNode, float PlayedTime, float ExcessTime)
{
    // End:0x1F
    if(SeqNode.AnimSeqName == 'unholster')
    {
        return;
    }
    // End:0x25A
    if(CurrentTrigger.TriggerType == 1)
    {
        // End:0x11C
        if(bReverseRev)
        {
            CurrentTrigger.DecreaseRevCount();
            // End:0xA1
            if(CurrentTrigger.GetRevCount() == 1)
            {
                PlayInteractAnimation('interactvalveend', 1, 0, 0.3);
                ActivateOutputLink(2);                
            }
            else
            {
                // End:0xF5
                if(CurrentTrigger.GetRevCount() > 0)
                {
                    DecreaseValveRotation();
                    PlayInteractAnimation('interactvalveloop', -1, 0, 0);
                    ActivateOutputLink(1);                    
                }
                else
                {
                    ActivateOutputLink(3);
                    PawnOwner.SetMove(1);
                }
            }            
        }
        else
        {
            CurrentTrigger.IncreaseRevCount();
            // End:0x196
            if(CurrentTrigger.GetRevCount() == (CurrentTrigger.NumberOfRevs - 1))
            {
                IncreaseValveRotation();
                PlayInteractAnimation('interactvalveloop', 1, 0, 0);
                ActivateOutputLink(2);                
            }
            else
            {
                // End:0x212
                if(CurrentTrigger.GetRevCount() < CurrentTrigger.NumberOfRevs)
                {
                    // End:0x1E2
                    if(CurrentTrigger.GetRevCount() > 1)
                    {
                        IncreaseValveRotation();
                    }
                    PlayInteractAnimation('interactvalveloop', 1, 0, 0);
                    ActivateOutputLink(1);                    
                }
                else
                {
                    ActivateOutputLink(3);
                    PlayMoveAnim(0, 'interactvalveloop', 1, 0, 0.4);
                    bReachedTrigger = false;
                    SetTimer(0.3);
                }
            }
        }        
    }
    else
    {
        // End:0x2B4
        if((CurrentTrigger.TriggerType == 0) || CurrentTrigger.TriggerType == 2)
        {
            ActivateOutputLink(3);
            PawnOwner.SetMove(1);
        }
    }
    //return;    
}

simulated function IncreaseValveRotation()
{
    // End:0x45
    if((CurrentEvent != none) && CurrentEvent.InteractSkelMeshRef != none)
    {
        CurrentEvent.InteractSkelMeshRef.AddValveRoll(16384);
    }
    //return;    
}

simulated function DecreaseValveRotation()
{
    // End:0x45
    if((CurrentEvent != none) && CurrentEvent.InteractSkelMeshRef != none)
    {
        CurrentEvent.InteractSkelMeshRef.AddValveRoll(-16384);
    }
    //return;    
}

simulated function ActivateOutputLink(int OutputLinkNumber)
{
    // End:0x4A
    if((CurrentEvent != none) && OutputLinkNumber < CurrentEvent.OutputLinks.Length)
    {
        CurrentEvent.OutputLinks[OutputLinkNumber].bHasImpulse = true;
    }
    //return;    
}

simulated function bool CanUseLookAtHint()
{
    return false;
    //return ReturnValue;    
}

function DrawAnimDebugInfo(HUD HUD, out float out_YL, out float out_YPos)
{
    //return;    
}

defaultproperties
{
    DistanceToAButton=48
    DistanceToAValve=40
    ControllerState=PlayerWalking
    bDisableFaceRotation=true
    bTwoHandedFullBodyAnimations=true
    MovementGroup=MG_NonInteractive
    AimMode=MAM_NoHands
    DisableMovementTime=-1
    DisableLookTime=-1
}