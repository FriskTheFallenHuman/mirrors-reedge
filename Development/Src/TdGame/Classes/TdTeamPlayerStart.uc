/*******************************************************************************
 * TdTeamPlayerStart generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdTeamPlayerStart extends TdPlayerStart
    native
    hidecategories(Navigation,Lighting,LightColor,Force);

var() bool CopSpawn;
var() bool RobberSpawn;
var int TeamNumber;
var protected export editinline SpriteComponent CopSprite;
var protected export editinline SpriteComponent RunnerSprite;

function PreBeginPlay()
{
    TeamNumber = ((CopSpawn) ? ((RobberSpawn) ? 2 : 1) : 0);
    //return;    
}

defaultproperties
{
    CopSpawn=true
    RobberSpawn=true
    TeamNumber=2
    begin object name=CopSpawn class=SpriteComponent
        Sprite=Texture2D'TdEditorResources.CopSpawnIcon'
        HiddenGame=true
        AlwaysLoadOnClient=false
        AlwaysLoadOnServer=false
        Scale=0.33
    object end
    // Reference: SpriteComponent'Default__TdTeamPlayerStart.CopSpawn'
    CopSprite=CopSpawn
    begin object name=RunnerSpawn class=SpriteComponent
        Sprite=Texture2D'TdEditorResources.RunnerSpawnIcon'
        HiddenGame=true
        AlwaysLoadOnClient=false
        AlwaysLoadOnServer=false
        Scale=0.33
    object end
    // Reference: SpriteComponent'Default__TdTeamPlayerStart.RunnerSpawn'
    RunnerSprite=RunnerSpawn
    SphereRenderComponent=DrawSphereComponent'Default__TdTeamPlayerStart.SpawnRadiusSphere'
    GenericSprite=SpriteComponent'Default__TdTeamPlayerStart.Sprite'
    PlayerStartTextureResources=RequestedTextureResources'Default__TdTeamPlayerStart.PlayerStartTextureResourcesObject'
    CylinderComponent=CylinderComponent'Default__TdTeamPlayerStart.CollisionCylinder'
    GoodSprite=SpriteComponent'Default__TdTeamPlayerStart.Sprite'
    BadSprite=SpriteComponent'Default__TdTeamPlayerStart.Sprite2'
    Components(0)=SpriteComponent'Default__TdTeamPlayerStart.Sprite'
    Components(1)=SpriteComponent'Default__TdTeamPlayerStart.Sprite2'
    Components(2)=ArrowComponent'Default__TdTeamPlayerStart.Arrow'
    Components(3)=CylinderComponent'Default__TdTeamPlayerStart.CollisionCylinder'
    Components(4)=PathRenderingComponent'Default__TdTeamPlayerStart.PathRenderer'
    Components(5)=DrawSphereComponent'Default__TdTeamPlayerStart.SpawnRadiusSphere'
    begin object name=CopSpawn class=SpriteComponent
        Sprite=Texture2D'TdEditorResources.CopSpawnIcon'
        HiddenGame=true
        AlwaysLoadOnClient=false
        AlwaysLoadOnServer=false
        Scale=0.33
    object end
    // Reference: SpriteComponent'Default__TdTeamPlayerStart.CopSpawn'
    Components(6)=CopSpawn
    begin object name=RunnerSpawn class=SpriteComponent
        Sprite=Texture2D'TdEditorResources.RunnerSpawnIcon'
        HiddenGame=true
        AlwaysLoadOnClient=false
        AlwaysLoadOnServer=false
        Scale=0.33
    object end
    // Reference: SpriteComponent'Default__TdTeamPlayerStart.RunnerSpawn'
    Components(7)=RunnerSpawn
    CollisionComponent=CylinderComponent'Default__TdTeamPlayerStart.CollisionCylinder'
}