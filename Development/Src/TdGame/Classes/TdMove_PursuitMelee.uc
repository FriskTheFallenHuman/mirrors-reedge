/*******************************************************************************
 * TdMove_PursuitMelee generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_PursuitMelee extends TdMove_BotMelee
    config(AIMeleeAttacks);

var() config MeleeAttackProperties JumpKickAttackPropertiesE;
var() config MeleeAttackProperties RunAttackPropertiesE;
var() config MeleeAttackProperties StandAttackPropertiesE;
var() config MeleeAttackProperties SlideAttackPropertiesE;
var() config MeleeAttackProperties ShoveAttackPropertiesE;
var() config MeleeAttackProperties JumpKickAttackPropertiesN;
var() config MeleeAttackProperties RunAttackPropertiesN;
var() config MeleeAttackProperties StandAttackPropertiesN;
var() config MeleeAttackProperties SlideAttackPropertiesN;
var() config MeleeAttackProperties ShoveAttackPropertiesN;
var() config MeleeAttackProperties JumpKickAttackPropertiesH;
var() config MeleeAttackProperties RunAttackPropertiesH;
var() config MeleeAttackProperties StandAttackPropertiesH;
var() config MeleeAttackProperties SlideAttackPropertiesH;
var() config MeleeAttackProperties ShoveAttackPropertiesH;
var protected bool bAttackDidHit;
var TdAI_Pursuit.EPursuitMeleeAttackType AttackType;

function bool CanDoMove()
{
    // End:0x87
    if((((PawnOwner.IsInMove(69) || PawnOwner.IsInMove(35)) || PawnOwner.IsInMove(92)) || PawnOwner.IsInMove(92)) || PawnOwner.IsInMove(17))
    {
        return false;
    }
    return super.CanDoMove();
    //return ReturnValue;    
}

simulated function StartMove()
{
    local TdAI_Pursuit AIPursuitController;
    local Vector HitLocation, HitNormal, EndTrace, StartTrace;
    local Actor HitActor;

    AIPursuitController = TdAI_Pursuit(BotOwner.myController);
    BotOwner.IgnoreMoveInput(true);
    AttackType = AIPursuitController.GetPendingMeleeAttack();
    // End:0xFE
    if(AttackType == 2)
    {
        StartTrace = BotOwner.Location;
        EndTrace = StartTrace - (vector(BotOwner.Rotation) * 100);
        HitActor = BotOwner.Trace(HitLocation, HitNormal, EndTrace, StartTrace, true);
        // End:0xFE
        if(HitActor != none)
        {
            // End:0xF6
            if(BotOwner.myController.EnemyDistance2d < float(100))
            {
                AttackType = 4;                
            }
            else
            {
                AttackType = 3;
            }
        }
    }
    BotOwner.UseRootMotion(true);
    BotOwner.UseRootRotation(true);
    BotOwner.CurrentRotationSpeed = 0;
    BotOwner.DirectionOfMovement = vector(BotOwner.Rotation);
    super.StartMove();
    ClearTimer();
    //return;    
}

simulated event StopMove()
{
    super.StopMove();
    PawnOwner.ClearOverrideWalkingState();
    BotOwner.IgnoreMoveInput(false);
    BotOwner.UseRootRotation(false);
    BotOwner.UseRootMotion(false);
    PawnOwner.StopCustomAnim(2, 0.2);
    BotOwner.myController.HeadFocus.PopEnabled('PursuitMelee');
    //return;    
}

function bool IsInterruptableByDodge()
{
    // End:0x24
    if((AttackType == 4) || AttackType == 2)
    {
        return false;
    }
    // End:0x36
    if(MeleeStage == 1)
    {
        return true;
    }
    // End:0x47
    if(MoveActiveTime < 0.2)
    {
        return true;
    }
    return AttackProperties.bIsInterruptableByDodge;
    //return ReturnValue;    
}

simulated function TriggerMove()
{
    AttackProperties = GetAttackProperties(AttackType);
    switch(AttackType)
    {
        // End:0x4A
        case 0:
            AnimationName = 'MeleeJumpStart';
            BlendInTime = 0.2;
            BlendOutTime = -1;
            // End:0x101
            break;
        // End:0x77
        case 3:
            AnimationName = 'MeleeRunStart';
            BlendInTime = 0.2;
            BlendOutTime = -1;
            // End:0x101
            break;
        // End:0xA4
        case 2:
            AnimationName = 'MeleeKickStart';
            BlendInTime = 0.2;
            BlendOutTime = -1;
            // End:0x101
            break;
        // End:0xD1
        case 1:
            AnimationName = 'MeleeSlideStart';
            BlendInTime = 0.2;
            BlendOutTime = -1;
            // End:0x101
            break;
        // End:0xFE
        case 4:
            AnimationName = 'MeleeShoveStart';
            BlendInTime = 0.2;
            BlendOutTime = -1;
            // End:0x101
            break;
        // End:0xFFFF
        default:
            break;
    }
    PlayMoveAnim(2, AnimationName, AttackProperties.AttackSpeed, BlendInTime, BlendOutTime, true);
    //return;    
}

simulated function MeleeAttackProperties GetAttackProperties(TdAI_Pursuit.EPursuitMeleeAttackType Attack)
{
    // End:0x74
    if(BotOwner.myController.GetDifficultyLevel() == 0)
    {
        switch(Attack)
        {
            // End:0x36
            case 0:
                return JumpKickAttackPropertiesE;
                // End:0x71
                break;
            // End:0x44
            case 3:
                return RunAttackPropertiesE;
                // End:0x71
                break;
            // End:0x52
            case 2:
                return StandAttackPropertiesE;
                // End:0x71
                break;
            // End:0x60
            case 4:
                return ShoveAttackPropertiesE;
                // End:0x71
                break;
            // End:0x6E
            case 1:
                return SlideAttackPropertiesE;
                // End:0x71
                break;
            // End:0xFFFF
            default:
                break;
        }        
    }
    else
    {
        // End:0xE8
        if(BotOwner.myController.GetDifficultyLevel() == 1)
        {
            switch(Attack)
            {
                // End:0xAA
                case 0:
                    return JumpKickAttackPropertiesN;
                    // End:0xE5
                    break;
                // End:0xB8
                case 3:
                    return RunAttackPropertiesN;
                    // End:0xE5
                    break;
                // End:0xC6
                case 2:
                    return StandAttackPropertiesN;
                    // End:0xE5
                    break;
                // End:0xD4
                case 4:
                    return ShoveAttackPropertiesN;
                    // End:0xE5
                    break;
                // End:0xE2
                case 1:
                    return SlideAttackPropertiesN;
                    // End:0xE5
                    break;
                // End:0xFFFF
                default:
                    break;
            }            
        }
        else
        {
            // End:0x15A
            if(BotOwner.myController.GetDifficultyLevel() == 2)
            {
                switch(Attack)
                {
                    // End:0x11F
                    case 0:
                        return JumpKickAttackPropertiesH;
                        // End:0x15A
                        break;
                    // End:0x12D
                    case 3:
                        return RunAttackPropertiesH;
                        // End:0x15A
                        break;
                    // End:0x13B
                    case 2:
                        return StandAttackPropertiesH;
                        // End:0x15A
                        break;
                    // End:0x149
                    case 4:
                        return ShoveAttackPropertiesH;
                        // End:0x15A
                        break;
                    // End:0x157
                    case 1:
                        return SlideAttackPropertiesH;
                        // End:0x15A
                        break;
                    // End:0xFFFF
                    default:
                        break;
                }
            }
            else
            {
            }
        }
        //return ReturnValue;        
    }
}

simulated function TriggerHit()
{
    bAttackDidHit = true;
    switch(AttackType)
    {
        // End:0x3D
        case 0:
            AnimationName = 'MeleeJumpEnd';
            BlendInTime = 0.1;
            BlendOutTime = 0.3;
            // End:0xF4
            break;
        // End:0x6A
        case 3:
            AnimationName = 'MeleeRunHit';
            BlendInTime = 0.1;
            BlendOutTime = 0.3;
            // End:0xF4
            break;
        // End:0x97
        case 2:
            AnimationName = 'MeleeKickHit';
            BlendInTime = 0.1;
            BlendOutTime = 0.3;
            // End:0xF4
            break;
        // End:0xC4
        case 1:
            AnimationName = 'MeleeSlideHit';
            BlendInTime = 0.1;
            BlendOutTime = 0.3;
            // End:0xF4
            break;
        // End:0xF1
        case 4:
            AnimationName = 'MeleeShoveEnd';
            BlendInTime = 0.1;
            BlendOutTime = 0.3;
            // End:0xF4
            break;
        // End:0xFFFF
        default:
            break;
    }
    PlayMoveAnim(2, AnimationName, 1, BlendInTime, BlendOutTime, true);
    BotOwner.SetOverrideWalkingState(0, BlendInTime);
    BotOwner.UseRootMotion(true);
    BotOwner.UseRootRotation(true);
    //return;    
}

simulated function TriggerMiss()
{
    local float AnimationSpeedFactor;

    TdAI_Pursuit(TdBotPawn(PawnOwner).myController).NotifyAttackMiss(AttackType);
    bAttackDidHit = false;
    switch(AttackType)
    {
        // End:0x6A
        case 0:
            AnimationName = 'MeleeJumpEnd';
            BlendInTime = 0.1;
            BlendOutTime = 0.3;
            // End:0x121
            break;
        // End:0x97
        case 3:
            AnimationName = 'MeleeRunMiss';
            BlendInTime = 0.1;
            BlendOutTime = 0.3;
            // End:0x121
            break;
        // End:0xC4
        case 2:
            AnimationName = 'MeleeKickMiss';
            BlendInTime = 0.1;
            BlendOutTime = 0.3;
            // End:0x121
            break;
        // End:0xF1
        case 1:
            AnimationName = 'MeleeSlideMiss';
            BlendInTime = 0.1;
            BlendOutTime = 0.3;
            // End:0x121
            break;
        // End:0x11E
        case 4:
            AnimationName = 'MeleeShoveEnd';
            BlendInTime = 0.1;
            BlendOutTime = 0.3;
            // End:0x121
            break;
        // End:0xFFFF
        default:
            break;
    }
    // End:0x174
    if(BotOwner.myController.GetDifficultyLevel() == 2)
    {
        SetMoveTimer(AttackProperties.MissedAttackPenelty, false, 'StopMissAnimation');
        AnimationSpeedFactor = 1.2;        
    }
    else
    {
        AnimationSpeedFactor = 1;
    }
    PlayMoveAnim(2, AnimationName, AnimationSpeedFactor, BlendInTime, BlendOutTime, true);
    BotOwner.SetOverrideWalkingState(0, BlendInTime);
    BotOwner.UseRootMotion(true);
    BotOwner.UseRootRotation(true);
    //return;    
}

simulated function OnCustomAnimEnd(AnimNodeSequence SeqNode, float PlayedTime, float ExcessTime)
{
    // End:0x28
    if(MeleeStage == 0)
    {
        super.OnCustomAnimEnd(SeqNode, PlayedTime, ExcessTime);        
    }
    else
    {
        PawnOwner.SetMove(1);
    }
    //return;    
}

simulated function StopMissAnimation()
{
    PawnOwner.SetMove(1);
    //return;    
}

simulated function class<DamageType> GetDamageType()
{
    switch(AttackType)
    {
        // End:0x16
        case 0:
            return Class'TdDmgType_MeleeAir';
            // End:0x51
            break;
        // End:0x24
        case 3:
            return Class'TdDmgType_MeleeLeft';
            // End:0x51
            break;
        // End:0x32
        case 2:
            return Class'TdDmgType_MeleeLeft';
            // End:0x51
            break;
        // End:0x40
        case 1:
            return Class'TdDmgType_MeleeSlide';
            // End:0x51
            break;
        // End:0x4E
        case 4:
            return Class'TdDmgType_Shove';
            // End:0x51
            break;
        // End:0xFFFF
        default:
            break;
    }
    return none;
    //return ReturnValue;    
}

simulated function EnableLOI()
{
    //return;    
}

defaultproperties
{
    JumpKickAttackPropertiesE=(HitAngle=40,HitRange=170,AttackHeightAdjustment=0,MissedAttackPenelty=2,Damage=50,AttackSpeed=0.95,RotationSpeed=0,RotationLimitAngle=120,HitDetectionStartTime=-1,PredictionTime=0.3,PredictionWeight=1,InvulnerableDamageTypes=(AllMelee),bIsInterruptableByDodge=false)
    RunAttackPropertiesE=(HitAngle=90,HitRange=120,AttackHeightAdjustment=0,MissedAttackPenelty=2.2,Damage=35,AttackSpeed=0.5,RotationSpeed=0,RotationLimitAngle=80,HitDetectionStartTime=0,PredictionTime=0.3,PredictionWeight=1,InvulnerableDamageTypes=none,bIsInterruptableByDodge=false)
    StandAttackPropertiesE=(HitAngle=100,HitRange=120,AttackHeightAdjustment=0,MissedAttackPenelty=0,Damage=35,AttackSpeed=0.95,RotationSpeed=0,RotationLimitAngle=180,HitDetectionStartTime=-1,PredictionTime=0.3,PredictionWeight=1,InvulnerableDamageTypes=none,bIsInterruptableByDodge=false)
    SlideAttackPropertiesE=(HitAngle=120,HitRange=200,AttackHeightAdjustment=-70,MissedAttackPenelty=1.5,Damage=35,AttackSpeed=0.95,RotationSpeed=0,RotationLimitAngle=80,HitDetectionStartTime=0.5,PredictionTime=0.3,PredictionWeight=1,InvulnerableDamageTypes=(AllMelee),bIsInterruptableByDodge=false)
    ShoveAttackPropertiesE=(HitAngle=65,HitRange=140,AttackHeightAdjustment=0,MissedAttackPenelty=0,Damage=15,AttackSpeed=0.95,RotationSpeed=360,RotationLimitAngle=360,HitDetectionStartTime=-1,PredictionTime=-1,PredictionWeight=0,InvulnerableDamageTypes=none,bIsInterruptableByDodge=false)
    JumpKickAttackPropertiesN=(HitAngle=40,HitRange=170,AttackHeightAdjustment=0,MissedAttackPenelty=1.5,Damage=50,AttackSpeed=1,RotationSpeed=0,RotationLimitAngle=0,HitDetectionStartTime=-1,PredictionTime=0.3,PredictionWeight=1,InvulnerableDamageTypes=(AllMelee),bIsInterruptableByDodge=false)
    RunAttackPropertiesN=(HitAngle=90,HitRange=120,AttackHeightAdjustment=0,MissedAttackPenelty=1.7,Damage=35,AttackSpeed=0.55,RotationSpeed=0,RotationLimitAngle=0,HitDetectionStartTime=0,PredictionTime=0.3,PredictionWeight=1,InvulnerableDamageTypes=(AllMelee),bIsInterruptableByDodge=false)
    StandAttackPropertiesN=(HitAngle=100,HitRange=120,AttackHeightAdjustment=0,MissedAttackPenelty=0,Damage=35,AttackSpeed=1,RotationSpeed=0,RotationLimitAngle=180,HitDetectionStartTime=-1,PredictionTime=0.3,PredictionWeight=0.8,InvulnerableDamageTypes=(MeleePunsh),bIsInterruptableByDodge=false)
    SlideAttackPropertiesN=(HitAngle=120,HitRange=200,AttackHeightAdjustment=-70,MissedAttackPenelty=1,Damage=35,AttackSpeed=1,RotationSpeed=0,RotationLimitAngle=0,HitDetectionStartTime=0.5,PredictionTime=0.3,PredictionWeight=1,InvulnerableDamageTypes=(AllMelee),bIsInterruptableByDodge=false)
    ShoveAttackPropertiesN=(HitAngle=65,HitRange=140,AttackHeightAdjustment=0,MissedAttackPenelty=0,Damage=15,AttackSpeed=1,RotationSpeed=360,RotationLimitAngle=360,HitDetectionStartTime=-1,PredictionTime=-1,PredictionWeight=0,InvulnerableDamageTypes=(AllMelee),bIsInterruptableByDodge=false)
    JumpKickAttackPropertiesH=(HitAngle=40,HitRange=170,AttackHeightAdjustment=0,MissedAttackPenelty=0.55,Damage=50,AttackSpeed=1.2,RotationSpeed=0,RotationLimitAngle=120,HitDetectionStartTime=-1,PredictionTime=0.3,PredictionWeight=0.8,InvulnerableDamageTypes=(AllMelee),bIsInterruptableByDodge=false)
    RunAttackPropertiesH=(HitAngle=90,HitRange=120,AttackHeightAdjustment=0,MissedAttackPenelty=0.65,Damage=35,AttackSpeed=0.6,RotationSpeed=0,RotationLimitAngle=80,HitDetectionStartTime=0,PredictionTime=0.3,PredictionWeight=0.8,InvulnerableDamageTypes=(AllMelee),bIsInterruptableByDodge=false)
    StandAttackPropertiesH=(HitAngle=100,HitRange=120,AttackHeightAdjustment=0,MissedAttackPenelty=0.35,Damage=35,AttackSpeed=1.2,RotationSpeed=0,RotationLimitAngle=180,HitDetectionStartTime=-1,PredictionTime=0.3,PredictionWeight=0.8,InvulnerableDamageTypes=(MeleePunsh),bIsInterruptableByDodge=true)
    SlideAttackPropertiesH=(HitAngle=120,HitRange=200,AttackHeightAdjustment=-70,MissedAttackPenelty=0.65,Damage=35,AttackSpeed=1.2,RotationSpeed=0,RotationLimitAngle=80,HitDetectionStartTime=0.5,PredictionTime=0.3,PredictionWeight=0.8,InvulnerableDamageTypes=(AllMelee),bIsInterruptableByDodge=false)
    ShoveAttackPropertiesH=(HitAngle=65,HitRange=140,AttackHeightAdjustment=0,MissedAttackPenelty=0.35,Damage=15,AttackSpeed=1.1,RotationSpeed=360,RotationLimitAngle=360,HitDetectionStartTime=-1,PredictionTime=-1,PredictionWeight=0,InvulnerableDamageTypes=(AllMelee),bIsInterruptableByDodge=true)
    bEnableFootPlacement=false
}