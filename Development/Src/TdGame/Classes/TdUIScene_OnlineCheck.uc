/*******************************************************************************
 * TdUIScene_OnlineCheck generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdUIScene_OnlineCheck extends TdUIScene_SubMenu
    config(UI)
    hidecategories(Object,UIRoot,Object);

var delegate<PlayOffline> __PlayOffline__Delegate;

delegate PlayOffline()
{
    //return;    
}

function SetupButtonBar()
{
    ButtonBar.AppendButton("Back", OnButtonBar_Back);
    ButtonBar.AppendButton("PlayOffline", OnButtonBar_PlayOffline);
    //return;    
}

function bool OnButtonBar_Back(UIScreenObject Sender, int PlayerIndex)
{
    OnCloseScene();
    return true;
    //return ReturnValue;    
}

function bool OnButtonBar_PlayOffline(UIScreenObject Sender, int PlayerIndex)
{
    OnPlayOffline();
    return true;
    //return ReturnValue;    
}

function OnCloseScene()
{
    CloseScene(self);
    //return;    
}

function OnPlayOffline()
{
    __OnSceneDeactivated__Delegate = OnSceneClosed_PlayOffline;
    CloseScene(self,, 1);
    //return;    
}

function OnSceneClosed_PlayOffline(UIScene ClosedScene)
{
    PlayOffline();
    //return;    
}

function bool HandleInputKey(const out InputEventParameters EventParms)
{
    local bool bResult;

    // End:0xBE
    if(EventParms.EventType == 1)
    {
        // End:0x6E
        if((EventParms.InputKeyName == 'XboxTypeS_A') || EventParms.InputKeyName == 'Enter')
        {
            OnPlayOffline();
            bResult = true;            
        }
        else
        {
            // End:0xBE
            if((EventParms.InputKeyName == 'XboxTypeS_B') || EventParms.InputKeyName == 'Escape')
            {
                OnCloseScene();
                bResult = true;
            }
        }
    }
    return bResult;
    //return ReturnValue;    
}

defaultproperties
{
    EventProvider=UIComp_Event'Default__TdUIScene_OnlineCheck.SceneEventComponent'
}