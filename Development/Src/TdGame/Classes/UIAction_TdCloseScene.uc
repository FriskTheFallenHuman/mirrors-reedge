/*******************************************************************************
 * UIAction_TdCloseScene generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class UIAction_TdCloseScene extends UIAction_Scene
    native
    hidecategories(Object);

var TdUIScene ClosedScene;

defaultproperties
{
    bAutoTargetOwner=true
    bLatentExecution=true
    ObjName="TdCloseScene"
    ObjCategory="Takedown"
}