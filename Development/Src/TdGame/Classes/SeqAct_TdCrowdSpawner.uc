/*******************************************************************************
 * SeqAct_TdCrowdSpawner generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class SeqAct_TdCrowdSpawner extends SeqAct_CrowdSpawner
    hidecategories(Object);

defaultproperties
{
    CrowdNodeClass=Class'TdCrowdPathNode'
    ActionDuration=(Distribution=DistributionFloatUniform'Default__SeqAct_TdCrowdSpawner.DistributionActionDuration')
    ActionInterval=(Distribution=DistributionFloatUniform'Default__SeqAct_TdCrowdSpawner.DistributionActionInterval')
    TargetActionInterval=(Distribution=DistributionFloatUniform'Default__SeqAct_TdCrowdSpawner.DistributionTargetActionInterval')
    ObjName="Td Crowd Spawner"
}