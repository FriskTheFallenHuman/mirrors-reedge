/*******************************************************************************
 * TdMove_Walking generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_Walking extends TdPhysicsMove
    config(PawnMovement);

struct IdleAnimStruct
{
    var name AnimName;
    var TdPawn.CustomNodeType NodeType;
    var bool bResetCameraLook;

    structdefaultproperties
    {
        AnimName=None
        NodeType=CNT_Canned
        bResetCameraLook=false
    }
};

var private transient bool bIsPlayingIdleAnim;
var() config float TriggerIdleAnimMinTime;
var() config float TriggerIdleAnimMaxTime;
var config array<config IdleAnimStruct> UnarmedIdleAnims;
var config array<config IdleAnimStruct> LightIdleAnims;
var config array<config IdleAnimStruct> HeavyIdleAnims;
var private transient IdleAnimStruct CurrentIdleAnim;

function bool CanDoMove()
{
    local Vector Extent, Start, End;
    local float CrouchCollisionSize;

    CrouchCollisionSize = 122;
    // End:0x18
    if(!super(TdMove).CanDoMove())
    {
        return false;
    }
    // End:0xD7
    if((PawnOwner.MovementState == 15) || PawnOwner.MovementState == 16)
    {
        Start = PawnOwner.Location;
        End = Start;
        End.Z += ((PawnOwner.default.CylinderComponent.CollisionHeight * 2) - CrouchCollisionSize);
        Extent = PawnOwner.GetCollisionExtent();
        // End:0xD7
        if(MovementTraceForBlocking(End, Start, Extent))
        {
            return false;
        }
    }
    return true;
    //return ReturnValue;    
}

simulated function StartMove()
{
    local float IdleTime;

    super.StartMove();
    PawnOwner.bIllegalLedgeTimer = 0;
    // End:0x7D
    if((PawnOwner.Controller != none) && PawnOwner.Controller.IsA('PlayerController'))
    {
        IdleTime = GetNewIdleTriggerTime();
        SetMoveTimer(IdleTime, false, 'OnIdleTimer');
    }
    // End:0x119
    if((PawnOwner.IsA('TdPlayerPawn') && PawnOwner.GetWeaponType() == 1) && PawnOwner.OldMovementState == 20)
    {
        PawnOwner.PlayCustomAnim(7, 'JumpLand', 1, 0.2, 0.1, false, true, false);
        SetMoveTimer(0.2, false, 'OnBlendOutJumpLandTimer');
    }
    //return;    
}

simulated function StopMove()
{
    local TdAnimNodeSequence AnimSeq1p;

    super.StopMove();
    PawnOwner.SetHipsOffset(vect(0, 0, 0));
    // End:0x3D
    if(bIsPlayingIdleAnim)
    {
        StopIdle();
    }
    // End:0xF5
    if(((PawnOwner.GetWeaponType() == 1) && PawnOwner.CustomWeaponNode1p != none) && PawnOwner.PendingMovementState != 16)
    {
        AnimSeq1p = TdAnimNodeSequence(PawnOwner.CustomWeaponNode1p.GetCustomAnimNodeSeq());
        // End:0xF5
        if((AnimSeq1p != none) && AnimSeq1p.AnimSeqName == 'JumpLand')
        {
            PawnOwner.StopCustomAnim(7, 0.1);
        }
    }
    //return;    
}

simulated function UpdateViewRotation(out Rotator out_Rotation, float DeltaTime, out Rotator DeltaRot)
{
    local Vector HipsOffset;
    local TdPlayerController TdPC;
    local bool bMovingView;

    // End:0x79
    if((PawnOwner.CurrentWalkingState > 2) && PawnOwner.SwanNeck1p != none)
    {
        HipsOffset.X = -FMin(PawnOwner.SwanNeck1p.Translation.Y, 5) * 4;        
    }
    else
    {
        HipsOffset.X = 0;
    }
    PawnOwner.SetHipsOffset(HipsOffset);
    super(TdMove).UpdateViewRotation(out_Rotation, DeltaTime, DeltaRot);
    TdPC = TdPlayerController(PawnOwner.Controller);
    // End:0x102
    if(TdPC != none)
    {
        UpdateMeleeAutoLockOn(TdPC, DeltaTime, out_Rotation, DeltaRot);
    }
    bMovingView = (((DeltaRot.Yaw > 0) || DeltaRot.Yaw < 0) || DeltaRot.Pitch > 0) || DeltaRot.Pitch < 0;
    // End:0x1BB
    if((PawnOwner.CurrentWalkingState > 0) || bMovingView)
    {
        // End:0x19D
        if(bIsPlayingIdleAnim)
        {
            StopIdle();
        }
        SetMoveTimer(GetNewIdleTriggerTime(), false, 'OnIdleTimer');
    }
    //return;    
}

simulated function HandleMoveAction(TdPawn.EMovementAction Action)
{
    super(TdMove).HandleMoveAction(Action);
    // End:0x4D
    if(Action != 0)
    {
        // End:0x2F
        if(bIsPlayingIdleAnim)
        {
            StopIdle();
        }
        SetMoveTimer(GetNewIdleTriggerTime(), false, 'OnIdleTimer');
    }
    //return;    
}

simulated function OnBlendOutJumpLandTimer()
{
    local TdAnimNodeSequence AnimSeq1p;

    AnimSeq1p = TdAnimNodeSequence(PawnOwner.CustomWeaponNode1p.GetCustomAnimNodeSeq());
    // End:0x5D
    if(AnimSeq1p.AnimSeqName == 'JumpLand')
    {
        PawnOwner.StopCustomAnim(7, 0.3);
    }
    //return;    
}

simulated function float GetNewIdleTriggerTime()
{
    local float TimeVariation;

    TimeVariation = FMax(TriggerIdleAnimMaxTime - TriggerIdleAnimMinTime, 0);
    return TriggerIdleAnimMinTime + (FRand() * TimeVariation);
    //return ReturnValue;    
}

simulated function OnIdleTimer()
{
    PlayIdle();
    //return;    
}

simulated function bool CanPlayIdle()
{
    // End:0x24
    if(PlayerController(PawnOwner.Controller).bCinematicMode)
    {
        return false;
    }
    // End:0x40
    if(PawnOwner.AgainstWallState != 0)
    {
        return false;
    }
    return true;
    //return ReturnValue;    
}

simulated function PlayIdle()
{
    local TdPawn.EWeaponType CurrentWeaponType;

    // End:0x2F
    if(!CanPlayIdle())
    {
        SetMoveTimer(GetNewIdleTriggerTime(), false, 'OnIdleTimer');
        return;
    }
    CurrentWeaponType = PawnOwner.GetWeaponType();
    // End:0x7A
    if((CurrentWeaponType == 0) && UnarmedIdleAnims.Length > 0)
    {
        CurrentIdleAnim = UnarmedIdleAnims[Rand(UnarmedIdleAnims.Length)];        
    }
    else
    {
        // End:0xAF
        if((CurrentWeaponType == 2) && LightIdleAnims.Length > 0)
        {
            CurrentIdleAnim = LightIdleAnims[Rand(LightIdleAnims.Length)];            
        }
        else
        {
            // End:0xE4
            if((CurrentWeaponType == 1) && HeavyIdleAnims.Length > 0)
            {
                CurrentIdleAnim = HeavyIdleAnims[Rand(HeavyIdleAnims.Length)];                
            }
            else
            {
                CurrentIdleAnim.AnimName = 'None';
                CurrentIdleAnim.bResetCameraLook = false;
            }
        }
    }
    // End:0x130
    if(CurrentIdleAnim.bResetCameraLook)
    {
        ResetCameraLook(0.6);
    }
    // End:0x190
    if(CurrentIdleAnim.AnimName != 'None')
    {
        PlayMoveAnim(CurrentIdleAnim.NodeType, CurrentIdleAnim.AnimName, 1, 0.4, 0.4);
        bIsPlayingIdleAnim = true;        
    }
    else
    {
        SetMoveTimer(GetNewIdleTriggerTime(), false, 'OnIdleTimer');
        return;
    }
    //return;    
}

simulated function bool IsPlayingIdleAnim()
{
    return bIsPlayingIdleAnim;
    //return ReturnValue;    
}

simulated function StopIdle(optional float BlendOutTime)
{
    BlendOutTime = 0.4;
    PawnOwner.StopCustomAnim(CurrentIdleAnim.NodeType, BlendOutTime);
    // End:0x4E
    if(CurrentIdleAnim.bResetCameraLook)
    {
        bResetCameraLook = false;
    }
    bIsPlayingIdleAnim = false;
    //return;    
}

simulated function OnCustomAnimEnd(AnimNodeSequence SeqNode, float PlayedTime, float ExcessTime)
{
    SetMoveTimer(GetNewIdleTriggerTime(), false, 'OnIdleTimer');
    bIsPlayingIdleAnim = false;
    //return;    
}

function CheckForCameraCollision(Vector CameraLocation, Rotator CameraRotation)
{
    local Vector HitLocation, HitNormal, TraceEnd, TraceStart, TraceExtent;

    local float HitTime;

    super(TdMove).CheckForCameraCollision(CameraLocation, CameraRotation);
    // End:0x191
    if(PawnOwner.AgainstWallState == 0)
    {
        TraceStart = CameraLocation - (vector(PawnOwner.Controller.Rotation) * 5);
        TraceEnd = CameraLocation + (vector(PawnOwner.Controller.Rotation) * 15);
        TraceExtent = vect(2, 2, 2);
        // End:0x189
        if(MovementTraceForBlockingEx(TraceEnd, TraceStart, TraceExtent, HitLocation, HitNormal))
        {
            HitTime = VSize(HitLocation - TraceStart) / VSize(TraceEnd - TraceStart);
            // End:0x141
            if((MaxLookConstraint.Pitch < 14000) || HitTime < 0.8)
            {
                MinLookConstraint.Pitch = int(float(Normalize(CameraRotation).Pitch) * HitTime);                
            }
            else
            {
                MinLookConstraint.Pitch = Normalize(CameraRotation).Pitch;
            }
            MaxLookConstraint.Pitch = 32768;
            bConstrainLook = true;            
        }
        else
        {
            bConstrainLook = false;
        }
    }
    //return;    
}

defaultproperties
{
    TriggerIdleAnimMinTime=30
    TriggerIdleAnimMaxTime=40
    UnarmedIdleAnims(0)=(AnimName=standidle1,NodeType=CNT_Canned,bResetCameraLook=true)
    UnarmedIdleAnims(1)=(AnimName=standidle2,NodeType=CNT_Canned,bResetCameraLook=true)
    UnarmedIdleAnims(2)=(AnimName=standidle3,NodeType=CNT_Canned,bResetCameraLook=true)
    ControllerState=PlayerWalking
    bShouldUnzoom=false
    bUseCameraCollision=true
    bEnableFootPlacement=true
    bEnableAgainstWall=true
    bAllowPickup=true
}