/*******************************************************************************
 * SeqAct_SetHeliSpeed generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class SeqAct_SetHeliSpeed extends SequenceAction
    hidecategories(Object);

var() TdAI_HeliController.HeliSpeed Speed;

defaultproperties
{
    InputLinks(0)=(LinkDesc="Activate",bHasImpulse=false,bDisabled=false,bDisabledPIE=false,LinkedOp=none,DrawY=0,bHidden=false,ActivateDelay=0)
    ObjName="Td Set Heli Speed"
    ObjCategory="AI"
}