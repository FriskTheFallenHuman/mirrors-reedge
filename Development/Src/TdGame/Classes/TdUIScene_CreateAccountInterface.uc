/*******************************************************************************
 * TdUIScene_CreateAccountInterface generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
interface TdUIScene_CreateAccountInterface extends Interface
    abstract;

function CreateAccountDone(int Error, optional string LocError)
{
    //return;    
}

function SetSceneDeactivatedDelegate(delegate<OnSceneDeactivated> SceneDeactivated)
{
    //return;    
}
