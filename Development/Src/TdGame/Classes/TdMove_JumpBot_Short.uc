/*******************************************************************************
 * TdMove_JumpBot_Short generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdMove_JumpBot_Short extends TdMove_JumpBot_Base
    config(PawnMovement);

simulated function BeginJump()
{
    PawnOwner.SetAnimationMovementState(1);
    PlayMoveAnim(2, 'JumpUpShortNormal', 1, 0.2, 0.1, true);
    PawnOwner.UseRootMotion(true);
    PawnOwner.UseRootRotation(true);
    //return;    
}

simulated function OnCustomAnimEnd(AnimNodeSequence SeqNode, float PlayedTime, float ExcessTime)
{
    PawnOwner.SetMove(1);
    //return;    
}
