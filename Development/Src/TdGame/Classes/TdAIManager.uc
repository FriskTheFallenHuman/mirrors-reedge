/*******************************************************************************
 * TdAIManager generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdAIManager extends Actor
    native
    notplaceable
    hidecategories(Navigation);

const MinDotForBurstPriority = 0.71f;
const MinDistForBurstPriority = 1000.f;
const cMaxAIUsingMelee = 1;

struct native PathfindingStruct
{
    var TdAIController C;
    var float LastPathfindTimeStamp;

    structdefaultproperties
    {
        C=none
        LastPathfindTimeStamp=0
    }
};

struct native Burst
{
    var TdBotPawn Pawn;
    var float StartTime;
    var float Duration;
    var bool bActive;
    var bool bPriorityBurst;

    structdefaultproperties
    {
        Pawn=none
        StartTime=0
        Duration=0
        bActive=false
        bPriorityBurst=false
    }
};

var array<TdAIController> AIControllers;
var private array<AITeam> Teams;
var private TdPlayerPawn Player;
var TdPawn Enemy;
var private array<TdGrenadeTargetArea> GrenadeTargetAreas;
var private TdAIController GrenadeThrowingBot;
var private TdProj_Grenade ActiveGrenade;
var bool bPlayerInvisibleToAI;
var bool bHasAnyoneStartedTestingCombatTransitionsThisFrame;
var private bool bLastSeenLocationHasBeenRestored;
var private bool bPredictedLastSeenLocationHasBeenSet;
var private bool bHaveTriedToPredict;
var bool CheckedLastSeenLocation;
var bool bAggressiveChaseAI;
var(AIManager) bool UseBurstControl;
var(AIManager) bool UseBulletControl;
var private array<TdAIController> BotsUsingMelee;
var private int NumNotifiedAI;
var private int NotifiedIndex;
var private int TaserSpotSearchesThisFrame;
var int UpdateVisibilityIndex;
var int MaxNumPathfindsPerFrame;
var int NumPathfindsThisFrame;
var private array<PathfindingStruct> PathfindingData;
var array<int> PathfindsPerFrameHistory;
var int PathfindsArrayIndex;
var float BiggestAveragePathfindsPerSecond;
var float LongestWaitTime;
var Vector PlayerViewLocation;
var Rotator PlayerViewRotation;
var Vector PlayerViewpointLocation;
var NavigationPoint PlayerNavigationPointForShieldWall;
var int ShieldFormations;
var NavigationPoint PlayerNavigationPoint;
var float LSLTimeStamp;
var Vector LastSeenLocation;
var private Vector OldLastSeenLocation;
var private Vector ReallyOldLastSeenLocation;
var Vector LastSeenHeadLocation;
var private NavigationPoint CurrentNavPointToCheck;
var int ShotsFiredThisFrame;
var(AIManager) int MaxShotsFiredPerFrame;
var int ShotsFiredThisSecond;
var(AIManager) int MaxShotsFiredPerSecond;
var float CurrentMeasuringSecond;
var float LastShotFiredTimeStamp;
var int NumCleanFramesSinceLastShotFired;
var(AIManager) int MaxNumCleanFrames;
var array<Burst> Bursts;
var int NumberOfActiveBursts;
var int BurstPriorityCount;
var(AIManager) int MaxSimultaneousBursts;
var int NumberOfFireIntentsThisFrame;

event Tick(float DeltaTime)
{
    local TdPlayerController PlayerController;

    bHasAnyoneStartedTestingCombatTransitionsThisFrame = false;
    PathfindsPerFrameHistory[PathfindsArrayIndex] = NumPathfindsThisFrame;
    PathfindsArrayIndex = Percent_IntInt(PathfindsArrayIndex + 1, 30);
    NumPathfindsThisFrame = 0;
    TaserSpotSearchesThisFrame = 0;
    PlayerController = ((Player != none) ? TdPlayerController(Player.Controller) : none);
    // End:0x12E
    if(PlayerController != none)
    {
        // End:0xE1
        if((PlayerController.PlayerCamera != none) && PlayerController.PlayerCamera.CameraStyle == 'FreeFlight')
        {
            PlayerViewLocation = Player.GetViewpointLocation();
            PlayerViewRotation = Player.Rotation;            
        }
        else
        {
            PlayerController.GetPlayerViewPoint(PlayerViewLocation, PlayerViewRotation);
        }
        PlayerViewpointLocation = Player.GetViewpointLocation();
        UpdatePlayerNavigationPoint();
        NotifyPlayerNavigationPointChanged();
    }
    // End:0x157
    if(AIControllers.Length > 0)
    {
        UpdateVisibilityIndex = Percent_IntInt(UpdateVisibilityIndex + 1, AIControllers.Length);        
    }
    else
    {
        UpdateVisibilityIndex = 0;
    }
    UpdateFiringStats();
    UpdateBursts();
    NumberOfFireIntentsThisFrame = 0;
    //return;    
}

function UpdateFiringStats()
{
    // End:0x46
    if((ShotsFiredThisFrame == 0) && TimeSince(LastShotFiredTimeStamp) < 0.3)
    {
        NumCleanFramesSinceLastShotFired++;
        // End:0x43
        if(NumCleanFramesSinceLastShotFired > MaxNumCleanFrames)
        {
            MaxNumCleanFrames = NumCleanFramesSinceLastShotFired;
        }        
    }
    else
    {
        NumCleanFramesSinceLastShotFired = 0;
    }
    // End:0x67
    if(ShotsFiredThisFrame > MaxShotsFiredPerFrame)
    {
        MaxShotsFiredPerFrame = ShotsFiredThisFrame;
    }
    ShotsFiredThisFrame = 0;
    //return;    
}

function ReportBulletFired()
{
    // End:0x5A
    if((WorldInfo.TimeSeconds - CurrentMeasuringSecond) > 1)
    {
        CurrentMeasuringSecond = float(int(WorldInfo.TimeSeconds));
        // End:0x53
        if(ShotsFiredThisSecond > MaxShotsFiredPerSecond)
        {
            MaxShotsFiredPerSecond = ShotsFiredThisSecond;
        }
        ShotsFiredThisSecond = 0;
    }
    ShotsFiredThisFrame++;
    ShotsFiredThisSecond++;
    LastShotFiredTimeStamp = WorldInfo.TimeSeconds;
    //return;    
}

function ReportFireIntent()
{
    NumberOfFireIntentsThisFrame++;
    //return;    
}

function bool OkToFireBullet()
{
    // End:0x0D
    if(!UseBulletControl)
    {
        return true;
    }
    return ShotsFiredThisFrame == 0;
    //return ReturnValue;    
}

function ClearFiringStats()
{
    MaxShotsFiredPerFrame = 0;
    ShotsFiredThisFrame = 0;
    ShotsFiredThisSecond = 0;
    MaxShotsFiredPerSecond = 0;
    NumberOfFireIntentsThisFrame = 0;
    //return;    
}

function bool AskToBurst(TdBotPawn Pawn, float BurstDuration)
{
    local Burst B;

    // End:0x0D
    if(!UseBurstControl)
    {
        return true;
    }
    B.Pawn = Pawn;
    B.StartTime = WorldInfo.TimeSeconds;
    B.Duration = BurstDuration;
    RemoveBurst(Pawn);
    B.bActive = NumberOfActiveBursts < MaxSimultaneousBursts;
    // End:0xE7
    if(!B.bActive)
    {
        B.bActive = CheckBurstPriority(Pawn);
        // End:0xE7
        if(B.bActive)
        {
            B.bPriorityBurst = true;
            BurstPriorityCount++;
        }
    }
    // End:0x102
    if(B.bActive)
    {
        NumberOfActiveBursts++;
    }
    Bursts.AddItem(B);
    return B.bActive;
    //return ReturnValue;    
}

function bool CheckBurstPriority(TdBotPawn Pawn)
{
    local Burst B;
    local int I;

    I = 0;
    J0x07:

    // End:0xA1 [Loop If]
    if(I < Bursts.Length)
    {
        B = Bursts[I];
        // End:0x97
        if(B.bActive && PrioritizedOver(Pawn, B.Pawn))
        {
            B.Pawn.InterruptBurst();
            RemoveBurst(B.Pawn);
            return true;
        }
        ++I;
        // [Loop Continue]
        goto J0x07;
    }
    return false;
    //return ReturnValue;    
}

function bool PrioritizedOver(TdBotPawn P1, TdBotPawn P2)
{
    local float p1Value, p2Value;

    // End:0x15C
    if((P1.myController.PlayerLookingTowardsMeDot > 0.71) && P1.myController.EnemyDistance < 1000)
    {
        // End:0x15A
        if((P2.myController.PlayerLookingTowardsMeDot > 0.71) && P2.myController.EnemyDistance < 1000)
        {
            p1Value = ((P1.myController.PlayerLookingTowardsMeDot - 0.71) / (1 - 0.71)) + ((1000 - P1.myController.EnemyDistance) / 1000);
            p2Value = ((P2.myController.PlayerLookingTowardsMeDot - 0.71) / (1 - 0.71)) + ((1000 - P2.myController.EnemyDistance) / 1000);
            return p1Value > p2Value;            
        }
        else
        {
            return true;
        }
    }
    return false;
    //return ReturnValue;    
}

function UpdateBursts()
{
    local Burst B;
    local int I;

    I = 0;
    J0x07:

    // End:0x155 [Loop If]
    if(I < Bursts.Length)
    {
        B = Bursts[I];
        // End:0xC5
        if(B.bActive)
        {
            // End:0xC2
            if(TimeSince(B.StartTime) > B.Duration)
            {
                Bursts[I].Pawn.BurstOver();
                // End:0xAD
                if(Bursts[I].bPriorityBurst)
                {
                    BurstPriorityCount--;
                }
                Bursts.Remove(I--, 1);
                NumberOfActiveBursts--;
            }
            // [Explicit Continue]
            goto J0x14B;
        }
        // End:0x14B
        if(!UseBurstControl || NumberOfActiveBursts < MaxSimultaneousBursts)
        {
            NumberOfActiveBursts++;
            B.bActive = true;
            B.StartTime = WorldInfo.TimeSeconds;
            Bursts[I] = B;
            B.Pawn.NotifyOkToBurst();
        }
        J0x14B:

        ++I;
        // [Loop Continue]
        goto J0x07;
    }
    //return;    
}

function ReportBurstFinished(TdBotPawn Pawn)
{
    RemoveBurst(Pawn);
    //return;    
}

private final function RemoveBurst(TdBotPawn Pawn)
{
    local int I;

    I = 0;
    J0x07:

    // End:0x91 [Loop If]
    if(I < Bursts.Length)
    {
        // End:0x87
        if(Bursts[I].Pawn == Pawn)
        {
            // End:0x79
            if(Bursts[I].bActive)
            {
                NumberOfActiveBursts--;
                // End:0x79
                if(Bursts[I].bPriorityBurst)
                {
                    BurstPriorityCount--;
                }
            }
            Bursts.Remove(I, 1);
            return;
        }
        I++;
        // [Loop Continue]
        goto J0x07;
    }
    //return;    
}

function bool OkToStartTestingCombatTransitions()
{
    local bool IsOk;

    IsOk = !bHasAnyoneStartedTestingCombatTransitionsThisFrame;
    bHasAnyoneStartedTestingCombatTransitionsThisFrame = true;
    return IsOk;
    //return ReturnValue;    
}

// Export UTdAIManager::execGetBlockingPawn(FFrame&, void* const)
native function TdBotPawn GetBlockingPawn(TdAIController me, bool bCrouching, optional bool UpdateMoveDir);

function NotifyPlayerNavigationPointChanged()
{
    // End:0x4B
    if(NumNotifiedAI < AIControllers.Length)
    {
        AIControllers[NotifiedIndex].NotifyNewPlayerNavigationPoint();
        NotifiedIndex = Percent_IntInt(NotifiedIndex + 1, AIControllers.Length);
        ++NumNotifiedAI;
    }
    //return;    
}

// Export UTdAIManager::execIsOkToUpdateVisibility(FFrame&, void* const)
native function bool IsOkToUpdateVisibility(TdAIController C);

function bool AllowPathfinding(TdAIController C)
{
    // End:0x1B
    if(NumPathfindsThisFrame == 0)
    {
        return IsPathfindingTimestampOkForNewPathfind(C);
    }
    return false;
    //return ReturnValue;    
}

function bool IsPathfindingTimestampOkForNewPathfind(TdAIController C)
{
    local PathfindingStruct PS;

    // End:0x53
    foreach PathfindingData(PS)
    {
        // End:0x52
        if(PS.C == C)
        {            
            return TimeSince(PS.LastPathfindTimeStamp) > (0.035 * float(AIControllers.Length));
        }        
    }    
    //return ReturnValue;    
}

function RegisterPathfind(TdAIController C)
{
    NumPathfindsThisFrame++;
    TimestampPathfind(C);
    // End:0x30
    if(NumPathfindsThisFrame > MaxNumPathfindsPerFrame)
    {
        MaxNumPathfindsPerFrame = NumPathfindsThisFrame;
    }
    //return;    
}

function TimestampPathfind(TdAIController C)
{
    local PathfindingStruct PS;

    // End:0x4D
    foreach PathfindingData(PS)
    {
        // End:0x4C
        if(PS.C == C)
        {
            PS.LastPathfindTimeStamp = WorldInfo.TimeSeconds;            
        }
        else
        {            
        }
    }    
    //return;    
}

function RegisterWaitTime(float WaitTime)
{
    // End:0x1A
    if(WaitTime > LongestWaitTime)
    {
        LongestWaitTime = WaitTime;
    }
    //return;    
}

function DrawDebugInfo(PlayerController PlayerC, Canvas aCanvas)
{
    //return;    
}

function ShowPrioritizedBurst()
{
    local Burst B;
    local int I;

    I = 0;
    J0x07:

    // End:0xAE [Loop If]
    if(I < Bursts.Length)
    {
        B = Bursts[I];
        // End:0xA4
        if(B.bActive && B.bPriorityBurst)
        {
            B.Pawn.DrawDebugSphere(B.Pawn.Location + vect(0, 0, 130), 15, 5, 255, 255, 255);
        }
        ++I;
        // [Loop Continue]
        goto J0x07;
    }
    //return;    
}

function float GetAveragePathfindsPerSecond()
{
    local int Value, I;

    Value = 0;
    I = 0;
    J0x0E:

    // End:0x3A [Loop If]
    if(I < PathfindsPerFrameHistory.Length)
    {
        Value += PathfindsPerFrameHistory[I];
        ++I;
        // [Loop Continue]
        goto J0x0E;
    }
    return float(Value) / float(PathfindsPerFrameHistory.Length);
    //return ReturnValue;    
}

// Export UTdAIManager::execUpdatePlayerNavigationPoint(FFrame&, void* const)
native function UpdatePlayerNavigationPoint();

// Export UTdAIManager::execUpdatePlayerNavigationPointForShieldWall(FFrame&, void* const)
native function UpdatePlayerNavigationPointForShieldWall(AITeam Team);

function AddTeam(AITeam T)
{
    Teams[Teams.Length] = T;
    //return;    
}

function RemoveTeam(AITeam T)
{
    local int I;

    I = 0;
    J0x07:

    // End:0x44 [Loop If]
    if(I < Teams.Length)
    {
        // End:0x3A
        if(Teams[I] == T)
        {
            Teams.Remove(I, 1);
            return;
        }
        ++I;
        // [Loop Continue]
        goto J0x07;
    }
    //return;    
}

function NavigationPoint GetPlayerNavigationPointForShieldWall()
{
    return PlayerNavigationPointForShieldWall;
    //return ReturnValue;    
}

function NotifyCheckedLastSeenPosition()
{
    SetLastSeenLocation(Enemy);
    //return;    
}

// Export UTdAIManager::execPlayerMoveOkForLastSeenLocationUpdate(FFrame&, void* const)
native function bool PlayerMoveOkForLastSeenLocationUpdate(TdPawn aPawn);

// Export UTdAIManager::execSetLastSeenLocation(FFrame&, void* const)
native final function SetLastSeenLocation(optional TdPawn aPawn, optional Vector L);

// Export UTdAIManager::execFindPredictedLastSeenLocation(FFrame&, void* const)
private native final function FindPredictedLastSeenLocation();

// Export UTdAIManager::execSetLastSeenLocationLocal(FFrame&, void* const)
private native final function SetLastSeenLocationLocal(Vector Loc);

// Export UTdAIManager::execRestoreOldLastSeenLocation(FFrame&, void* const)
private native final function RestoreOldLastSeenLocation(Vector L);

function SetPlayer(TdPlayerPawn InPlayer)
{
    Player = InPlayer;
    //return;    
}

function Add(TdAIController Bot)
{
    local PathfindingStruct PS;

    AIControllers.AddItem(Bot);
    PS.C = Bot;
    PS.LastPathfindTimeStamp = -999;
    PathfindingData.AddItem(PS);
    //return;    
}

function RemoveBot(TdAIController Bot)
{
    local PathfindingStruct PS;

    // End:0x16
    if(GrenadeThrowingBot == Bot)
    {
        GrenadeThrowingBot = none;
    }
    AIControllers.RemoveItem(Bot);
    // End:0x5D
    foreach PathfindingData(PS)
    {
        // End:0x5C
        if(PS.C == Bot)
        {
            PathfindingData.RemoveItem(PS);            
        }
        else
        {            
        }
    }    
    RemoveBurst(Bot.myPawn);
    // End:0x89
    if(AIControllers.Length == 0)
    {
        NotifiedIndex = 0;        
    }
    else
    {
        // End:0xA8
        if(NotifiedIndex >= AIControllers.Length)
        {
            NotifiedIndex = AIControllers.Length - 1;
        }
    }
    // End:0xBE
    if(AIControllers.Length == 0)
    {
        UpdateVisibilityIndex = 0;        
    }
    else
    {
        // End:0xDD
        if(UpdateVisibilityIndex >= AIControllers.Length)
        {
            UpdateVisibilityIndex = AIControllers.Length - 1;
        }
    }
    //return;    
}

function AddGrenadeArea(TdGrenadeTargetArea TargetArea)
{
    GrenadeTargetAreas.AddItem(TargetArea);
    //return;    
}

function RemoveGrenadeArea(TdGrenadeTargetArea TargetArea)
{
    GrenadeTargetAreas.RemoveItem(TargetArea);
    //return;    
}

function FinishedThrowingGrenade(TdProj_Grenade Grenade)
{
    GrenadeThrowingBot = none;
    ActiveGrenade = Grenade;
    //return;    
}

function bool NearbyOffensiveGrenadeExists(TdBotPawn Pawn)
{
    // End:0x3E
    if(ActiveGrenade != none)
    {
        return VSize2D(Pawn.Location - ActiveGrenade.Location) < float(3000);        
    }
    else
    {
        return false;
    }
    //return ReturnValue;    
}

function NotifyGrenadeExploded(Vector ExplosionLocation, float Lifetime)
{
    local TdAIController C;

    // End:0x2E
    foreach AIControllers(C)
    {
        C.NotifyGrenadeExploded(ExplosionLocation, Lifetime);        
    }    
    //return;    
}

event GrenadeTick()
{
    // End:0x1A
    if(ActiveGrenade.bHasExploded)
    {
        ActiveGrenade = none;
    }
    //return;    
}

function NotifyPlayerSpotted(Pawn TheEnemy)
{
    local int I;
    local Sequence GameSequence;
    local array<SequenceObject> SpottedEvents;

    GameSequence = WorldInfo.GetGameSequence();
    GameSequence.FindSeqObjectsByClass(Class'SeqEvt_TdPlayerSpotted', true, SpottedEvents);
    I = 0;
    J0x38:

    // End:0x7A [Loop If]
    if(I < SpottedEvents.Length)
    {
        SequenceEvent(SpottedEvents[I]).CheckActivate(TheEnemy, TheEnemy, false);
        ++I;
        // [Loop Continue]
        goto J0x38;
    }
    //return;    
}

function ReportUsingMelee(TdAIController C, bool flag)
{
    local int I;

    I = BotsUsingMelee.Find(C);
    // End:0x3D
    if(flag && I == -1)
    {
        BotsUsingMelee.AddItem(C);        
    }
    else
    {
        // End:0x65
        if(!flag && I != -1)
        {
            BotsUsingMelee.Remove(I, 1);
        }
    }
    //return;    
}

function bool OkToMelee(TdAIController C)
{
    return (BotsUsingMelee.Length < 1) || BotsUsingMelee.Find(C) != -1;
    //return ReturnValue;    
}

function bool SomeoneElseIsPursuingOrMeleeing(TdAIController me)
{
    local TdAIController AIController;

    // End:0x98
    foreach AIControllers(AIController)
    {
        // End:0x58
        if(((AIController == me) || AIController.IsDead()) || AIController.EnemyDistance2d > float(800))
        {
            continue;            
        }
        else
        {
            // End:0x97
            if(AIController.IsInState('Pursuit', true) || AIController.IsInState(AIController.MeleeState))
            {                
                return true;
            }            
        }
    }    
    return false;
    //return ReturnValue;    
}

function bool IsAnyoneDoingAFinishingAttack(optional TdAIController exclude)
{
    local TdAIController AIController;
    local TdAI_Pursuit Pursuit;

    // End:0x59
    foreach AIControllers(AIController)
    {
        // End:0x23
        if(AIController == exclude)
        {
            continue;            
        }
        else
        {
            Pursuit = TdAI_Pursuit(AIController);
            // End:0x58
            if(Pursuit != none)
            {
                // End:0x58
                if(Pursuit.IsDoingFinishingAttack())
                {                    
                    return true;
                }
            }            
        }
    }    
    return false;
    //return ReturnValue;    
}

function bool IsAnyoneEngagedInCloseCombat(TdAIController me)
{
    local TdAIController AIController;

    // End:0x19
    if(me.EnemyIsDisarming())
    {
        return true;
    }
    // End:0x54
    foreach AIControllers(AIController)
    {
        // End:0x53
        if((AIController != me) && AIController.IsInMelee())
        {            
            return true;
        }        
    }    
    return false;
    //return ReturnValue;    
}

function NotifyPlayerReachable()
{
    local TdAIController AIController;

    // End:0x24
    foreach AIControllers(AIController)
    {
        AIController.NotifyPlayerReachable();        
    }    
    //return;    
}

exec function ChaseAI()
{
    //return;    
}

// Export UTdAIManager::execGetNumberOfFriendsWithinRadius2D(FFrame&, void* const)
native final function int GetNumberOfFriendsWithinRadius2D(TdAIController me, Vector pos, float Radius);

// Export UTdAIManager::execFindBestTaserSpot(FFrame&, void* const)
native final function bool FindBestTaserSpot(TdAIController C, out NavigationPoint np, Vector Point, int NetworkID, optional float MaxDist, optional float MaxDistZ);

// Export UTdAIManager::execIsGoalForAnyBot(FFrame&, void* const)
native final function bool IsGoalForAnyBot(NavigationPoint np, optional TdAIController exclude);

defaultproperties
{
    UseBurstControl=true
    MaxSimultaneousBursts=2
}