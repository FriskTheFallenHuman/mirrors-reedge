/*******************************************************************************
 * TdGhostPawn generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdGhostPawn extends TdGhostPawnBase
    config(Game)
    hidecategories(Navigation);

defaultproperties
{
    TrailParticleSystem_Normal=ParticleSystem'TT_Ghost.Effects.PS_FX_GhostTrail_01'
    TrailParticleSystem_Wide=ParticleSystem'TT_Ghost.Effects.PS_FX_GhostTrail_Wider_01'
    TrailHeadSocket=Trail_Head
    TrailNeckSocket=Trail_Neck
    TrailSpineSocket=Trail_Spine
    TrailSpine2Socket=Trail_Spine2
    TrailRightHandSocket=Trail_RighHand
    TrailLeftHandSocket=Trail_LeftHand
    TrailLeftFootSocket=Trail_LeftFoot
    TrailRightFootSocket=Trail_RightFoot
    FootDecalMaterial=DecalMaterial'TT_Ghost.Materials.M_FX_FootPrintsDecal_01'
    AmbientSoundSC=SoundCue'A_TimeTrial.Cues.Ghost_Proxy'
    bDisableCharacterSounds=true
    begin object name=ActorCollisionCylinder class=CylinderComponent
        CollideActors=false
        BlockActors=false
        BlockNonZeroExtent=false
    object end
    // Reference: CylinderComponent'Default__TdGhostPawn.ActorCollisionCylinder'
    ActorCylinderComponent=ActorCollisionCylinder
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        SkeletalMesh=SkeletalMesh'TT_Ghost.GhostCharacter_01'
        AnimTreeTemplate=AnimTree'AT_C3P.AT_C3P'
        AnimSets(0)=TdAnimSet'AS_F3P_Unarmed.AS_F3P_Unarmed'
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdGhostPawn.MyLightEnvironment'
        bOwnerNoSeeWithShadow=false
        bCastDynamicShadow=false
        CollideActors=false
        BlockZeroExtent=false
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdGhostPawn.TdPawnMesh3p'
    Mesh3p=TdPawnMesh3p
    bSimulateGravity=false
    bSimGravityDisabled=true
    SceneCapture=SceneCaptureCharacterComponent'Default__TdGhostPawn.SceneCaptureCharacterComponent0'
    DrawFrustum=DrawFrustumComponent'Default__TdGhostPawn.DrawFrust0'
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        SkeletalMesh=SkeletalMesh'TT_Ghost.GhostCharacter_01'
        AnimTreeTemplate=AnimTree'AT_C3P.AT_C3P'
        AnimSets(0)=TdAnimSet'AS_F3P_Unarmed.AS_F3P_Unarmed'
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdGhostPawn.MyLightEnvironment'
        bOwnerNoSeeWithShadow=false
        bCastDynamicShadow=false
        CollideActors=false
        BlockZeroExtent=false
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdGhostPawn.TdPawnMesh3p'
    Mesh=TdPawnMesh3p
    begin object name=CollisionCylinder class=CylinderComponent
        CollideActors=false
        BlockActors=false
        BlockNonZeroExtent=false
    object end
    // Reference: CylinderComponent'Default__TdGhostPawn.CollisionCylinder'
    CylinderComponent=CollisionCylinder
    bAlwaysRelevant=true
    bBlockActors=false
    Components(0)=SceneCaptureCharacterComponent'Default__TdGhostPawn.SceneCaptureCharacterComponent0'
    Components(1)=DrawFrustumComponent'Default__TdGhostPawn.DrawFrust0'
    begin object name=CollisionCylinder class=CylinderComponent
        CollideActors=false
        BlockActors=false
        BlockNonZeroExtent=false
    object end
    // Reference: CylinderComponent'Default__TdGhostPawn.CollisionCylinder'
    Components(2)=CollisionCylinder
    Components(3)=ArrowComponent'Default__TdGhostPawn.Arrow'
    Components(4)=DynamicLightEnvironmentComponent'Default__TdGhostPawn.MyLightEnvironment'
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        SkeletalMesh=SkeletalMesh'TT_Ghost.GhostCharacter_01'
        AnimTreeTemplate=AnimTree'AT_C3P.AT_C3P'
        AnimSets(0)=TdAnimSet'AS_F3P_Unarmed.AS_F3P_Unarmed'
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdGhostPawn.MyLightEnvironment'
        bOwnerNoSeeWithShadow=false
        bCastDynamicShadow=false
        CollideActors=false
        BlockZeroExtent=false
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdGhostPawn.TdPawnMesh3p'
    Components(5)=TdPawnMesh3p
    begin object name=CollisionCylinder class=CylinderComponent
        CollideActors=false
        BlockActors=false
        BlockNonZeroExtent=false
    object end
    // Reference: CylinderComponent'Default__TdGhostPawn.CollisionCylinder'
    Components(6)=CollisionCylinder
    begin object name=ActorCollisionCylinder class=CylinderComponent
        CollideActors=false
        BlockActors=false
        BlockNonZeroExtent=false
    object end
    // Reference: CylinderComponent'Default__TdGhostPawn.ActorCollisionCylinder'
    Components(7)=ActorCollisionCylinder
    begin object name=CollisionCylinder class=CylinderComponent
        CollideActors=false
        BlockActors=false
        BlockNonZeroExtent=false
    object end
    // Reference: CylinderComponent'Default__TdGhostPawn.CollisionCylinder'
    CollisionComponent=CollisionCylinder
}