#define ENGINE_VERSION 10897
#define BUILT_FROM_CHANGELIST 1532151

#if GAMENAME==TDGAME
	#define EPIC_COMPANY_NAME  "EA Digital iLLusions CE AB."
	#define EPIC_COPYRIGHT_STRING "Copyright 2009 EA Digital iLLusions CE AB."
	#define EPIC_PRODUCT_NAME "Mirror's Edge"
#else
	#define EPIC_COMPANY_NAME  "Epic Games, Inc."
	#define EPIC_COPYRIGHT_STRING "Copyright 1998-2013 Epic Games, Inc. All Rights Reserved."
	#define EPIC_PRODUCT_NAME "UnrealEngine3"
#endif
