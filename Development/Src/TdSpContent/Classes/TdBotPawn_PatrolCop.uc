/*******************************************************************************
 * TdBotPawn_PatrolCop generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdBotPawn_PatrolCop extends TdBotPawn
    config(AI)
    hidecategories(Navigation);

function SoundCue GetSpecificFootStepSound(TdPhysicalMaterialFootSteps FootStepSounds, int FootDown)
{
    local SoundCue retval;

    switch(FootDown)
    {
        // End:0x1A
        case 0:
            retval = SoundCue'Fire';
            // End:0x2B7
            break;
        // End:0x36
        case 1:
            retval = FootStepSounds._01_Female_FootStepCrouch;
            // End:0x2B7
            break;
        // End:0x53
        case 2:
            retval = FootStepSounds._02_Female_FootStepWalk;
            // End:0x2B7
            break;
        // End:0x70
        case 3:
            retval = FootStepSounds._03_Female_FootStepRun;
            // End:0x2B7
            break;
        // End:0x8D
        case 4:
            retval = FootStepSounds._04_Female_FootStepSprint;
            // End:0x2B7
            break;
        // End:0xAA
        case 5:
            retval = FootStepSounds._05_Female_FootStepSprintRelease;
            // End:0x2B7
            break;
        // End:0xC7
        case 6:
            retval = FootStepSounds._06_Female_FootStepWallRun;
            // End:0x2B7
            break;
        // End:0xE4
        case 7:
            retval = FootStepSounds._07_Female_FootStepWallrunRelease;
            // End:0x2B7
            break;
        // End:0x101
        case 8:
            retval = FootStepSounds._08_Female_FootStepLandSoft;
            // End:0x2B7
            break;
        // End:0x11E
        case 9:
            retval = FootStepSounds._09_Female_FootStepLandMedium;
            // End:0x2B7
            break;
        // End:0x13B
        case 10:
            retval = FootStepSounds._10_Female_FootStepLandHard;
            // End:0x2B7
            break;
        // End:0x158
        case 11:
            retval = FootStepSounds._11_Female_FootStepAttack;
            // End:0x2B7
            break;
        // End:0x175
        case 21:
            retval = FootStepSounds._21_Female_HandStepSoft;
            // End:0x2B7
            break;
        // End:0x192
        case 22:
            retval = FootStepSounds._22_Female_HandStepMedium;
            // End:0x2B7
            break;
        // End:0x1AF
        case 23:
            retval = FootStepSounds._23_Female_HandStepHard;
            // End:0x2B7
            break;
        // End:0x1CC
        case 24:
            retval = FootStepSounds._24_Female_HandStepLongRelease;
            // End:0x2B7
            break;
        // End:0x1E9
        case 25:
            retval = FootStepSounds._25_Female_HandStepShortRelease;
            // End:0x2B7
            break;
        // End:0x206
        case 26:
            retval = FootStepSounds._26_Female_HandStepAttack;
            // End:0x2B7
            break;
        // End:0x223
        case 31:
            retval = FootStepSounds._31_Female_BodyAttack;
            // End:0x2B7
            break;
        // End:0x240
        case 32:
            retval = FootStepSounds._32_Female_BodyLandSoft;
            // End:0x2B7
            break;
        // End:0x25D
        case 33:
            retval = FootStepSounds._33_Female_BodyLandHard;
            // End:0x2B7
            break;
        // End:0x27A
        case 34:
            retval = FootStepSounds._34_Female_BodyLandRoll;
            // End:0x2B7
            break;
        // End:0x297
        case 35:
            retval = FootStepSounds._35_Female_BodyVault;
            // End:0x2B7
            break;
        // End:0x2B4
        case 36:
            retval = FootStepSounds._36_Female_BodySlide;
            // End:0x2B7
            break;
        // End:0xFFFF
        default:
            break;
    }
    return retval;
    //return ReturnValue;    
}

defaultproperties
{
    begin object name=AdditionalSkeletalMeshComponent class=TdSkeletalMeshComponent
        ParentAnimComponent=TdSkeletalMeshComponent'Default__TdBotPawn_PatrolCop.TdPawnMesh3p'
        bDisableWarningWhenAnimNotFound=true
        ShadowParent=TdSkeletalMeshComponent'Default__TdBotPawn_PatrolCop.TdPawnMesh3p'
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdBotPawn_PatrolCop.MyLightEnvironment'
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdBotPawn_PatrolCop.AdditionalSkeletalMeshComponent'
    AdditionalSkeletalMesh=AdditionalSkeletalMeshComponent
    ActorCylinderComponent=CylinderComponent'Default__TdBotPawn_PatrolCop.ActorCollisionCylinder'
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        AnimTreeTemplate=AnimTree'AT_Cop.AT_Cop'
        PhysicsAsset=PhysicsAsset'CH_TKY_Cop_SWAT.Male3p_Physics'
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdBotPawn_PatrolCop.MyLightEnvironment'
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdBotPawn_PatrolCop.TdPawnMesh3p'
    Mesh3p=TdPawnMesh3p
    MoveClasses=/* Array type was not detected. */
    ArmorBulletsHeadSettings=(Hard=0.2)
    ArmorBulletsBodySettings=(Hard=0.2)
    ArmorBulletsLegsSettings=(Hard=0.2)
    ArmorMeleeHeadSettings=(Hard=0.1)
    ArmorMeleeBodySettings=(Hard=0.1)
    ArmorMeleeLegsSettings=(Hard=0.1)
    ArmorBulletsHeadSettings_CHASE=(Hard=0.2)
    ArmorBulletsBodySettings_CHASE=(Hard=0.2)
    ArmorBulletsLegsSettings_CHASE=(Hard=0.2)
    ArmorMeleeHeadSettings_CHASE=(Hard=0.1)
    ArmorMeleeBodySettings_CHASE=(Hard=0.1)
    ArmorMeleeLegsSettings_CHASE=(Hard=0.1)
    SceneCapture=SceneCaptureCharacterComponent'Default__TdBotPawn_PatrolCop.SceneCaptureCharacterComponent0'
    DrawFrustum=DrawFrustumComponent'Default__TdBotPawn_PatrolCop.DrawFrust0'
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        AnimTreeTemplate=AnimTree'AT_Cop.AT_Cop'
        PhysicsAsset=PhysicsAsset'CH_TKY_Cop_SWAT.Male3p_Physics'
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdBotPawn_PatrolCop.MyLightEnvironment'
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdBotPawn_PatrolCop.TdPawnMesh3p'
    Mesh=TdPawnMesh3p
    CylinderComponent=CylinderComponent'Default__TdBotPawn_PatrolCop.CollisionCylinder'
    Components(0)=SceneCaptureCharacterComponent'Default__TdBotPawn_PatrolCop.SceneCaptureCharacterComponent0'
    Components(1)=DrawFrustumComponent'Default__TdBotPawn_PatrolCop.DrawFrust0'
    Components(2)=CylinderComponent'Default__TdBotPawn_PatrolCop.CollisionCylinder'
    Components(3)=ArrowComponent'Default__TdBotPawn_PatrolCop.Arrow'
    Components(4)=DynamicLightEnvironmentComponent'Default__TdBotPawn_PatrolCop.MyLightEnvironment'
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        AnimTreeTemplate=AnimTree'AT_Cop.AT_Cop'
        PhysicsAsset=PhysicsAsset'CH_TKY_Cop_SWAT.Male3p_Physics'
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdBotPawn_PatrolCop.MyLightEnvironment'
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdBotPawn_PatrolCop.TdPawnMesh3p'
    Components(5)=TdPawnMesh3p
    Components(6)=CylinderComponent'Default__TdBotPawn_PatrolCop.CollisionCylinder'
    Components(7)=CylinderComponent'Default__TdBotPawn_PatrolCop.ActorCollisionCylinder'
    CollisionComponent=CylinderComponent'Default__TdBotPawn_PatrolCop.CollisionCylinder'
}