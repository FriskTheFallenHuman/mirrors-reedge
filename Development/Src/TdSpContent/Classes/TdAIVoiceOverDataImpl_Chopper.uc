/*******************************************************************************
 * TdAIVoiceOverDataImpl_Chopper generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdAIVoiceOverDataImpl_Chopper extends TdAIVoiceOverData;

defaultproperties
{
    VOs=/* Array type was not detected. */
    FirstVO=29
    LastVo=31
}