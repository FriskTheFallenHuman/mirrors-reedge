/*******************************************************************************
 * TdBotPawn_SupportHelicopterGunner generated by Eliot.UELib using UE Explorer.
 * Eliot.UELib ? 2009-2022 Eliot van Uytfanghe. All rights reserved.
 * http://eliotvu.com
 *
 * All rights belong to their respective owners.
 *******************************************************************************/
class TdBotPawn_SupportHelicopterGunner extends TdBotPawn
    config(AI)
    hidecategories(Navigation);

simulated function Turn(float DeltaTime)
{
    //return;    
}

event Vector GetViewpointLocation(optional bool ForceCrouch)
{
    return super.GetViewpointLocation(true);
    //return ReturnValue;    
}

defaultproperties
{
    ActorCylinderComponent=CylinderComponent'Default__TdBotPawn_SupportHelicopterGunner.ActorCollisionCylinder'
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        AnimTreeTemplate=AnimTree'AT_Cop.AT_Helicopter'
        PhysicsAsset=PhysicsAsset'CH_TKY_Cop_SWAT.Male3p_Physics'
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdBotPawn_SupportHelicopterGunner.MyLightEnvironment'
        CastShadow=false
        bCastDynamicShadow=false
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdBotPawn_SupportHelicopterGunner.TdPawnMesh3p'
    Mesh3p=TdPawnMesh3p
    SceneCapture=SceneCaptureCharacterComponent'Default__TdBotPawn_SupportHelicopterGunner.SceneCaptureCharacterComponent0'
    DrawFrustum=DrawFrustumComponent'Default__TdBotPawn_SupportHelicopterGunner.DrawFrust0'
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        AnimTreeTemplate=AnimTree'AT_Cop.AT_Helicopter'
        PhysicsAsset=PhysicsAsset'CH_TKY_Cop_SWAT.Male3p_Physics'
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdBotPawn_SupportHelicopterGunner.MyLightEnvironment'
        CastShadow=false
        bCastDynamicShadow=false
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdBotPawn_SupportHelicopterGunner.TdPawnMesh3p'
    Mesh=TdPawnMesh3p
    CylinderComponent=CylinderComponent'Default__TdBotPawn_SupportHelicopterGunner.CollisionCylinder'
    Components(0)=SceneCaptureCharacterComponent'Default__TdBotPawn_SupportHelicopterGunner.SceneCaptureCharacterComponent0'
    Components(1)=DrawFrustumComponent'Default__TdBotPawn_SupportHelicopterGunner.DrawFrust0'
    Components(2)=CylinderComponent'Default__TdBotPawn_SupportHelicopterGunner.CollisionCylinder'
    Components(3)=ArrowComponent'Default__TdBotPawn_SupportHelicopterGunner.Arrow'
    Components(4)=DynamicLightEnvironmentComponent'Default__TdBotPawn_SupportHelicopterGunner.MyLightEnvironment'
    begin object name=TdPawnMesh3p class=TdSkeletalMeshComponent
        AnimTreeTemplate=AnimTree'AT_Cop.AT_Helicopter'
        PhysicsAsset=PhysicsAsset'CH_TKY_Cop_SWAT.Male3p_Physics'
        LightEnvironment=DynamicLightEnvironmentComponent'Default__TdBotPawn_SupportHelicopterGunner.MyLightEnvironment'
        CastShadow=false
        bCastDynamicShadow=false
    object end
    // Reference: TdSkeletalMeshComponent'Default__TdBotPawn_SupportHelicopterGunner.TdPawnMesh3p'
    Components(5)=TdPawnMesh3p
    Components(6)=CylinderComponent'Default__TdBotPawn_SupportHelicopterGunner.CollisionCylinder'
    Components(7)=CylinderComponent'Default__TdBotPawn_SupportHelicopterGunner.ActorCollisionCylinder'
    CollisionComponent=CylinderComponent'Default__TdBotPawn_SupportHelicopterGunner.CollisionCylinder'
}