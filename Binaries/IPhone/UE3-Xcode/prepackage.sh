#!/bin/sh

if [ UE3.xcodeproj/project.pbxproj.datecheck -nt ../UE3.xcodeproj/project.pbxproj ]
then
  echo "Copying project..."
  mkdir ../UE3.xcodeproj > /dev/null
  cp UE3.xcodeproj/project.pbxproj.datecheck ../UE3.xcodeproj/project.pbxproj
else
  echo "Project file is up to date."
fi

if [ -e $1-$2-$3.app.dSYM.zip.datecheck ]
then
  if [ $1-$2-$3.app.dSYM.zip.datecheck -nt ../$1-$2-$3.app.dSYM.zip ]
  then
    echo "Unzipping new .dSYM..."
    cp $1-$2-$3.app.dSYM.zip.datecheck ../$1-$2-$3.app.dSYM.zip
    pushd .. > /dev/null
    unzip -o $1-$2-$3.app.dSYM.zip
    popd > /dev/null
  else
    echo ".dSYM is up to date."
  fi
fi

if [ -e ../$1-$2-$3.app.dSYM ]
then
  pushd .. > /dev/null
  cp -R $1-$2-$3.app.dSYM $1-$2/$3/Payload/$1.app.dSYM
  popd > /dev/null
fi
