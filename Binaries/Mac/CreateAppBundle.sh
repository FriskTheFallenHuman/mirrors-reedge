#!/bin/sh

if [ "$#" -lt "6" ]; then
	echo "Nothing to do"
	exit
fi

WORKING_DIR=$1
GAME_NAME=$2
EXE_NAME=$3
MIN_MACOSX_VERSION=$4
MACOSX_SDK_VERSION=$5
DEV_FOLDER=$6

cd "$WORKING_DIR"

mkdir "$EXE_NAME.app"
mkdir "$EXE_NAME.app/Contents"
mkdir "$EXE_NAME.app/Contents/MacOS"
mkdir "$EXE_NAME.app/Contents/Frameworks"
mkdir "$EXE_NAME.app/Contents/Resources"
mkdir "$EXE_NAME.app/Contents/Resources/English.lproj"
mkdir "$EXE_NAME.app/Contents/Resources/RadioEffectUnit.component"
mkdir "$EXE_NAME.app/Contents/Resources/RadioEffectUnit.component/Contents"
mkdir "$EXE_NAME.app/Contents/Resources/RadioEffectUnit.component/Contents/MacOS"
mkdir "$EXE_NAME.app/Contents/Resources/RadioEffectUnit.component/Contents/Resources"
mkdir "$EXE_NAME.app/Contents/Resources/RadioEffectUnit.component/Contents/Resources/English.lproj"

cp -f "$EXE_NAME" "$EXE_NAME.app/Contents/MacOS"
cp -f "../../Development/External/libogg-1.2.2/macosx/libogg.dylib" "$EXE_NAME.app/Contents/Frameworks"
cp -f "../../Development/External/libvorbis-1.3.2/macosx/libvorbis.dylib" "$EXE_NAME.app/Contents/Frameworks"
cp -f "../../Development/Src/Mac/Resources/$GAME_NAME.icns" "$EXE_NAME.app/Contents/Resources/$GAME_NAME.icns"

# Prepare Info.plist
cp -f "../../Development/Src/Mac/Resources/$GAME_NAME-Info.plist" "$EXE_NAME.app/Contents/Info.plist"

# Generate PkgInfo
echo 'echo -n "APPL????"' | bash > "$EXE_NAME.app/Contents/PkgInfo"

# Copy InfoPlist.strings
iconv -f UTF-8 -t UTF-16 "../../Development/Src/Mac/Resources/English.lproj/InfoPlist.strings" > "$EXE_NAME.app/Contents/Resources/English.lproj/InfoPlist.strings"

# Compile XIB resource
$DEV_FOLDER/usr/bin/ibtool --errors --warnings --notices --output-format human-readable-text --compile "$EXE_NAME.app/Contents/Resources/English.lproj/MainMenu.nib" "../../Development/Src/Mac/Resources/English.lproj/MainMenu.xib" --sdk /Developer/SDKs/MacOSX$MACOSX_SDK_VERSION.sdk

# Copy RadioEffect component
cp -f "../../Development/Src/Mac/Resources/RadioEffectUnit.component/Contents/MacOS/RadioEffectUnit" "$EXE_NAME.app/Contents/Resources/RadioEffectUnit.component/Contents/MacOS/RadioEffectUnit"
cp -f "../../Development/Src/Mac/Resources/RadioEffectUnit.component/Contents/Resources/English.lproj/Localizable.strings" "$EXE_NAME.app/Contents/Resources/RadioEffectUnit.component/Contents/Resources/English.lproj/Localizable.strings"
cp -f "../../Development/Src/Mac/Resources/RadioEffectUnit.component/Contents/Resources/RadioEffectUnit.rsrc" "$EXE_NAME.app/Contents/Resources/RadioEffectUnit.component/Contents/Resources/RadioEffectUnit.rsrc"
cp -f "../../Development/Src/Mac/Resources/RadioEffectUnit.component/Contents/Info.plist" "$EXE_NAME.app/Contents/Resources/RadioEffectUnit.component/Contents/Info.plist"

# Add CodeResources needed for signed bundles (for Mac App Store)
mkdir "$EXE_NAME.app/Contents/_CodeSignature"
echo 'echo -n ""' | bash > "$EXE_NAME.app/Contents/_CodeSignature/CodeResources"
cd "$EXE_NAME.app/Contents"
ln -s _CodeSignature/CodeResources CodeResources
cd ../..

touch -c "$EXE_NAME.app"

zip -r -y -1 "$EXE_NAME.app.stub" "$EXE_NAME.app"
