@echo off

rem To install on emulator:  Install.bat <GameName> <Config> -e
rem To install on device:    Install.bat <GameName> <Config> -d

echo Installing %1Game-Android-%2.apk
adb %3 install -r %1Game-Android-%2.apk
