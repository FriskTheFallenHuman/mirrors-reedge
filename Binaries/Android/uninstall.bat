@echo off

rem To uninstall from emulator:  Uninstall.bat -e
rem To uninstall from device:    Uninstall.bat -d

echo Removing com.epicgames.UnrealEngine3
adb %4 uninstall com.epicgames.UnrealEngine3
