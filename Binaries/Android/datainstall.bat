@echo off

rem To install on emulator:         DataInstall.bat <GameName> <Config> <TexFormat> -e
rem To install on device:           DataInstall.bat <GameName> <Config> <TexFormat> -d

echo 1. Removing old content from sdcard/UnrealEngine3...
c:\Android\android-sdk-windows\platform-tools\adb %4 shell rm -r /sdcard/UnrealEngine3

set DEPLOY_DIR=..\..\Development\Intermediate\%1Game\Android\%2\Deploy\UnrealEngine3
echo 2. Creating deployment directory in %DEPLOY_DIR%

rmdir /s /q %DEPLOY_DIR%
mkdir %DEPLOY_DIR%

xcopy /s /i ..\..\%1Game\CookedAndroid_%3 %DEPLOY_DIR%\%1Game\CookedAndroid_%3

mkdir %DEPLOY_DIR%\%1Game\CookedAndroid\Movies
copy ..\..\%1Game\Build\Android\Resources\Movies\*.m4v %DEPLOY_DIR%\%1Game\Movies

mkdir %DEPLOY_DIR%\%1Game\CookedAndroid\Music
copy ..\..\%1Game\Build\Android\Resources\Music\*.mp3 %DEPLOY_DIR%\%1Game\Music

mkdir %DEPLOY_DIR%\%1Game\Engine\Shaders
xcopy /s /i ..\..\Engine\Shaders\Mobile %DEPLOY_DIR%\Engine\Shaders\Mobile

mkdir %DEPLOY_DIR%\%1Game\Engine\Stats
xcopy /s /i ..\..\Engine\Stats %DEPLOY_DIR%\Engine\Stats

echo 3. Installing new content to sdcard/UnrealEngine3...
c:\Android\android-sdk-windows\platform-tools\adb %4 push %DEPLOY_DIR% /sdcard/UnrealEngine3

echo 4. Done
