@echo off

rem To install on emulator:         FullInstall.bat <GameName> <Config> <TexFormat> -e
rem To install on device:           FullInstall.bat <GameName> <Config> <TexFormat> -d

call Uninstall.bat %*
call Install.bat %*
call DataInstall.bat %*

