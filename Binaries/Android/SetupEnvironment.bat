@echo off

rem Epic variables
set ANDROID_ROOT=C:\Android
set ANT_ROOT=%ANDROID_ROOT%\apache-ant-1.8.1

rem Standard variables
set CYGWIN=nodosfilewarning
set CYGWIN_HOME=%ANDROID_ROOT%\cygwin
set JAVA_HOME=%ANDROID_ROOT%\jdk1.6.0_21
set NDKROOT=%ANDROID_ROOT%\android-ndk-r8b

set PATH=%ANT_ROOT%\bin;%ANDROID_ROOT%\android-sdk-windows\tools;%ANDROID_ROOT%\android-sdk-windows\platform-tools;%CYGWIN_HOME%\bin;%JAVA_HOME%\bin;%PATH%
set CLASSPATH=%CLASSPATH%;%JAVA_HOME%\lib

echo Environment variables for Android development are now set.
pause