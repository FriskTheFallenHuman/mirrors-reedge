@echo off
setlocal

title Compile initial engine tools

:: Set the path to vcvarsall.bat
set "VCVARSALL=C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat"

:: Call vcvarsall.bat x86 since the initial tools doesn't support x64
call "%VCVARSALL%" x86

:: Change the current directory
cd Development\Tools\Mobile\RPCUtility

:: Build RPCUtility
msbuild RPCUtility.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\..\..\Src\Targets\Clean

:: Build Clean.exe
msbuild Clean.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Go up two folders
cd ..\..\UnrealBuildTool

:: Build Clean.exe
msbuild UnrealBuildTool.csproj /m /nologo /t:Rebuild /p:Configuration=Release

endlocal

@pause