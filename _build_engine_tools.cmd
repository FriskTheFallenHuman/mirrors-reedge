@echo off
setlocal

title Compile engine tools

:: Set the path to vcvarsall.bat
set "VCVARSALL=C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat"

:: Call vcvarsall.bat x86 since the engine tools doesn't support x64
call "%VCVARSALL%" x86

:: Change the current directory
cd Development\Tools\UnSetup

:: Build UnSetup
msbuild UnSetup.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\UnrealFrontend

:: Build UnrealFrontend
msbuild UnrealFrontend.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\UnrealLoc

:: Build UnrealLoc
msbuild UnrealLoc.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\UnrealSwarm

:: Build UnrealSwarm
msbuild UnrealSwarm.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\UnrealCommand

:: Build UnrealCommand
msbuild UnrealCommand.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\UDKLift

:: Build UDKLift
msbuild UDKLift.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\StatsViewer

:: Build StatsViewer
msbuild StatsViewer.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\ShaderKeyTool

:: Build ShaderKeyTool
msbuild EpicGames.Tools.ShaderKeyTool.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\PackageDiffFrontEnd

:: Build PackageDiffFrontEnd
msbuild PackageDiffFrontEnd.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\P4Utils\P4PopulateDepot

:: Build P4PopulateDepot
msbuild P4PopulateDepot.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\..\NetworkProfiler

:: Build NetworkProfiler
msbuild NetworkProfiler.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\MobileShaderAnalyzer

:: Build MobileShaderAnalyzer
msbuild MobileShaderAnalyzer.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\MemLeakCheckDiffer

:: Build MemLeakCheckDiffer
msbuild MemLeakCheckDiffer.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\GameplayProfiler

:: Build GameplayProfiler
msbuild GameplayProfiler.sln /m /nologo /t:Rebuild /p:Configuration=Release

:: Change the current directory
cd ..\AIProfiler

:: Build AIProfiler
msbuild AIProfiler.sln /m /nologo /t:Rebuild /p:Configuration=Release

endlocal

@pause